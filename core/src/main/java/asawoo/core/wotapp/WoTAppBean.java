package asawoo.core.wotapp;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import java.util.Set;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 15/06/17.
 */
public class WoTAppBean {

    private String id;

    private String description;

    private String url;

    private boolean active;

    private Set<String> requiredFunctionalities;

    public WoTAppBean(String id, String description, String url, boolean active) {
        this(id, description, url, active, null);
    }

    public WoTAppBean(String id, String description, String url, boolean active, Set<String> requiredFunctionalities) {
        this.id = id;
        this.description = description;
        this.url = url;
        this.active = active;
        this.requiredFunctionalities = requiredFunctionalities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<String> getRequiredFunctionalities() {
        return requiredFunctionalities;
    }

    public void setRequiredFunctionalities(Set<String> requiredFunctionalities) {
        this.requiredFunctionalities = requiredFunctionalities;
    }
}

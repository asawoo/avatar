package asawoo.core.wotapp;

import org.osgi.framework.BundleContext;

/**
 * Created by alban on 18/05/17.
 */
public interface WoTApp {

    String ASAWOO_WOTAPP_NAMESPACE = "asawoo.wotapp";
    String ASAWOO_WOTAPP_ID_PROPERTY = "id";
    String ASAWOO_WOTAPP_DESCRIPTION_PROPERTY = "description";
    String ASAWOO_WOTAPP_URL_PROPERTY = "url";
    String ASAWOO_WOTAPP_ACTIVE_PROPERTY = "active";

    public String getId();
    public String getDescription();
    public BundleContext getBundleContext();
}

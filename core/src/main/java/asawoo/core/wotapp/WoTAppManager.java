package asawoo.core.wotapp;

import org.osgi.framework.BundleException;

import javax.ws.rs.InternalServerErrorException;
import java.io.File;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Created by alban on 18/05/17.
 */
public interface WoTAppManager {

    /**
     *
     * @return a collection of active WoTApps
     */
    Collection<WoTAppBean> geWotapps();

    /**
     *
     * @return a collection of available / deployable WoTApps
     */
    Collection<WoTAppBean> getAvailableWotapps();

    /**
     *
     * @param wotappId WoTApp identifier
     * @return the active WoTApp matching specified ID
     */
    WoTAppBean getWotapp(String wotappId);

    /**
     *
     * @param wotappId
     * @return the directory where the WoTApp matching wotappId is deployed
     */
    File getWotappDir(String wotappId);

    /**
     *
     * @param wotappId
     * @throws NoSuchElementException
     * @throws InternalServerErrorException
     */
    void loadWotappBundle(String wotappId) throws NoSuchElementException, InternalServerErrorException, InterruptedException;

    /**
     *
     * @param wotappId
     * @throws BundleException
     */
    void unloadWotappBundle(String wotappId) throws BundleException;
}

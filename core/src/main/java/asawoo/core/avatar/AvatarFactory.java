package asawoo.core.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.config.AvatarConfiguration;

/**
 *
 * This interface allows to create and dispose avatars.
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 12/12/16.
 */
public interface AvatarFactory {

    String PREFIX_PLATFORM = "platform";
    String PREFIX_AVATAR = "avatar";

    void createAvatar(AvatarConfiguration configuration);

    void disposeAvatar(String avatarId);
}

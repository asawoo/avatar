package asawoo.core.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 05/05/17.
 */
public interface AvatarStateListener {

    void onStateChanged(int state);

}

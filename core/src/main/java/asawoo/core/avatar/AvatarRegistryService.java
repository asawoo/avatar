package asawoo.core.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.annotation.Functionality;
import asawoo.core.annotation.Method;
import asawoo.core.annotation.Parameter;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.FunctionalityInterface;

import javax.ws.rs.*;
import java.util.List;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 27/01/17.
 */
@Functionality(name = "AvatarFactory",
                description = "Provides services to list, create and dispose avatars.")
public interface AvatarRegistryService /*extends FunctionalityInterface */{

    /**
     *
     * @return a list of avatar URIs
     */
    @Method
    @Path("/avatars")
    @GET
    List<String> getAvatars();

//    TODO : generate this URI for each avatar
//     /**
//     *
//     * @return an avatar
//     */
//    @Method
//    @Path("/avatar")
//    @GET
//    String getAvatar();

    /**
     *
     * @param avatarConfiguration
     */
    @Method
    @Path("/avatars")
    @POST
    void addAvatar(@Parameter(name = "configuration")
                   AvatarConfiguration avatarConfiguration);

    @Method
    @Path("/avatars")
    @DELETE
    void removeAvatar(@Parameter(name = "avatarId")
                      String avatarId);

    @Method
    @Path("/avatars")
    @PUT
    void updateAvatar(@Parameter(name = "configuration")
                        AvatarConfiguration avatarConfiguration);

}

package asawoo.core.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.annotation.Functionality;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.*;
import asawoo.core.util.AsawooUtils;
import asawoo.core.util.CollectionListener;
import asawoo.core.util.PlatformUtils;
import io.vertx.core.json.JsonObject;
import org.apache.felix.bundlerepository.*;
import org.apache.felix.ipojo.*;
import org.apache.felix.ipojo.annotations.*;
import org.apache.felix.ipojo.annotations.Property;

import java.util.*;

/**
 *
 * Representation of an Avatar.
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 17/10/16.
 *
 */
public interface Avatar {

    int STATUS_STARTING = 0;
    int STATUS_READY = 1;
    int STATUS_ERROR = -1;

    String getAvatarId();

    AvatarConfiguration getConfiguration();

    int getStatus();

    String getUri();

    FunctionalityManager getFunctionalityManager();

    /**
     *
     * @return a JSON String view of this avatar
     */
    JsonObject toJson();

}

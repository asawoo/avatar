package asawoo.core.reasoner;

import java.util.List;

public interface AdaptationRule {

	public void setContextualInstances(List<String> instancesURI);

	public String toJenaRule();

	public void addInferredPossibility(String adapted, String purposePredicate,
			String candidate, String score, Integer blankNodeCount);
}

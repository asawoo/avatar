package asawoo.core.reasoner;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface SemanticReasoner {

	/**
	 * Sends an update (INSERT or DELETE) SPARQL query
	 * to a reasoner graph.
	 * @param query
	 * @param graph
	 */
	public void launchUpdateQuery(String query, String graph);

	/**
	 * Sends a SELECT SPARQL query to a reasoner graph.
	 * Returns a map of SPARQL bindings.
	 * @param query
	 * @param graph
	 * @return
	 */
	public Map<?, ?> launchSelectQuery(String query, String graph);
	
	/**
	 * Sends a SELECT SPARQL query to a reasoner context graph.
	 * Returns a map of SPARQL bindings.
	 * @param query
	 * @param graph
	 * @return
	 */
	public List<String> launchContextSelectQuery(String query, String graph);
	
	/**
	 * CONSTRUCT a graph and returns its NTriples representation
	 * @param query
	 * @param graph
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String constructAsNTriples(String query, String graph)
			throws FileNotFoundException, UnsupportedEncodingException;
	
	/**
	 * Loads data onto a reasoner graph.
	 * @param files
	 * @param graph
	 * @param format
	 */
	public void loadToGraph(InputStream[] files, String graph, String format);
	
	/**
	 * Loads data onto a reasoner graph.
	 * @param data
	 * @param graph
	 * @param format
	 */
	public void loadToGraph(String data, String graph, String format);
	
	/**
	 * Inserts triples in the reasoner default graph.
	 * @param queryBody
	 */
	public void insertTriples(String queryBody);
	
	/**
	 * Inserts triples in a graph.
	 * @param queryBody
	 */
	public void insertTriplesToGraph(String queryBody, String graph);
	
	/**
	 * Removes a graph of an avatar from the reasoner.
	 * @param avatarURI
	 */
	public void removeReasonerForAvatar(String avatarURI);

	/**
	 * Returns a list of available functionalities for a graph.
	 * @param avatarURI
	 * @return
	 */
	public List<String> getAvailableFunctionalities(String avatarURI);
	
	/**
	 * Returns a list of partially available functionalities for a graph.
	 * @param avatarURI
	 * @return
	 */
	public Map<String,String[]> getIncompleteFunctionalities(String avatarURI);

	/**
	 * Inserts a functionality of a given type onto an avatar graph.
	 * @param functionalityInstance
	 * @param functionalityType
	 * @param avatarURI
	 */
	public void insertNewFunctionalityForAvatar(String functionalityInstance,
			String functionalityType, String avatarURI);	
	
	/**
	 * Inserts functionalities each of a given type onto an avatar graph.
	 * Entry keys are instances URIs, whereas entry values are types URIs.
	 * @param functionalityInstance
	 * @param functionalityType
	 * @param avatarURI
	 */
	public void insertNewFunctionalitiesForAvatar(Map<String, String> functionalityInstanceType,
			String avatarURI);

	/**
	 * Removes a functionality from an avatar graph.
	 * @param functionalityInstance
	 * @param functionalityType
	 * @param avatarURI
	 */
	public void removeFunctionalityForAvatar(String functionalityInstance,
			String avatarURI);

	/**
	 * Convenient method to transform
	 * 1-var SPARQL bindings into a simple list.
	 * @param bindings
	 * @param varName
	 * @return
	 */
	public List<String> availableFunctBindingsAsArray(List<Map> bindings, String varName);

	public void initReasonerForAvatar(String avatarURI);

	public Map<String, String[]> incompleteFunctBindingsAsArray(List<Map> bindings, String groupByVar,
			String concatVar);

	public void registerAdaptationRules(Collection<?> rules);	
	
}

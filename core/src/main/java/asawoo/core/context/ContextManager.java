package asawoo.core.context;

import java.util.List;
import java.util.Map;

public interface ContextManager {

	/**
	 * Updates an avatar context with a raw data (numerical value, string...)
	 * @param rawData The raw data, interpretable as a string
	 * @param sourceTypeURI The URI of the source type, to allow interpreting the value accordingly
	 * @param avatarURI The URI of the avatar
	 */
	public void processRawData(Object rawData, String sourceTypeURI, String avatarURI);
	
	/**
	* Updates an avatar context with a fresh contextual instance
	* 
	* @param contextualInstanceURI The URI of the contextual instance to insert
	* @param avatarURI The URI of the avatar
	*/
	public void updateContextWithInstance(String contextualInstanceURI, String avatarURI);

	/**
	 * Answers a purpose-based adaptation question
	 * 
	 * @param adaptationPurpose The adaptation purpose URI (cf. SemanticReasonerConfiguration for static URis to use)
	 * @param adaptedFunctionality The functionality concerned by the adaptation
	 * @param avatarURI The URI of the avatar
	 * @param [strictScore] true for binary choices (e.g. to execute a functionality or not) false otherwise (composition, priority...)
	 * 					
	 * @return An URI corresponding to the optimal adaptation choice (e.g. a functionality, a protocol, ...)
	 */
	public List<String> getPurposeBasedAnswer(String adaptationPurpose, String adaptedFunctionality, String avatarURI, Boolean... strictScore);

	/**
	 * Returns the contextual instances associated to an avatar.
	 * 
	 * @param avatarURI The URI of the avatar
	 * 
	 * @return A SPARQL result map of contextual instances
	 */
	public Map<?, ?> getContext(String avatarURI);

	@Deprecated
	public void getContext();
	
}

package asawoo.core.context;

/**
 * This class represents any semantized data pushed to the ContextManager
 * (may come from a sensor, a web service, etc.)
 * @author mt
 *
 */
public interface ContextualData {

	/**
	 * The URI of the source type.
	 * This info is required to fire transformation rules. 
	 * @return
	 */
	public String getSourceTypeURI();

	/**
	 * The URI of the data source itself.
	 * @return
	 */
	public String getSourceURI();

	/**
	 * The actual value.
	 * This is obviously required, in order to be compared 
	 * to contextualization thresholds.
	 * @return
	 */
	public String getValue();

}

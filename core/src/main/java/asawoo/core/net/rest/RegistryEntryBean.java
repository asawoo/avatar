package asawoo.core.net.rest;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 08/09/16.
 */
public class RegistryEntryBean extends FunctionalityProviderBean implements Serializable {

    // TODO might be unnecessary if we had avatarID
    private boolean local;

    // this field is required for Vertx JSon serialization
//    private String serviceId;

    public RegistryEntryBean() {
        super();
    }

    public RegistryEntryBean(RegistryEntryBean copyBean) {
        super();
        Map<String, FunctionalityRequest> frs;
        if (copyBean.getMethods() != null) {
            frs = new HashMap<>();
            copyBean.getMethods().forEach((methodName, fr) -> {
                frs.put(methodName, new FunctionalityRequest(fr.url, fr.verb, fr.parameters));
            });
        } else {
            frs = null;
        }

        Map<String, Object> properties = null;
        if (copyBean.getServiceProperties() != null) {
            properties = new HashMap<>(copyBean.getServiceProperties()); // !! hazardous to copy "Object"
        }

        this.local = copyBean.isLocal();
        this.avatarId = copyBean.getAvatarId();
        this.name = copyBean.getName();
        this.description = copyBean.getDescription();
        this.interfaceName = copyBean.getInterfaceName();
        this.methods =  frs;
        this.instanceName = copyBean.getInstanceName();
        this.serviceProperties = properties;
        this.baseUri = copyBean.getBaseUri();
        this.semanticType = copyBean.getSemanticType();
        this.exposable = copyBean.isExposable();
    }

    // TODO add a constructor with a complete list of fields ?

    public RegistryEntryBean(boolean local, FunctionalityProviderBean functionalityBean) {
//        super(functionalityBean,
//                functionalityBean.getAvatarId(),
//                functionalityBean.getFunctionalityProvider(),
//                functionalityBean.getInstanceName(),
//                functionalityBean.getBaseUri(),
//                functionalityBean.getServiceProperties());
        super();
        this.local = local;
        this.avatarId = functionalityBean.getAvatarId();
        this.name = functionalityBean.getName();
        this.interfaceName = functionalityBean.getInterfaceName();
        this.instanceName = functionalityBean.getInstanceName();
        this.description = functionalityBean.getDescription();
        this.semanticType = functionalityBean.getSemanticType();
        this.baseUri = functionalityBean.getBaseUri();
        this.methods = functionalityBean.getMethods();
        this.serviceProperties = functionalityBean.getServiceProperties();
        this.exposable = functionalityBean.isExposable();
    }


    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

//    public String getServiceId() {
//        // TODO compute a string ID based on URI
//        return getUri().getHost()+FUNC_METHOD_SEPARATOR+getFuncNameMethod();
//    }

    /*
    @Override
    public int hashCode() {
        // TODO compute an hash based on URI, functionality name and method ?
//        int hash = getUri().hashCode() + 3*(getFuncNameMethod().hashCode());
        return super.hashCode();
    }
    */

}

package asawoo.core.net.rest;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityRequest;
import com.google.common.collect.Multimap;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 25/05/16.
 */
public interface RestServiceRegistry {

    String REGISTRY_SYSTEM_FUNCTIONALITY = "asawoo.rest.registry";

    String REST_REGISTRY_MAPPING = "/registry";


    /**
     *
     * @return
     */
    Map<String, Multimap<String, RegistryEntryBean>> getAllFunctionalities();

    Map<String, Multimap<String, RegistryEntryBean>> getFunctionalities(boolean isLocal);

    // TODO + filter features

    Set<RegistryEntryBean> getRegistryEntriesByFunctionality(String functionalityName);

    Set<RegistryEntryBean> getFilteredRegistryEntriesByFunctionality(String functionalityName, Map<String, Object> propertiesFilter);

    /**
     *
     * @param functionalityClass
     * @param method
     * @return REST uri to call the given method
     */
    Set<FunctionalityRequest> getUriByFunctionalityMethod(Class<? extends FunctionalityInterface> functionalityClass, Method method);

    /**
     *
     * @param functionalityName
     * @param methodName
     * @return REST uri to call the given method
     */
    Set<FunctionalityRequest> getUriByFunctionalityMethod(String functionalityName, String methodName);

    /**
     *
     * @param registryEntry
     */
    void publishUri(RegistryEntryBean registryEntry);

    /**
     *
     * @param functionality
     */
    void removeService(FunctionalityProviderBean functionality);

    /**
     *
     * @param uri, could be a complete or partial (e.g., just the path)
     * @return
     */
    String findFunctionalityNameByUri(String uri);

    /**
     * @param methodPath, partial path
     * @param functionalityInterface functionality interface class name
     * @return a set of URIs matching methodPath and functionalityInterface
     */
    Set<String> findFullUriByFunctionalityAndMethodPath(String functionalityInterface, String methodPath);

    /**
     * Registers a system functionality with its REST route
     * @param systemFunctionality
     * @param functionalityRequest
     */
    void registerSystemRoute(String systemFunctionality, FunctionalityRequest functionalityRequest);

    /**
     *
     * @return a multimap of system URIs
     */
    Multimap<String, FunctionalityRequest> getSystemRoutes();

    /**
     * Removes the system root for systemFunctionality at the specified url. If url is null then delete all routes for this systemFunctionality
     * @param systemFunctionality
     * @param url
     */
    void unregisterSystemRoute(String systemFunctionality, String url);

    // TODO add a method to unregister a specific route (e.g., avatars/avatarId)


    // TODO update, remove

    void addListener(RestRegistryListener listener);

    void removeListener(RestRegistryListener listener);

}

package asawoo.core.net.rest;/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 29/09/16.
 * TODO add functionalityName to get filtered notifications
 */
public interface RestRegistryListener {

    void onServiceAdded(RegistryEntryBean reb);

    void onServiceModified(RegistryEntryBean reb);

    void onServiceRemoved(RegistryEntryBean reb);

}

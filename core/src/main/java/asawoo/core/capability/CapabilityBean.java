/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.capability;

import asawoo.core.functionality.FunctionalityProviderBean;

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr>
 * TODO make this class immutable ? to avoid hashCode change ?
 */
public class CapabilityBean {

    private String name;

    private String description;

    private FunctionalityProviderBean functionality;

    public CapabilityBean(FunctionalityProviderBean functionality, String name, String description) {
        this.functionality = functionality;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FunctionalityProviderBean getFunctionality() {
        return functionality;
    }

    public void setFunctionality(FunctionalityProviderBean functionality) {
        this.functionality = functionality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CapabilityBean)) return false;

        CapabilityBean that = (CapabilityBean) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return getFunctionality().equals(that.getFunctionality());

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + getFunctionality().hashCode();
        return result;
    }
}

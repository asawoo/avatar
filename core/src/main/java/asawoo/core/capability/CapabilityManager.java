/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.capability;

import java.util.Collection;

import asawoo.core.annotation.Capability;
import asawoo.core.util.CollectionListener;
import asawoo.core.util.ListenableCollection;

/**
 * The {@code CapabilityManager} is responsible for managing capabilities
 * exposed by an appliance. It is implemented as an OSGi service.
 *
 * @author lesommer
 * @version 0.1.0
 */

public interface CapabilityManager extends ListenableCollection<CapabilityBean, CollectionListener<CapabilityBean>> {

    /**
     * Returns the capabilities of the appliance.
     *
     * @return The capabilities of the applicance.
     */
    public Collection<CapabilityBean> getCapabilities();

    /**
     * Returns the capabilities offered by the provider specified as parameter,
     * or null if it is not a capability provider.
     *
     * @param p A capability provider
     * @return the capabilities offered by the provider specified as parameter,
     * or null if it is not a capability provider.
     */
    public Collection<CapabilityBean> getCapabilities(Object p);
    
    /**
     * Returns the providers that offer the capability specified as parameter.
     * @param c A capability.
     * @return The providers that offer the capability specified as parameter.
     */
    public Collection<Object> getCapabilityProviders(CapabilityBean c);

    /**
     * Returns the capability providers of the appliance.
     *
     * @return The capability providers of the appliance.
     */
    public Collection<Object> getCapabilityProviders();

    /**
     * Adds all the capabilities offered by the provider specified as parameter.
     *
     * @param p The capability provider.
     */
    public void addCapabilityProvider(Object p);

    /**
     * Removes all the capabilites offered by the provider p.
     *
     * @param p The capability provider.
     */
    public void removeCapabilityProvider(Object p);
    
    /**
     * Return all the capabilities formated in Owl.
     * @return All the capabilities formated in Owl.
     */
    public String getOwlCapabilities();

    /**
     * Execute a capability if exist.
     * @param capability The capability
     */
    public Object executeCapability(String capability/*, Class<T>... args*/);
}

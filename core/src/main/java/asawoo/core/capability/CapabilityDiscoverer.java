package asawoo.core.capability;

import java.util.Collection;

import asawoo.core.annotation.Capability;

public class CapabilityDiscoverer {

	private CapabilityManager capabilityManager;
	private Collection<CapabilityBean> applianceCapabilities;
    
    /**
	 * @param capabilityManager
	 */
	public CapabilityDiscoverer(CapabilityManager capabilityManager) {
		super();
		this.capabilityManager = capabilityManager;
	}



	public Collection<CapabilityBean> getCapabilities(){
    	this.applianceCapabilities = this.capabilityManager.getCapabilities();
    	return this.applianceCapabilities;
    }

}

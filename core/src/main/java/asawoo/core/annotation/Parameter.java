package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A {@code Parameter} describe the parameter used by a {@code Method}.
 *
 * @author desmargez
 * @version 0.1.0
 */
@Target(value = ElementType.PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Parameter {
	/**
	 * Parameter name.
	 * 
	 * @return The parameter name.
	 */
	public String name(); // Necessary because it cannot be introspected

	/**
	 * Parameter semantic description
	 * 
	 * @return The parameter semantic description (unit, range, etc...).
	 */
	public String description() default "";

}

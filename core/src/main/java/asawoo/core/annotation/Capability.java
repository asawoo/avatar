/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A {@code Capability} is described using a small number of attributes, each
 * having a simple, atomic value. An attribute indicates an ability to generate
 * or process a particular type of data or to perform a specific action.
 *
 * @author lesommer, desmargez
 * @version 0.1.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
//(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Capability {

    /**
     * The name of the capability.
     *
     * @return The name of the capability.
     */
    public String name() default "";

    /**
     * The attributes that characterized the capability.
     *
     * @return The attributes characterizing the capability.
     */
    //public Method method() default null;

    /**
     * A description of the capability. Can be a semantic, or a textual
     * description.
     *
     * @return The description of the capability.
     */
    public String description() default "";
	
	/**
	 * The Functionality implemented by the method
	 */
	//public FunctionalCapability functionalCapability();
    public String functionality();
}

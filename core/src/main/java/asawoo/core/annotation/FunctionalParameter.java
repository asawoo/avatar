/**
 * 
 */
package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Functionality parameter
 * 
 * @author desmargez
 * @version 0.1.0
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface FunctionalParameter {
	
	/**
	 * The name of the functional parameter.
	 * 
	 * @return The name of the functional parameter.
	 */
	public String name() default "";
}

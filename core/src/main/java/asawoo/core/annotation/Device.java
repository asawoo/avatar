/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identification of a device using 4 attributes (productId, productName,
 * vendordId, vendorName).
 * @author lesommer
 * @version 0.1.0
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Device {
    /**
     * The ID of the product.
     * @return The ID of the product.
     */
    public String productId();
    /**
     * The name of the product.
     * @return The name of the product.
     */
    public String productName();
    /**
     * The ID of the vendor.
     * @return The ID of the vendor.
     */
    public String vendorId();
    /**
     * The name of the vendor.
     * @return The name of the vendor.
     */
    public String vendorName();    
}

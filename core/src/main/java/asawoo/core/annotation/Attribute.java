/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An {@code Attribute} is used to characterized a {@code Capability}.
 *
 * @author lesommer
 * @version 0.1.0
 */

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Attribute {
    /**
     * Returns the name of the attribute.
     * 
     * @return The name of the attribute.
     */
    public String name() default "";
	
    /**
     * The value of the attribute {@code name}.
     * @return The value of the attribute {@code name}.
     */
    public String value() default "";
	
    /**
     * The type of the value of the attribute {@code name}.
     * @return The type of the value of the attribute {@code name}.
     */
    public String type() default"";
}

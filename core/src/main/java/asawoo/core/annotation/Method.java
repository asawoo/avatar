package asawoo.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * A {@code Method} describe the method used to implement a {@code Capability}.
 *
 * @author desmargez
 * @version 0.1.0
 */

@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Method {

	/**
	 * The semantic description of the method 
	 *
	 * @return The return value description
	 */
	public String description() default "";
	
	/**
	 * The semantic description of returned value if any
	 *
	 * @return The return value description
	 */
	public String returns () default "";

	/**
     * List of parameters.
     * 
     * return the list of parameters.
     */
	public Parameter[] parameters() default {};
}

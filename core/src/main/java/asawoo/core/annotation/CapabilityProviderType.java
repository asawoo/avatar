/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

package asawoo.core.annotation;

/**
 * The type of a {@code Capability}
 * @author lesommer
 */
public enum CapabilityProviderType {
    NONE("NONE"),
    PROCESSOR("PROCESSOR"),
    MEMORY("MEMORY"),
    STORAGE("STORAGE"),
    NETWORK("NETWORK"),
    DISPLAY("DISPLAY"),
    /** A sensor  */
    SENSOR("SENSOR"),
    /** An actuator  */
    ACTUATOR("ACTUATOR");
    
    private String key;
    
    private CapabilityProviderType(String key) {
    	this.key = key;
    }
    
    public String toString() {
    	return this.key;
    }
}

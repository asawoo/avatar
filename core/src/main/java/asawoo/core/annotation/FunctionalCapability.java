package asawoo.core.annotation;
/**
 * The name of a functionality implemented by a Capability
 * @author desmargez
 * @version 0.1.0
 *
 */
public @interface FunctionalCapability {
	
	/**
	 * The name of the functional capability.
	 * 
	 * @return The name of the functional capability.
	 */
	public String name() default "";
}

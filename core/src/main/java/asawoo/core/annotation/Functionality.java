/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

package asawoo.core.annotation;

import java.lang.annotation.*;

/**
 * This annotation is used to describe a functionality. A functionality allows
 * to perform a set of actions on an appliance.
 * 
 * @author lesommer
 * @version 0.1.0
 */

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Functionality {

    /**
     * The name of the functionality.
     *
     * @return The name of the functionality.
     */
    public String name() default "";

    /**
     * Returns true if the functionality can be exposed by a Web service, and
     * false otherwise.
     * 
     * @return true if the functionality can be exposed by a Web service, and
     *         false otherwise.
     */
    public boolean exposable() default true;
    
    /**
     * Returns a description of the functionality
     * @return a description of the functionality
     */
    public String description() default "";
    
//    /**
//     * The attributes that characterized the capability.
//     *
//     * @return The attributes characterizing the capability.
//     */
//    public Attribute[] attributes() default {};

    /**
     *
     * @return a URI pointing to the semantic type of this functionality
     */
    String semanticType() default "";

}

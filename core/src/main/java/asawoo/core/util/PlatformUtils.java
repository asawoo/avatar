package asawoo.core.util;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.config.AsawooConfiguration;

import java.net.*;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 27/01/17.
 */
public class PlatformUtils {

    /**
     * TODO make sure hostname is the right one (i.e., bound to the right network interface)
     *
     * @return
     */
    public static String getHostname() {
        String host = System.getenv().getOrDefault("HOSTNAME", "localhost");
        try {
            String localhostName = InetAddress.getLocalHost().getHostName();
            host = localhostName;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return host;

    }

    public static final String getIpAddress(String networkInterface, boolean ipv6) throws SocketException {
        NetworkInterface netint = NetworkInterface.getByName(networkInterface);
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();

        while (inetAddresses.hasMoreElements()) {
            InetAddress address = inetAddresses.nextElement();
            if ((ipv6 && address instanceof Inet6Address)
                    || (!ipv6 && address instanceof Inet4Address)) {
                return address.getHostAddress();
            }
        }
        // not found or not v4 / v6 compliant
        return null;
    }

    public static final String getIpAddress() throws SocketException {
        return getIpAddress(AsawooConfiguration.getInterfaceName(), AsawooConfiguration.isIpv6());
    }

}

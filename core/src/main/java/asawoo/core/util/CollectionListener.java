/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.util;

/**
 * 
 * @author lesommer
 * @version 0.1.0
 */

/**
 * A {@code CollectionListener} object makes it possible to be notified about
 * the changes that can occur on a {@code ListenableCollection}.
 *
 * @author $Author: lesommer $
 * @version $Revision: 1.1 $
 */

public interface CollectionListener<E> {

    void onAdd(E elt);
    
    void onRemove(E elt);
    
    void onUpdate(E elt);
	
}
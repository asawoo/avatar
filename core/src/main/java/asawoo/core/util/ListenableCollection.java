/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

package asawoo.core.util;

import java.util.Collection;

/**
 * A listenable collection is an object whose modifications (add, remove,
 * update) notified to listeners.
 *
 * @author lesommer
 * @version 0.1.0
 */
public interface ListenableCollection<E, L extends CollectionListener<E>> {

    /**
     * Adds a new {@code CollectionListener} that needs to be notified whenever
     * this object is used.
     *
     * @param listener The listener to add.
     */
    public void addListener(L listener);

    /**
     * Removes the specified {@code CollectionListener} from the list of
     * listeners of this {@code ListenableCollection} object.
     *
     * @param listener The listener to remove.
     */
    public void removeListener(L listener);

    /**
     * Returns the list of listeners.
     *
     * @return The list of listeners.
     */
    public Collection<L> getListeners();
}

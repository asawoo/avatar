package asawoo.core.util;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.annotation.Functionality;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityProviderBean;
import com.google.common.base.Predicates;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.ComponentInstance;
import org.apache.felix.ipojo.Factory;
import org.apache.felix.ipojo.InstanceManager;
import org.apache.felix.ipojo.Pojo;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.net.URI;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 04/11/15.
 */
public class AsawooUtils {

    private static Logger logger = LoggerFactory.getLogger(AsawooUtils.class);

    public static boolean isNotEmpty(String s) {
        return s != null && !s.isEmpty();
    }

    public static char INSTANCE_NAME_SEPARATOR = '-';

    public static final String ASAWOO_FUNCTIONALITY_NAMESPACE = "asawoo.functionality";
    public static final String ASAWOO_FUNCTIONALITY_NAME_FILTER_PROPERTY = "name";
    public static final String ASAWOO_CAPABILITY_NAMESPACE = "asawoo.capability";
    public static final String ASAWOO_CAPABILITY_NAME_FILTER_PROPERTY = "name";
    public static final String ASAWOO_CAPABILITY_FACTORY_FILTER_PROPERTY = "factory";
    public static final String ASAWOO_CAPABILITY_DEVICE_FILTER_PROPERTY = "device";

    /**
     * Loads avatar configuration embedded in bundle
     *
     * @param bundleContext
     * @param configFilename
     * @return
     * @throws Exception
     */
    public static AvatarConfiguration loadAvatarConfiguration(BundleContext bundleContext, String configFilename) throws Exception {
        // load conf
        InputStream is = bundleContext.getBundle().getResource(configFilename).openConnection().getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        String json = sb.toString();

        JsonObject config = new JsonObject(json);

        return new AvatarConfiguration(config);

    }

    public static String getMethodPathFromURI(URI uri) {
        String fullPath = uri.getPath();
        // truncate path by removing <appliance> and <functionality name> parts
        int indexOfApplianceFuncSeparator =  fullPath.indexOf("/", 1);
        return fullPath.substring(fullPath.indexOf("/", indexOfApplianceFuncSeparator+1));
    }

    @Deprecated
    public static String buildBasePath(String functionalityName) {
        // APPLIANCE NAME
        String applianceName = AsawooConfiguration.getApplianceName();
        String appliancePath = applianceName != null ? "/"+applianceName : "";

        // Functionality name
        String basePath = appliancePath
                            +functionalityName != null ? "/"+functionalityName.toLowerCase() : "";

        return basePath;
    }

    public static <K, V> Map<K, V> dictionaryToMap(Dictionary<K, V> dictionary) {
        if (dictionary == null) {
            return null;
        }
        Map<K, V> map = new HashMap<>(dictionary.size());
        Enumeration<K> keys = dictionary.keys();
        while (keys.hasMoreElements()) {
            K key = keys.nextElement();
            map.put(key, dictionary.get(key));
        }
        return map;
    }

    public static boolean isFunctionalityFactory(Factory factory) {
        String[] specs = factory.getComponentDescription().getprovidedServiceSpecification();
        boolean isFunctionalityFactory = false;

        for (String s : specs) {
            if (s.equals(FunctionalityInterface.class.getName())) {
                isFunctionalityFactory = true;
            }
        }

        return isFunctionalityFactory;
    }

    public static <F extends Pojo & FunctionalityInterface> String buildFunctionalityInstanceUri(FunctionalityInterface object, String functionalityName) {
        F functionality = (F) object;
        InstanceManager me = (InstanceManager) functionality.getComponentInstance();
        String ipojoInstanceName = me.getInstanceDescription().getName();

        // retrieve matching service
        try {
            ServiceReference[] refs =
                    me.getContext().getServiceReferences(FunctionalityInterface.class.getName(),
                            "(instance.name=" + ipojoInstanceName +")"); // TODO we can add a functionality=functionalityName for finer selection
            if (refs != null) {
                ServiceReference svcRef = refs[0]; // first match
                String avatarId = (String) svcRef.getProperty(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID);
                String asawooInstanceName = getInstanceNameFromiPojo(avatarId, ipojoInstanceName);
                String baseUri = buildFunctionalityBaseUri(avatarId, functionalityName);
                return baseUri + "/" + asawooInstanceName.toLowerCase();
            } else {
              logger.error("No service reference found for instance name " + ipojoInstanceName);
              // NOTE Alban : dirty fallback. But sometimes service reference is not found :(
              return "/functionalities-instances/" + functionalityName + '-' + ipojoInstanceName;
            }
        } catch (InvalidSyntaxException e) {
            // Should not happen
        }
        // else something went wrong beyond this point
        return null;

    }

    /**
     * Given an avatar Id and a functionality, builds a relative URI
     * @param avatarId
     * @param functionalityName
     * @return
     */
    public static String buildFunctionalityBaseUri(String avatarId, String functionalityName) {

        // TODO get rid of appliance path ?
        // APPLIANCE NAME
        String applianceName = AsawooConfiguration.getApplianceName();
        String appliancePath = applianceName != null ? "/"+applianceName : "";

        // avatar ID (avoid redunduncy when avatarId = applianceName)
        String avatarPath = (avatarId != null && !avatarId.equals(applianceName)) ? "/"+avatarId.toLowerCase() : "";

        // Functionality name
        String functionalityPath = functionalityName != null ? "/"+functionalityName.toLowerCase() : "";

//        // instance name
//        String instancePath = instanceName != null ? "/"+instanceName.toLowerCase() : "";

        // GENERATE URL
        String url = appliancePath+avatarPath+functionalityPath;

        return url;
    }

    /**
     * Same as @buildFunctionalityBaseUri but prefixes with IP server and port
     * @param avatarId
     * @param functionalityName
     * @return
     */
    public static String buildFullFunctionalityBaseUri(String avatarId, String functionalityName) {
        // HOSTNAME
        String host = null;
        try {
            host = PlatformUtils.getIpAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (host == null) {
            host = PlatformUtils.getHostname();
        }


        // GENERATE URL
        String url = "http://"+host+":"+AsawooConfiguration.getHttpPort()+buildFunctionalityBaseUri(avatarId, functionalityName);
        return url;
    }

    /**
     * Extracts avatar URI portion from baseURI
     * @param functionality
     * @return
     */
    public static String getAvatarURI(FunctionalityProviderBean functionality) {
        String functionalityName = functionality.getName();
        String baseUri = functionality.getBaseUri();

        return baseUri.substring(0, baseUri.length() - functionalityName.length() - 1);

    }

    /**
     * returns baseUri/instance/method
     *
     * @param functionality
     * @param methodPath
     * @return
     */
    public static String getFullURI(FunctionalityProviderBean functionality, String methodPath) {

        return functionality.getBaseUri() + "/" + functionality.getInstanceName() + methodPath;

    }

    public static String buildAvatarURI(String avatarId) {
        String host = null;
        try {
            host = PlatformUtils.getIpAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (host == null) {
            host = PlatformUtils.getHostname();
        }
        return "http://"+host+":"+AsawooConfiguration.getHttpPort()+"/"+avatarId;
    }

    public static String absoluteBaseURI() {
        String host = null;
        try {
            host = PlatformUtils.getIpAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (host == null) {
            host = PlatformUtils.getHostname();
        }
        return "http://"+host+":"+AsawooConfiguration.getHttpPort();
    }

    public static String getInstanceNameFromiPojo(String avatarId, String ipojoInstanceName) {
        if (ipojoInstanceName.startsWith(avatarId+INSTANCE_NAME_SEPARATOR)) {
            return ipojoInstanceName.substring(avatarId.length() + 1);
        } else {
            // assume iPojo instance name is derived from classname. Just keep the simple name
            int lastSeparatorIndex = ipojoInstanceName.lastIndexOf(".");
            return ipojoInstanceName.substring(lastSeparatorIndex+1);
        }
    }

    public static String buildIpojoInstanceName(String avatarId, String instanceName) {
        return avatarId+INSTANCE_NAME_SEPARATOR+instanceName;
    }


    public static String getFunctionalitySemanticType(Class inter) {
        return ((Functionality) inter.getAnnotation(Functionality.class)).semanticType();
    }
}

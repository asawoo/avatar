/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * {@code Table2} is a two-column table.
 * 
 * @author lesommer
 * @version 0.1.0
 */
public class Table2<A, B> {
    ArrayList<A> col1;
    ArrayList<B> col2;

    /**
     * Creates a new two-column table.
     */
    public Table2() {
	this.col1 = new ArrayList<>();
	this.col2 = new ArrayList<>();
    }

    /**
     * Adds a new row in the table composed of two elements. Only one line of
     * type (a,b) can exist in the table.
     * 
     * @param a The element of the first column
     * @param b The element of the second column
     */
    public synchronized void put(A a, B b) {
		if (!(this.col1.contains(a) && this.col2.contains(b))) {
			this.col1.add(a);
			this.col2.add(b);
		}
    }

    /**
     * Removes the row at the index specified as parameter.
     * 
     * @param index The index of the row that must be removed.
     */
    public synchronized void removeRow(int index) {
		this.col1.remove(index);
		this.col2.remove(index);
    }

    /**
     * Removes the row (a,b), where a and b are specified as parameters.
     * 
     * @param a The element of the first column.
     * @param b The element of the second column.
     */
    public synchronized void removeRow(A a, B b) {
	this.removeRow(this.getIndex(a, b));
    }

    /**
     * Removes rows whose elements in the first column are equal to parameter a.
     * 
     * @param a element of type A (first column)
     */
    public synchronized void removeRowsA(A a) {
	Iterator<A> iterCol1 = this.col1.iterator();
	Iterator<B> iterCol2 = this.col2.iterator();
	while (iterCol1.hasNext()) {
	    A eltCol1 = iterCol1.next();
	    B eltCol2 = iterCol2.next();
	    if (eltCol1.equals(a)) {
		iterCol1.remove();
		iterCol2.remove();
	    }
	}
    }

    /**
     * Removes rows whose elements in the second column are equal to parameter
     * b.
     * 
     * @param b element of type B (second column)
     */
    public synchronized void removeRowsB(B b) {
	Iterator<A> iterCol1 = this.col1.iterator();
	Iterator<B> iterCol2 = this.col2.iterator();
	while (iterCol1.hasNext()) {
	    A eltCol1 = iterCol1.next();
	    B eltCol2 = iterCol2.next();
	    if (eltCol1.equals(b)) {
		iterCol1.remove();
		iterCol2.remove();
	    }
	}
    }

    /**
     * Returns the index of the row (a,b) or -1 if the row does not exist.
     * 
     * @param a The first element of the row
     * @param b The second element of the row
     * @return the index of the row (a,b) or -1 if the row does not exist.
     */
    public int getIndex(A a, B b) {
	int i = 0;
	while (i < this.col1.size()
		&& !(this.col1.get(i).equals(a) && this.col2.get(i).equals(b))) {
	    i++;
	}
	int index = this.col1.get(i).equals(a) && this.col2.get(i).equals(b) ? i
		: -1;
	return index;
    }

    /**
     * Returns the elements of the first column associated with the elements of
     * the second column are equal to the parameter b.
     * 
     * @param b the
     * @return the elements of the first column associated with the elements of
     *         the second column are equal to the parameter b.
     */
    public Collection<A> getRowsA(B b) {
	ArrayList<A> result = new ArrayList<>();
	Iterator<A> iterCol1 = this.col1.iterator();
	Iterator<B> iterCol2 = this.col2.iterator();
	while (iterCol2.hasNext()) {
	    A eltCol1 = iterCol1.next();
	    B eltCol2 = iterCol2.next();
	    if (eltCol2.equals(b)) {
		result.add(eltCol1);
	    }
	}
	return result;
    }

    /**
     * Returns the elements of the second column associated with the elements of
     * the first column are equal to the parameter a.
     * 
     * @param b the
     * @return the elements of the second column associated with the elements of
     *         the first column are equal to the parameter a.
     */
    public Collection<B> getRowsB(A a) {
	ArrayList<B> result = new ArrayList<>();
	Iterator<A> iterCol1 = this.col1.iterator();
	Iterator<B> iterCol2 = this.col2.iterator();
	while (iterCol1.hasNext()) {
	    A eltCol1 = iterCol1.next();
	    B eltCol2 = iterCol2.next();
	    if (eltCol1.equals(a)) {
		result.add(eltCol2);
	    }
	}
	return result;
    }


    public boolean containsA(A a){
	return this.col1.contains(a);
    }

    public boolean containsB(B b){
	return this.col2.contains(b);
    }
}

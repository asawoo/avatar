/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.functionality;

import asawoo.core.util.CollectionListener;

/**
 * A {@code FunctionalityManagerListener} is an object that can be notified of the
 * modifications of functionalities in an appliance, such as the addition or the
 * removal of functionalities.
 * @author lesommer
 * @version 0.1.0
 */
public interface FunctionalityManagerListener extends CollectionListener<FunctionalityProviderBean>{

}

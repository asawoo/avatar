package asawoo.core.functionality;

import java.util.Dictionary;


public interface FunctionalityFactory {
	public FunctionalityInterface instantiate(Dictionary<String,String> props);		
}

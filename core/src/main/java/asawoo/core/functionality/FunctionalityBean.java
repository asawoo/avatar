package asawoo.core.functionality;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 30/03/17.
 */
public class FunctionalityBean {

    protected String name;
    protected String description;
    protected String interfaceName;
    protected transient Class<? extends FunctionalityInterface> implementedFunctionality;

    /**
     * URI formatted.
     * Example: http://asawoo/vocab/Led
     */
    protected String semanticType;
    // <methodName, URI> TODO !! caution, does not support two methods with same name (but easier to serialize)
    protected Map<String, FunctionalityRequest> methods;

    protected boolean exposable;

    /* TODO keep default empty constructor ? */
    public FunctionalityBean() {

    }

    public FunctionalityBean(String name, String description, Class<? extends FunctionalityInterface> implementedFunctionality, String semanticType, Map<String, FunctionalityRequest> methods, boolean exposable) {
        this.implementedFunctionality = implementedFunctionality;
        this.interfaceName = implementedFunctionality.getName(); // TODO get FQN or simple name instead ?
        this.name = name != null ? name : interfaceName;
        this.description = description;
        this.methods = methods;
        this.semanticType = semanticType;
        this.exposable = exposable;
    }

    /**
     * Identity
     * @param fb
     */
    public FunctionalityBean(FunctionalityBean fb) {
        this(fb.getName(), fb.getDescription(), fb.getImplementedFunctionality(), fb.getSemanticType(), fb.getMethods(), fb.isExposable());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public Class<? extends FunctionalityInterface> getImplementedFunctionality() {
        return implementedFunctionality;
    }

    public String getSemanticType() {
        return semanticType;
    }

    public void setSemanticType(String semanticType) {
        this.semanticType = semanticType;
    }

    public Map<String, FunctionalityRequest> getMethods() {
        return methods;
    }

    public void setMethods(Map<String, FunctionalityRequest> methods) {
        this.methods = methods;
        // init base URI if necessary
        /*
        // TODO elsewhere
        if (baseUri == null
                && this.methods != null
                && !this.methods.isEmpty()) {
            Map.Entry<String, FunctionalityRequest> entry = methods.entrySet().iterator().next();
            String methodUri = entry.getValue().url;
            System.out.println("METHOD URI :: "+methodUri);
            int endOfPathIdx = methodUri.indexOf(getName().toLowerCase()) + getName().length();
            baseUri = methodUri.substring(0,endOfPathIdx);
        }
        */
    }

    public boolean isExposable() {
        return exposable;
    }

    @Override
    public String toString() {
        return getName();
    }
}

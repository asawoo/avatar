package asawoo.core.functionality;

import java.util.ArrayList;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public abstract class FunctionalityActivator implements BundleActivator {
	
    protected ArrayList<ServiceRegistration> services=new ArrayList<>();
        

    protected void registerFactory(BundleContext context,Class<?> factoryClass) {
    	FunctionalityFactory factory;
		try {
			factory = (FunctionalityFactory) factoryClass.newInstance();
	    	ServiceRegistration reg=context.registerService(factoryClass.getName(),factory, new Hashtable<String,String>());    	
	    	services.add(reg);    	
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    			
    }
    
    @Override    
    public void stop(BundleContext context) throws Exception {
    	for (ServiceRegistration reg: services) {
    		reg.unregister();
    	}
    	// TODO Check life cycle to make sure we don't remove still active bundles
    	services.clear();
    }
}

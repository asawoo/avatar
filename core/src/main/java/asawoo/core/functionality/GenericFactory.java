package asawoo.core.functionality;

import java.util.Dictionary;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

abstract public class GenericFactory implements FunctionalityFactory {

	@Override
	public FunctionalityInterface instantiate(Dictionary<String, String> props) {		
		BundleContext context=FrameworkUtil.getBundle(this.getClass()).getBundleContext(); // CAUTION, method only available from osgi Core 4.2.0

		System.out.println("Instantiating new Functionality instance "+props.get("name"));

		FunctionalityInterface func=buildInstance(props);
		if (func==null) {
			System.out.println("ERROR: "+this.getClass().getName()+": Could not create instance");
			return null;
		}

		String exposed=props.get("exposed");
		if (exposed!=null && exposed.equals("true"))
		{
	        props.put("service.exported.interfaces", "*");

	        //props.put("service.exported.configs", "org.apache.cxf.ws");
	        
	        props.put("service.exported.configs", "org.apache.cxf.rs");
	        //props.put("service.exported.intents", "HTTP"); 
		}
		
		Class<?>[] interfaces=func.getClass().getInterfaces();		
		for (Class<?> c:interfaces) {
			if (FunctionalityInterface.class.isAssignableFrom(c)) {
				String url="/"+c.getName()+"/"+props.get("name");
				//props.put("org.apache.cxf.ws.httpservice.context", url);
				props.put("org.apache.cxf.rs.httpservice.context", url);
				System.out.println("Registering functionality interface "+url);
				context.registerService(c.getName(),func, props);
			}			
		}
		return func;
	}
	
	abstract public FunctionalityInterface buildInstance(Dictionary<String, String> props);
	
}

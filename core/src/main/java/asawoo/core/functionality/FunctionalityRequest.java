package asawoo.core.functionality;

import java.util.ArrayList;
import java.util.List;

public class FunctionalityRequest {
	public String url;
	public String verb;
	public List<String> parameters;

	public FunctionalityRequest() {

	}

	public FunctionalityRequest(String url,String verb,List<String>parameters) {
		this.url=url;
		this.verb=verb;
		this.parameters=new ArrayList<String>();
		if (parameters!=null) {
			for (String p:parameters) {
				this.parameters.add(p);
			}
		}		
	}
}

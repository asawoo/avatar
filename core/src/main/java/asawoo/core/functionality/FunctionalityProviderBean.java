/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.functionality;

import asawoo.core.util.AsawooUtils;

import java.util.Map;

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 29/10/15.
 * TODO make this class immutable ? to avoid hashCode change ?
 */
public class FunctionalityProviderBean extends FunctionalityBean {

    protected String avatarId;
    /**
     * Functionality base URI (i.e., from scheme up to the functionality part)
     */
    protected String baseUri;

    private transient FunctionalityInterface functionalityProvider;

    protected String instanceName;

    protected Map<String, Object> serviceProperties;

    /* TODO keep default empty constructor ? */
    public FunctionalityProviderBean() {
        super();
    }

    public FunctionalityProviderBean(FunctionalityBean fb,
                                     String avatarId,
                                     FunctionalityInterface functionalityProvider,
                                     String instanceName,
                                     String baseUri,
                                     Map<String, Object> serviceProperties) {
        super(fb);
        this.avatarId = avatarId;
        this.functionalityProvider = functionalityProvider;
        this.instanceName = instanceName;
        this.baseUri = baseUri;
        this.serviceProperties = serviceProperties;
    }

    @Deprecated
    public FunctionalityProviderBean(String name, String description, Class<? extends FunctionalityInterface> implementedFunctionality) {
        super(name, description, implementedFunctionality, null, null, true);
    }

    @Deprecated
    public FunctionalityProviderBean(String name,
                                     String description,
                                     Class<? extends FunctionalityInterface> implementedFunctionality,
                                     String avatarId,
                                     FunctionalityInterface functionalityProvider,
                                     String providerInstanceName,
                                     Map<String, Object> serviceProperties,
                                     String baseUri,
                                     String semanticType) {

        super(name, description, implementedFunctionality, semanticType, null, true);
        this.avatarId = avatarId;
        this.instanceName = providerInstanceName;
        this.baseUri = baseUri;
        this.functionalityProvider = functionalityProvider;
        this.serviceProperties = serviceProperties;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public FunctionalityInterface getFunctionalityProvider() {
        return functionalityProvider;
    }

    public Map<String, Object> getServiceProperties() {
        return serviceProperties;
    }

    /*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FunctionalityProviderBean)) return false;

        FunctionalityProviderBean that = (FunctionalityProviderBean) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return !(getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null);

    }
*/

    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public void setBaseUri(String baseUri) {
        this.baseUri = baseUri;
    }

    /*
    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }
    */

    /*
    public void addMethod(String method, FunctionalityRequest uri) {
        if (methods == null) {
            methods = new HashMap<>();
        }
        methods.put(method, uri);
    }
    */

    public FunctionalityRequest getFunctionalityUrisByMethod(String methodName) {
        return methods == null ? null : methods.get(methodName);
    }

    /**
     *
     * @return the same instance of functionality with methods URIs as absolute URIs
     */
    public FunctionalityProviderBean toAbsoluteUri() {
        for (FunctionalityRequest fr : methods.values()) {
            if (fr.url.startsWith("/")) {
                fr.url = AsawooUtils.getFullURI(this, fr.url);
            }
        }
        return this;
    }

}

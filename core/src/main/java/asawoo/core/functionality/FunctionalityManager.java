/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.core.functionality;

import asawoo.core.util.CollectionListener;
import asawoo.core.util.ListenableCollection;
import org.apache.felix.ipojo.Factory;

import java.util.Collection;
import java.util.Dictionary;
import java.util.Map;

/**
 * The {@code FunctionManager} is responsible of managing the functionalities
 * provided by an appliance. Functionalities are implemented using OSGi
 * services.
 * 
 * @author lesommer
 * @version 0.1.0
 */
public interface FunctionalityManager extends ListenableCollection<FunctionalityProviderBean, FunctionalityManagerListener> {

    /**
     * Returns the functionalities that could be provided by the appliance if
     * the functionality dependencies can be resolved (functionality and
     * capability).
     * 
     * @return The functionalities that could be provided by the appliance.
     */	
    Collection<String> getAvailableFunctionalities();

    Collection<FunctionalityBean> getAvailableFunctionalities(String avatarId);

    /**
     * Returns the functionalities that are currently provided by the appliance.
     * 
     * @return The functionalities that are currently provided by the appliance.
     */
    Collection<String> getEnabledFunctionalities();

    Collection<FunctionalityProviderBean> getFunctionalities();

    Collection<FunctionalityProviderBean> getFunctionalitiesByAvatar(String avatarId);

    @Deprecated
    FunctionalityInterface getFunctionality(String name);

//    /**
//     *
//     * @param name
//     * @param func
//     */
//    @Deprecated
//    void  addFunctionality(String name,FunctionalityInterface func);

    FunctionalityInterface  spawnFunctionality(Dictionary<String,String> configuration);
    Object invoke(String instanceName,String method,Dictionary<String,String> parameters);
    FunctionalityRequest getMethodRequest(String functionality,String appliance,String method);

    /**
     *
     * @param avatarId
     * @param functionalityInterface
     * @return a bean holding functionality properties
     */
    FunctionalityProviderBean getFunctionalityByInterface(String avatarId, Class<? extends FunctionalityInterface> functionalityInterface);

    /**
     * Returns matching functionality or creates it and adds it to the manager.
     *
     * @param avatarId
     * @param implementedFunctionalityClazz
     * @param functionalityInterface
     * @param instanceName
     * @param serviceProperties
     * @return a bean holding functionality properties
     */
    FunctionalityProviderBean getOrAddFunctionality(String avatarId, Class implementedFunctionalityClazz, FunctionalityInterface functionalityInterface, String instanceName, Map<String, Object> serviceProperties);


    Collection<Factory> getImplementingFactories(String functionalityName);

    Factory getFunctionalityFactory(String factoryName);

    Collection<FunctionalityBean> getFunctionalityByFactory(String factoryName);

    Collection<FunctionalityBean> getFunctionalityByFactory(Factory factory);

    Collection<FunctionalityBean> getFunctionalityBySemanticType(String uri);

    FunctionalityBean getFunctionalityBeanByInterface(Class<? extends FunctionalityInterface> functionalityInterface);

    /**
     *
     * @param avatarId avatar Identifier that functionality relates to
     * @param func functionality bean (holding Interface fully qualified name, functionality name and description)
     */
    void addFunctionality(String avatarId, FunctionalityProviderBean func);


    /**
     * @param avatarId avatar Identifier that functionality relates to
     * @param name Functionality Interface fully qualified name
     */
    void removeFunctionality(String avatarId, String name);

    /**
     *
     * @param functionality
     */
    void removeFunctionality(FunctionalityProviderBean functionality);

    void addListener(String avatarId, FunctionalityManagerListener listener);

    void removeListener(String avatarId, FunctionalityManagerListener listener);

    void addFactoryListener(CollectionListener listener);

    void removeFactoryListener(CollectionListener listener);

    Collection<FunctionalityManagerListener> getListeners(String avatarId);


}

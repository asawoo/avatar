package asawoo.core.functionality;

import java.util.Collection;

/**
 * CollaborativeFunctionalityManager is responsible for managing the composite functionalities that are
 * made available through collaboration between avatars
 */
public interface CollaborativeFunctionalityManager {

    Collection<FunctionalityBean> getAvailableCollaborativeFunctionalities(String avatarId);
}

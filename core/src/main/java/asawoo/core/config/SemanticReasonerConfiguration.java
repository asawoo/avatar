package asawoo.core.config;

public class SemanticReasonerConfiguration {
	
	public static final String FUNCT_ONTO_FILE = "functionality.ontology.file";
	
	// Prefixes end with slash or hash
	public static final String ASAWOO_PREFIX 		= "http://liris.cnrs.fr/asawoo/";
	public static final String ASAWOO_VOCAB_PREFIX 	= ASAWOO_PREFIX + "vocab#";
	public static final String ASAWOO_FUNCT_PREFIX 	= ASAWOO_PREFIX + "functionalities#";
	public static final String ASAWOO_CTX_PREFIX 	= ASAWOO_PREFIX + "vocab/context#";
	
	// URIs do not end with a slash
	public static final String DEFAULT_GRAPH_URI 	= ASAWOO_PREFIX + "default";
	public static final String CTX_GRAPH_URI 	= ASAWOO_PREFIX + "vocab/context";

	// Vocab-related URIs
	public static final String FUNCTIONALITY_TYPE = ASAWOO_VOCAB_PREFIX + "Functionality";
	
	// Adaptation purposes URIs (fixed to ASAWoO)
	public static final String IMP_PURPOSE = ASAWOO_CTX_PREFIX + "hasSuitableCapabilityForImplementation";
	public static final String COMP_PURPOSE = ASAWOO_CTX_PREFIX + "hasSuitableFunctionalityForComposition";
	public static final String EXP_PURPOSE = ASAWOO_CTX_PREFIX + "hasExposability";
	public static final String PRTCL_PURPOSE = ASAWOO_CTX_PREFIX + "hasPreferredProtocol";
	public static final String CODE_PURPOSE = ASAWOO_CTX_PREFIX + "hasPreferredCodeLocation";
	public static final String PRIORITY_PURPOSE = ASAWOO_CTX_PREFIX + "hasPrioritizationForFunctionality";
	
	// Raw value types
	public static final String TEMPERATURE = "http://w3id.org/saref#Temperature";
	public static final String GAS = "http://w3id.org/saref#Gas";
	
	// Init raw value
	public static final String INIT_VALUE = "init";
	
	// Contextual adaptation-related properties
	public static final String EFFECTIVE_INSTANCE_PROP = ASAWOO_CTX_PREFIX + "isEffectiveInstance";
	
	//Prefixed filters on select queries to retrieve only functionality classes
	public static final String FILTER_FUNCT_CLASS	=
			"	FILTER (?functClass != asawoo:Functionality) ." 
		 	+ "	FILTER (?functClass != rdfs:Resource) ."
		 	+ "	FILTER (?functClass != owl:Thing) ."
		 	+ "	FILTER (?functClass != owl:Class) ."
		 	+ "	FILTER (?functClass != rdfs:Class) ."
			+ "	FILTER (?functClass != sosa:Procedure) ."
		 	+ "	FILTER (!isBlank(?functClass)) .";
	
	public static final String PREFIXES 			=
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
			+ "PREFIX owl: <http://www.w3.org/2002/07/owl#> \n"
			+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n"
			+ "PREFIX sosa: <http://www.w3.org/ns/sosa/> \n"
			+ "PREFIX asawoo: <" + ASAWOO_VOCAB_PREFIX + "> \n"
			+ "PREFIX asawoo-ctx: <" + ASAWOO_CTX_PREFIX + "> \n";

}

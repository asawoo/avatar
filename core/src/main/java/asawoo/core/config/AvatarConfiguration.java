package asawoo.core.config;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.Serializable;
import java.util.*;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 09/12/16.
 */
public class AvatarConfiguration implements Serializable {

    public static final String PROPERTY_AVATAR_ID = "avatarId";
    public static final String PROPERTY_AVATAR_LABEL = "avatarLabel";
    public static final String SERVICE_PROPERTY_AVATAR_ID = "service.avatar.id";
    public static final String PROPERTY_FUNCTIONALITIES = "functionalities";
    public static final String PROPERTY_FUNCTIONALITY_FACTORY = "factory.name";
    public static final String PROPERTY_CONFIGURATION = "configuration";

    private String avatarId;

    private String avatarLabel;

    private JsonObject rawConfig;

    private ListMultimap<String, Properties> functionalitiesConfigurations;


    public AvatarConfiguration(String avatarId, ListMultimap<String, Properties> functionalitiesConfigurations) {
        this.avatarId = avatarId;
        this.functionalitiesConfigurations = functionalitiesConfigurations;
    }

    public AvatarConfiguration(String avatarId, String avatarLabel, ListMultimap<String, Properties> functionalitiesConfigurations) {
        this.avatarId = avatarId;
        this.avatarLabel = avatarLabel;
        this.functionalitiesConfigurations = functionalitiesConfigurations;
    }

    /**
     * Configuration example:
     *
     * {"avatarId": "foo",
     *              "functionalities": [{"functionality.name" : "TemperatureSensor",
     *                                   "factory.name" : "com.sensor.TempSensor",
     *                                   "instance.name": "inst",
     *                                   "temp.unit": "C"
     *                                  },
     *                                  {"functionality.name" : "Led",
     *                                   "factory.name" : "bar.Led",
     *                                   "led.id": "left"
     *                                  },
     *                                  {"factory.name" : "bar.Led",
     *                                   "led.id": "right"
     *                                  }
     *                                 ]
     * }
     *
     * @param configuration
     * @throws IllegalFormatException
     */
    public AvatarConfiguration(JsonObject configuration) throws IllegalFormatException {

        String avatarId = configuration.getString(PROPERTY_AVATAR_ID);
        if (avatarId == null) {
            throw new IllegalArgumentException("JSON configuration should contain a "+PROPERTY_AVATAR_ID+" property.");
        } else {
            this.avatarId = avatarId;
            this.avatarLabel = configuration.getString(PROPERTY_AVATAR_LABEL);

            JsonArray funcConfig = configuration.getJsonArray(PROPERTY_FUNCTIONALITIES);
            if (funcConfig != null) {
                this.functionalitiesConfigurations = ArrayListMultimap.create();
                for (int i=0; i < funcConfig.size(); i++) {
                    JsonObject funcObject = funcConfig.getJsonObject(i);
                    String funcFactoryName = funcObject.getString(AvatarConfiguration.PROPERTY_FUNCTIONALITY_FACTORY);

                    Properties props = new Properties();
                    funcObject.forEach(entry -> {
                        if (!entry.getKey().equals(AvatarConfiguration.PROPERTY_FUNCTIONALITY_FACTORY))
                            props.put(entry.getKey(), entry.getValue());
                    });

                    this.functionalitiesConfigurations.put(funcFactoryName, props);

//                    List<Properties> config;
//                    if (this.functionalitiesConfigurations.containsKey(funcFactoryName)) {
//                        config = this.functionalitiesConfigurations.get(funcFactoryName);
//                        config.add(props);
//                    } else {
//                        config = new ArrayList<Properties>();
//                        config.add(props);
//                        this.functionalitiesConfigurations.put(funcFactoryName, config);
//                    }

                }

//                Set<String> functionalities = funcConfig.fieldNames();
//                functionalities.forEach(funcFactoryName -> {
//                    List<Properties> config = new ArrayList<Properties>();
//                    Object v = funcConfig.getValue(funcFactoryName);
//                    if (v instanceof Map) {
//                        JsonObject jo = new JsonObject((Map) v);
//                        Properties props = new Properties();
//                        jo.forEach(entry -> {
//                            props.put(entry.getKey(), entry.getValue());
//                        });
//                        config.add(props);
//                    } else if (v instanceof List) {
//                        JsonArray ja = new JsonArray((List) v);
//                        for (int i=0; i< ja.size(); i++) {
//                            JsonObject jo = ja.getJsonObject(i);
//                            Properties props = new Properties();
//                            jo.forEach(entry -> {
//                                props.put(entry.getKey(), entry.getValue());
//                            });
//                            config.add(props);
//                        }
//                    }
//
//                    this.functionalitiesConfigurations.put(funcFactoryName, config);
//                });
            }
        }

        rawConfig = configuration;

    }

    public String getAvatarId() {
        return avatarId;
    }

    public String getAvatarLabel() {
        return avatarLabel;
    }

    public ListMultimap<String, Properties> getFunctionalitiesConfigurations() {
        return functionalitiesConfigurations;
    }

    public Set<String> getFunctionnalities() {
        return functionalitiesConfigurations != null ?
                functionalitiesConfigurations.keySet() : null;
    }

    public List<Properties> getFunctionalityConfiguration(String functionalityFactoryName) {
        return functionalitiesConfigurations != null ?
                functionalitiesConfigurations.get(functionalityFactoryName) : null;
    }

    public JsonObject getRawConfig() {
        return rawConfig;
    }

    @Override
    public String toString() {
        if (getRawConfig() != null) {
            return getRawConfig().encodePrettily();
        } else {
            return "{ \"avatarId\":\"" + getAvatarId() + "\", \"avatarLabel\":\"" + getAvatarLabel() + "\"}";
        }
    }
}

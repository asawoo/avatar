package asawoo.core.config;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */


import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 01/06/16.
 */
public class AsawooConfiguration {

    public static final String PROPERTY_APPLIANCE_NAME = "appliance.name";

    public static final String PROPERTY_AVATAR_ID = "asawoo.avatar.id";

    public static final String PROPERTY_HTTP_PORT = "http.port";

    public static final String PROPERTY_NET_INTERFACE_NAME = "ifname";

    public static final String PROPERTY_NET_IPV6 = "ipv6";

    public static final String PROPERTY_OSGI_HTTP_SERVICE_PORT = "org.osgi.service.http.port";

    public static final String PROPERTY_WEBROOT_DIR = "asawoo.webroot.dir";

    public static final String DEFAULT_WEBROOT_DIR = "www";

    private static int HTTP_PORT;

    private static String NET_INTERFACE_NAME;

    public static final String[] RESERVED_WORDS = {"asawoo","avatar","registry",
            "wotapp", "wotapps", "dtn",
            "deviceconfigurations", "static", "lib"};

    public static String getApplianceName() {
        return System.getProperty(PROPERTY_APPLIANCE_NAME);
    }

    public static String getPlatformAvatarId() {
        String avatarId = System.getProperty(PROPERTY_AVATAR_ID);
        if (avatarId == null) {
            String applianceName = getApplianceName();
            return  applianceName == null ? getDefaultLocalAvatarId() : applianceName;
        } else {
            return avatarId;
        }

    }

    public static int getHttpPort() {
        return (HTTP_PORT != 0) ? HTTP_PORT : Integer.parseInt(System.getProperty(PROPERTY_HTTP_PORT));
    }

    public static void setHttpPort(int port) {
        System.setProperty(PROPERTY_HTTP_PORT, Integer.toString(port));
        HTTP_PORT = port;
    }

    public static String getInterfaceName() {
        if (NET_INTERFACE_NAME == null) {
            String ifnameProperty = System.getProperty(PROPERTY_NET_INTERFACE_NAME);
            if (ifnameProperty == null) {
                // default
                try {
                    Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
                    while (nets.hasMoreElements()) {
                        NetworkInterface iface = nets.nextElement();
                        if (!iface.isLoopback() && iface.isUp()) {
                            NET_INTERFACE_NAME = iface.getName();
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }
            } else {
                // set
                NET_INTERFACE_NAME = ifnameProperty;
//                if (NetworkInterface.getByName(ifnameProperty) != null) {
//
//                }
            }

        }
        return NET_INTERFACE_NAME;
    }

    public static boolean isIpv6() {
        if (System.getProperties().containsKey(PROPERTY_NET_IPV6)) {
            return Boolean.parseBoolean(System.getProperty(PROPERTY_NET_IPV6));
        } else {
            // default
            return false;
        }
    }

    public static File getWebrootDir() {
        String webrootDirPath = System.getProperty(PROPERTY_WEBROOT_DIR);
        File webrootDir = webrootDirPath != null ? new File(webrootDirPath)
                                                : new File(DEFAULT_WEBROOT_DIR); // starts from default Asawoo execution location
        return webrootDir; // TODO create if directory does not exist ?
    }

    /* FIXME
    public static String getHost() {
        return PlatformUtils.getHostname();
    }
    */

    private static String getDefaultLocalAvatarId() {
        try {
            return "platform@"+ InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return "";
        }
    }

}

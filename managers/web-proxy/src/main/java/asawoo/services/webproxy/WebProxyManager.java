package asawoo.services.webproxy;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.*;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.handler.AuthHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;
import org.apache.felix.ipojo.annotations.*;
import io.vertx.ext.web.Router;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;

@Component
@Provides
@Instantiate
public class WebProxyManager {

	@Requires
	private Router router;

	@Requires
	private Vertx vertx;

	private static String SALT = "saltysalt";

	private static String FILEPATH = "settings/proxy.json";

	HttpServer securedServer;

	private MessageDigest md;

	private Logger logger = LoggerFactory.getLogger(WebProxyManager.class);

	private class SimpleAuthProvider implements AuthProvider {
		private SimpleUser user;
		public SimpleAuthProvider(SimpleUser user) {
			this.user = user;
		}

		public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
			if (authInfo.getString("username").equals(user.principal().getString("username"))
					&& user.authenticate(authInfo.getString("password"))) {
				resultHandler.handle(Future.succeededFuture(user));
			} else {
				resultHandler.handle(Future.failedFuture("Bad username"));
			}
		}
	}

	private class SimpleUser extends AbstractUser {
		private String username;
		private byte[] passwordDigest;

		public SimpleUser(String username, byte[] passwordDigest) {
			this.passwordDigest = passwordDigest;
			this.username = username;
		}

		public Boolean authenticate(String password) {
			return Arrays.equals(this.passwordDigest, md.digest((SALT + password).getBytes()));
		}

		protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
			resultHandler.handle(Future.succeededFuture(true));
		}

		public void setAuthProvider(AuthProvider authProvider) {
			return;
		}

		public JsonObject principal() {
			return new JsonObject().put("username", username);
		}
	}

	private void startSecuredServer(Integer port, Boolean selfSigned, String username, byte[] passwordDigest) {
		if (securedServer != null) {
			logger.info("Shutting down secured web server before starting with new configuration");
			securedServer.close();
		}

		if (username == null || passwordDigest == null) {
			logger.info("No username/password defined for secured server.");
			return;
		}

		if (port == null) port = 10000;

		if (selfSigned != null && selfSigned) {
			HttpServerOptions httpOpts = new HttpServerOptions();
			createCertificate(httpOpts);
			securedServer = vertx.createHttpServer(httpOpts);
		} else {
			securedServer = vertx.createHttpServer();
		}

		Router securedRouter = Router.router(vertx);

		AuthHandler authHandler = BasicAuthHandler.create(new SimpleAuthProvider(new SimpleUser(username, passwordDigest)));
		securedRouter.route().handler(authHandler);

		securedRouter.mountSubRouter("/", router);
		securedServer.requestHandler(securedRouter::accept);
		securedServer.listen(port);
		logger.info("Secured web server listening on port " + port);
	}

	private void createCertificate(HttpServerOptions httpOpts) {
		try {
			KeyStore store = KeyStore.getInstance("JKS");
			store.load(null, null);
			CertAndKeyGen keypair = new CertAndKeyGen("RSA", "SHA256WithRSA", null);
			X500Name x500Name = new X500Name("localhost", "IT", "unknown", "unknown", "unknown", "unknown");
			keypair.generate(1024);
			PrivateKey privKey = keypair.getPrivateKey();
			X509Certificate[] chain = new X509Certificate[1];
			chain[0] = keypair.getSelfCertificate(x500Name, new Date(), (long) 365 * 24 * 60 * 60);
			store.setKeyEntry("selfsigned", privKey, "passwordInTheCodeIsBAD".toCharArray(), chain);
			store.store(new FileOutputStream(".keystore"), "passwordInTheCodeIsBAD".toCharArray());
			logger.info("Using a self-signed certificate for HTTPS support in secured web server");
			httpOpts.setKeyStoreOptions(new JksOptions().setPath(".keystore").setPassword("passwordInTheCodeIsBAD"));
			httpOpts.setSsl(true);
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	}

	private void setProxySettings(String proxyHost, String proxyPort) {
		// FIXME: also define https.proxyHost ?
		if (proxyHost != null) {
			logger.debug("System.setProperty http.proxyHost " + proxyHost);
			System.setProperty("http.proxyHost", proxyHost);
		}
		if (proxyPort != null) {
			logger.debug("System.setProperty http.proxyPort " + proxyPort);
			System.setProperty("http.proxyPort", proxyPort);
		}
		System.setProperty("http.nonProxyHosts", "localhost|127.*|[::1]|semantic-repo|code-repo");
	}

	private JsonObject readProxySettings() {
		FileSystem fs = vertx.fileSystem();
		if(fs.existsBlocking(FILEPATH)) {
			Buffer data = fs.readFileBlocking(FILEPATH);
			return data.toJsonObject();
		} else {
			return new JsonObject();
		}
	}

	private void writeProxySettings(JsonObject settings) {
		FileSystem fs = vertx.fileSystem();
		if (!fs.existsBlocking("settings")) {
			fs.mkdirBlocking("settings");
		}
		fs.writeFileBlocking(FILEPATH, Buffer.buffer(settings.encodePrettily()));
	}

	private void applySettings(JsonObject settings) {
		JsonObject proxy = settings.getJsonObject("proxy");
		if (proxy == null) proxy = new JsonObject();
		setProxySettings(proxy.getString("host"), proxy.getString("port"));
		JsonObject securedServer = settings.getJsonObject("securedServer");
		if (securedServer == null) securedServer = new JsonObject();
		JsonObject user = settings.getJsonObject("user");
		if (user == null) user = new JsonObject();
		startSecuredServer(securedServer.getInteger("port"), securedServer.getBoolean("selfSigned"), user.getString("username"), user.getBinary("passwordDigest"));
	}

	@Validate
	private void start() {
		logger.info("Start the Web proxy manager");

		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		JsonObject settings = readProxySettings();
		applySettings(settings);

		router.get("/proxysettings").handler(routingContext -> {
			routingContext.response().setStatusCode(HttpResponseStatus.OK.code()).end(settings.toString());
		});

		router.patch("/proxysettings").handler(routingContext -> {
			JsonObject body = routingContext.getBodyAsJson();
			if (body.containsKey("user")) {
				JsonObject user = body.getJsonObject("user");
				if (user.containsKey("password")) {
					user.put("passwordDigest", md.digest((SALT + user.getString("password")).getBytes()));
					user.remove("password");
				}
				body.put("user", user);
			}
			settings.mergeIn(body);
			applySettings(settings);
			writeProxySettings(settings);
			routingContext.response().setStatusCode(HttpResponseStatus.OK.code()).end();
		});
	}
}

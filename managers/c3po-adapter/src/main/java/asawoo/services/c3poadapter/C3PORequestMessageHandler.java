package asawoo.services.c3poadapter;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import asawoo.services.net.dtn.util.http.HttpRequestWrapper;
import fr.c3po.io.serializer.IllegalObjectFormatException;
import fr.c3po.message.Message;
import fr.c3po.message.MessageContentType;
import fr.c3po.message.MessageHeader;
import fr.c3po.message.MessageType;
import fr.c3po.message.channel.Channel;
import fr.c3po.service.C3POMessageService;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Set;

/**
 * This Handler receives and processes an HTTP request message, executes it locally and passes the HTTP response to the provided handler
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 21/04/16.
 */
public class C3PORequestMessageHandler implements Handler<Message> {

    private C3POMessageService msgService;
    private VertxHttpClient httpClient;
    private RestServiceRegistry restServiceRegistry;

    private DTNRequestMetadata metadata;
    private String replyTopic;
    private Channel replyChannel;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public C3PORequestMessageHandler(C3POMessageService msgService,
                                     VertxHttpClient httpClient,
                                     RestServiceRegistry restServiceRegistry,
                                     Channel replyChannel) {
        this.msgService = msgService;
        this.httpClient = httpClient;
        this.restServiceRegistry = restServiceRegistry;
        this.replyChannel = replyChannel;
    }

    @Override
    public void handle(Message message) {

        // TODO remove trace
        logger.info("Message ("+ message.getMessageType().name() +") received : " + message.getId() + " on network " + message.getNetworkID());// + " :: content : " + new String(message.getContent(), StandardCharsets.UTF_8));

        if (message.getMessageType().equals(MessageType.SERVICE_REQUEST)) {

            // extract request
            try {
                HttpRequestWrapper req = DTNUtils.<HttpRequestWrapper>deserialize(message.getContent());
                metadata = req.getMetadata();
                replyTopic = getReplyTopic(message);

                // execute request
                String httpMethod = metadata.getHttpMethod();

                // get or rebuild request
                URI dtnUri = null;
                // set dtnUri port to local HTTP port
                int port = AsawooConfiguration.getHttpPort();

                if (DTNUtils.getCommunicationModeFromMetadata(metadata) == CommunicationMode.UNICAST) {
                    URI c3poDtnUri = metadata.getDtnUri();
                    // TODO add query params OR = req.getRequestParameters ??
                    dtnUri = URI.create("http://localhost:" + port + c3poDtnUri.getPath());
                } else {
                    // URI follows the pattern: http+mcast://<topic>/method
                    String functionality = metadata.getDtnUri().getHost(); // topic / functionality
                    String methodPath = metadata.getDtnUri().getPath(); //AsawooUtils.getMethodPathFromURI(metadata.getDtnUri());

                    logger.info("Looking for functionality [" + functionality + "] and method path ["+methodPath+"]");

                    // first check if this is a functionality or a system functionality
                    if (restServiceRegistry.getSystemRoutes().containsKey(functionality)) {
                        Collection<FunctionalityRequest> frs = restServiceRegistry.getSystemRoutes().get(functionality);
                        for (FunctionalityRequest fr : frs) {
                            if (httpMethod.equals(fr.verb)
                                && ((methodPath == null && fr.url == null)
                                    ||
                                    fr.url.equals(methodPath))) {
                                dtnUri = new URI("http://localhost:"+port+methodPath);
                            }
                        }

                        if (dtnUri == null) {
                            String errorMessage = "Unknown method path [" + methodPath + "] in registry for system functionality [" + functionality + "]";
                            logger.error(errorMessage);
                            replyError(message.getSource(), message.getId(), C3POResponseMessageHandler.RESPONSE_ERROR_PREFIX+errorMessage);
                            return;
                        }


                    } else {
                        // guess local URI to call from topic (functionality) and path (method)
                        Set<String> fullUris = restServiceRegistry.findFullUriByFunctionalityAndMethodPath(functionality, methodPath);
                        System.out.println("******** "+fullUris.size()+" matching URIs found");
                        fullUris.forEach(uri->System.out.println("******///// "+uri));
                        if (fullUris != null && !fullUris.isEmpty()) {
                            // TODO FIXME: execute against multiple matching URIs ?
                            // TODO FIXME: difficulty: how to treat multiple answers ? only the first one ? same callback should be used ?
                            URI registryUri = URI.create(fullUris.iterator().next());

                            dtnUri = new URI(registryUri.getScheme(),
                                    registryUri.getUserInfo(),
                                    "localhost",//registryUri.getHost(),
                                    port,
                                    registryUri.getPath(),
                                    registryUri.getQuery(),
                                    registryUri.getFragment());

                        } else {
                            // no matching URI found
                            // TODO
                            String errorMessage = "No matching URI has been found in registry against functionality [" + functionality + "] and method path [" + methodPath + "]";
                            logger.error(errorMessage);
                            replyError(message.getSource(), message.getId(), C3POResponseMessageHandler.RESPONSE_ERROR_PREFIX+errorMessage);
                            return;
                        }
                    }

                }
                logger.info("Trying to execute " + httpMethod + " method on " + dtnUri.toString());



                httpClient.executeRequest(httpMethod,
                        dtnUri,
                        req.getRequestParameters(),
                        req.getRequestBody(),
                        new C3POResponseHandler(message.getSource(), message.getId()));



            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private String getReplyTopic(Message message) {
        return C3PODTNAdapter.REPLY_TOPIC_PREFIX+message.getId();
    }

    private void replyError(String destNodeId, String replyId, String errorMessage) {
        Message errorResponseMessage = msgService.createMessage(destNodeId, MessageType.SERVICE_RESPONSE, errorMessage.getBytes(), MessageContentType.BINARY);
        // we use a proper MessageHeader to hold the replyId
        try {
            errorResponseMessage.addHeader(MessageHeader.SERVICE_REQUEST_ID, replyId);
        } catch (IllegalObjectFormatException e) {
            e.printStackTrace();
        }
        dispatchMessage(errorResponseMessage);
    }

    private void dispatchMessage(Message message) {
        if (DTNUtils.getCommunicationModeFromMetadata(metadata) == CommunicationMode.UNICAST && replyChannel != null) {
            logger.info("Sending HTTP response in C3PO message "+message.getId()+" through Channel "+replyChannel.toString());
            msgService.sendMessage(replyChannel, message);
        } else {
            logger.info("Publishing HTTP response in C3PO message "+message.getId()+" on topic "+replyTopic);
            msgService.publishMessage(replyTopic, message);
        }
    }

    private class C3POResponseHandler implements Handler<HttpClientResponse> {

        private String nodeId;
        private String replyId;

        public C3POResponseHandler(String destNodeId, String replyID) {
            this.nodeId = destNodeId;
            this.replyId = replyID;
        }

        @Override
        public void handle(HttpClientResponse response) {
            logger.info("Received response from HTTP request");
            response.bodyHandler(buffer -> {
                byte[] msgContent = buffer.getBytes();
                Message responseMessage = msgService.createMessage(nodeId, MessageType.SERVICE_RESPONSE, msgContent, MessageContentType.BINARY);

                // we use a proper MessageHeader to hold the replyId
                try {
                    responseMessage.addHeader(MessageHeader.SERVICE_REQUEST_ID, replyId);
                } catch (IllegalObjectFormatException e) {
                    e.printStackTrace();
                }

                // TODO check TTL before replying ? already done by C3PO ?
                try {
                    Long ttl = DTNUtils.getTimeout(metadata);
                    if (ttl != null) {
                        responseMessage.updateHeader(MessageHeader.LIFETIME, String.valueOf(ttl));
                    }
                } catch (IllegalObjectFormatException e) {
                    e.printStackTrace();
                }

                // send response back
                dispatchMessage(responseMessage);

            });
        }
    }

}

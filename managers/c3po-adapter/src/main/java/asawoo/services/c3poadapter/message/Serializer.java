package asawoo.services.c3poadapter.message;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import fr.c3po.message.Message;

import java.io.*;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 15/03/16.
 */
public class Serializer {

    public static <T extends Serializable> T getContent(Message message) throws IOException, ClassNotFoundException {
        return deserialize(message.getContent());
    }

    public static <T extends Serializable> byte[] serialize(T object) throws IOException {
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream()){
            try(ObjectOutputStream oos = new ObjectOutputStream(bos)){
                oos.writeObject(object);
            }
            return bos.toByteArray();
        }
    }

    public static <T extends Serializable> T deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream bis = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream ois = new ObjectInputStream(bis)){
                Object o = (T) ois.readObject();
//                if (o instanceof T) {
                    return (T) o;
//                } else {
//                    return null;
//                }
            }
        }
    }


}

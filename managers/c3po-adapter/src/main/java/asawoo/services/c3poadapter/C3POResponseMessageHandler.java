package asawoo.services.c3poadapter;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.util.DTNConstants;
import fr.c3po.message.Message;
import fr.c3po.message.MessageType;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;

import java.net.URI;
import java.nio.charset.StandardCharsets;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on  24/04/16.
 */
public class C3POResponseMessageHandler implements Handler<Message> {

    public static final String RESPONSE_ERROR_PREFIX = "ERROR:";

    private URI callbackUri;

    private HttpServerResponse response;

    private String requestId;

    private VertxHttpClient httpClient;

    private int callbackPort;

    public C3POResponseMessageHandler(String requestId, HttpServerResponse response, URI callbackUri, VertxHttpClient httpClient, int callbackPort) {
        this.requestId = requestId;
        this.response = response;
        this.callbackUri = callbackUri;
        this.httpClient = httpClient;
        this.callbackPort = callbackPort;
    }

    @Override
    public void handle(Message message) {

        if (message.getMessageType().equals(MessageType.SERVICE_RESPONSE)) {

            // extract response
            // HTTP response is received as a String
            String httpResponse = new String(message.getContent(), StandardCharsets.UTF_8);
            //DTNUtils.<String>deserialize(message.getContent());
            //        response.putHeader("Content-Length", String.valueOf(httpResponse.length()));
            //        response.write(httpResponse);

//            System.out.println("C3PO Response RECEIVED !! " + httpResponse);

            if (!response.ended()) {
                if (httpResponse.startsWith(RESPONSE_ERROR_PREFIX)) {
                    response.setStatusCode(HttpResponseStatus.NOT_FOUND.code());
                    httpResponse = RESPONSE_ERROR_PREFIX
                                    .concat(" "+HttpResponseStatus.NOT_FOUND.code()+" ")
                                    .concat(HttpResponseStatus.NOT_FOUND.reasonPhrase())
                                    .concat("\n")
                                    .concat(httpResponse.substring(RESPONSE_ERROR_PREFIX.length()));
                } else {
                    response.setStatusCode(HttpResponseStatus.OK.code());
                }
                response.end(httpResponse);
            }
            //        response.close();

            // TODO or call local callback URI
            // client.execute POST on localhost:9090/callback?reqId = message_id
            if (callbackUri == null) {
                callbackUri = URI.create("http://localhost:"
                        + callbackPort
                        + DTNConstants.DTN_DEFAULT_CALLBACK_URL_MAPPING
                        + "/" + requestId);
            }

            httpClient.executeRequest(HttpMethod.POST.name(),
                    callbackUri,
                    null,
                    httpResponse,
                    response -> {
                        // do nothing or check status code ?
                    });
        }
    }


}

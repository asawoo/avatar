package asawoo.services.c3poadapter;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.vertx.VertxConfiguration;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityManager;
import asawoo.core.functionality.FunctionalityManagerListener;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.services.c3poadapter.message.Serializer;
import asawoo.services.http.client.HttpClientService;
import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.service.DTNCallbackManager;
import asawoo.services.net.dtn.service.DTNNeighborDiscoveryListener;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import asawoo.services.net.dtn.util.http.HttpRequestWrapper;
import fr.c3po.C3PO;
import fr.c3po.config.C3POConfigurationProperties;
import fr.c3po.config.ConfigurationParameter;
import fr.c3po.io.serializer.IllegalObjectFormatException;
import fr.c3po.message.*;
import fr.c3po.message.channel.Channel;
import fr.c3po.message.channel.ServerChannel;
import fr.c3po.message.topic.Topic;
import fr.c3po.net.ConnectivityType;
import fr.c3po.net.NeighborDiscoveryListener;
import fr.c3po.net.NeighborDiscoveryManager;
import fr.c3po.net.NetworkManager;
import fr.c3po.net.NetworkNode;
import fr.c3po.net.NetworkTopologyManager;
import fr.c3po.net.util.NetUtils;
import fr.c3po.router.MessageForwardingStrategy;
import fr.c3po.service.C3POMessageService;
import io.netty.util.internal.StringUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.annotations.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;
import java.util.logging.Level;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 03/03/16.
 */
@Component(immediate=true)
@Instantiate
@Provides
public class C3PODTNAdapter implements DTNAdapter, FunctionalityManagerListener {

    @Requires
    private C3POMessageService msgService;

    @Requires
    private HttpClientService httpClient;

    @Requires
    private VertxHttpClient vertxHttpClient;

    @Requires
    private Vertx vertx;
    @Requires
    private VertxConfiguration vertxConfiguration;

    @Requires
    private DTNCallbackManager dtnCallbackManager;

    @Requires // TODO filter on local functionality manager
    private FunctionalityManager functionalityManager;

    @Requires
    private RestServiceRegistry restServiceRegistry;

    private Logger logger = LoggerFactory.getLogger(C3PODTNAdapter.class);

//    @Requires(filter = "(factory.name = asawoo.services.c3poadapter.C3POSubscriber")
//    private Factory subscriberFactory;

    private String c3poNodeId;

    private final String frameworkName = "C3PO";

    private final int serverChannelPort = 99; // TODO move in a configuration file (vertx conf ?)
//    private final int replyChannelPort = 999; // TODO move in a configuration file (vertx conf ?)

    private Map<String, Topic> topics;
//    /* Map of <replyTopic/channelId, responseHandlers> */
//    private Map<String, C3POResponseMessageHandler> responseHandlersByChannelId;

    private ServerChannel serverChannel;
    /* Map of <nodeId, Channel> */
    private Map<String, Channel> channels;
    /* Map of <nodeId, MessageDispatcher> */
    private Map<String, MessageDispatcher> dispatchers;

    private List<DTNNeighborDiscoveryListener> neighborDiscoveryListeners;
    private NeighborDiscoveryListener c3poNeighborDiscoveryListener;

    public final static String REPLY_TOPIC_PREFIX = "asa_";


    public C3PODTNAdapter() {
        this.topics = new HashMap<>();
//        this.responseHandlersByChannelId = new HashMap<>();
        this.channels = new HashMap<>();
        this.dispatchers = new HashMap<>();
        this.neighborDiscoveryListeners = new LinkedList<>();
//        this.callbackSubscribers = new HashMap<>();

        // TODO remove or do it elsewhere
        System.setProperty(AsawooConfiguration.PROPERTY_AVATAR_ID, getNodeId());
    }

    @Validate
    public void start() {
        logger.info("JULogging :: " + logger.getClass() + " - DEBUG : " + logger.isDebugEnabled() + " - TRACE : " + logger.isTraceEnabled());

        subscribeToMulticastGroups();

        // open server channel
        openServerChannel();

        // register neighbor discovery listener
        registerNeighborDiscoveryListener();

    }

    @Invalidate
    public void stop() {
        closeTopics();
        closeChannels();
        unregisterNeighborDiscoveryListener();
    }

    @Override
    public String getDTNFrameworkName() {
        return frameworkName;
    }

    @Override
    public void setNodeId(String nodeId) {
        this.c3poNodeId = nodeId;
        // TODO check if this is correct
        C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(C3POConfigurationProperties.NETWORK_NODE_ID).setValue(nodeId);
    }

    @Override
    public String getNodeId() {
        return C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(C3POConfigurationProperties.NETWORK_NODE_ID).getValue();
    }

    @Override
    public String forwardRequest(DTNRequestMetadata metadata, MultiMap queryParams, String requestBody, HttpServerResponse response) throws DTNUnsupportedComModeException {
        Message message = createRequestMessage(metadata, queryParams, requestBody);

        // multicast topic = host (e.g., http+mcast://groupOrFunctionalityName/method)
        // unicast destination node = host (e.g., http://C3PO01/functionalityPath)
        String destination = DTNUtils.getHost(metadata);
        CommunicationMode comMode = DTNUtils.getCommunicationModeFromMetadata(metadata);
        //DTNUtils.getFunctionalityFromMetadata(metadata);

        int callbackPort = AsawooConfiguration.getHttpPort(); //vertxConfiguration.getConfig().getInteger(VertxConfiguration.HTTP_PORT);
        String callbackPath = "";

        logger.info("Forwarding Request - COM MODE : "+comMode+" // destination : "+destination);
        switch(comMode) {
            case BROADCAST:
            case ANYCAST:
            case MULTICAST:

                System.out.println("/////// Reponse : "+response);

                if (response != null) { // no response object to write response into
                    // Before publishing
                    // listen for response on "message.id" topic
                    String replyTopicStr = REPLY_TOPIC_PREFIX + message.getId();
                    callbackPath = replyTopicStr;

                    C3POResponseMessageHandler responseMH = new C3POResponseMessageHandler(callbackPath,
                            response,
                            metadata.getCallback(),
                            vertxHttpClient,
                            callbackPort);

                    Topic replyTopic = msgService.getTopic(replyTopicStr);
                    MessageListener subscriber = new MessageListener() {
                        @Override
                        public void onReceive(Message message) {
                            if (comMode == CommunicationMode.ANYCAST) {
                                unsubscribeAndCloseTopic(replyTopic);

                                // TODO might use a healing mechanism here ?
                            }
                            responseMH.handle(message);

                        }
                    };
                    replyTopic.addSubscriber(subscriber);
                    topics.put(replyTopicStr, replyTopic);

                    // TODO unsubscribe after TTL expires or when "all" responses have been received
                    final Long ttl = DTNUtils.getTimeout(metadata);
                    if (ttl != null && ttl > 0) {
                        vertx.setTimer(ttl, delay -> {
                            if (topics.containsKey(replyTopicStr)) {
                                //                        callbackSubscribers.remove(replyTopic).unsubscribe();
                                unsubscribeAndCloseTopic(replyTopic);
                            }
                        });
                    }
                }


//                // extract functionality name from REST URI path by asking the REST functionality registry
//                String path = metadata.getDtnUri().getPath();
//                String functionalityTopic = restServiceRegistry.getFunctionalityNameByUri(path);
//                if (functionalityTopic != null) {
//                    logger.info("--- Publishing C3PO message on topic : " + functionalityTopic);
//                    msgService.publishMessage(functionalityTopic, message);
//                } else {
//                    String errorMessage = "Could not forward request to C3PO DTN communication layer: URI path "+path+" is not mapped to any C3PO topic.";
//                    response.setStatusCode(HttpResponseStatus.NOT_FOUND.code())
//                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(errorMessage.length()))
//                            .end(errorMessage);
//                }
                System.out.println("/////// Publishing MCAST MSG to "+destination+" ("+message.getMessageType().name()+")");
                msgService.publishMessage(destination, message);
                break;
            case UNICAST:
                callbackPath = message.getId();

                // listen for a response on channel
                Channel channel;

                // if this channel is already known, add a responseHandler
                // TODO CAUTION: ignore case of node IDs ? the following is case-sensitive
                if (channels.containsKey(destination)) {
                    channel = channels.get(destination);
                } else {
                    channel = msgService.getChannel(destination, serverChannelPort);
                    newChannel(channel);
                }

                // TODO even if there is no provided HttpResponse object, there might be a callback URI (i.e, if DTNAdapter.forwardRequest is used directly)
                if (response != null) { // no response object to write response into


                    MessageDispatcher md = dispatchers.get(destination);
                    md.addResponseMessageHandler(callbackPath,
                            new C3POResponseMessageHandler(callbackPath,
                                    response,
                                    metadata.getCallback(),
                                    vertxHttpClient,
                                    callbackPort));
                }

                // TODO Implement stop listening after TTL expires ?
                msgService.sendMessage(channel, message);
//                msgService.sendMessage(destination, serverChannelPort, message);
                //                throw new DTNUnsupportedComModeException(comMode.name()+" is not supported by "+getDTNFrameworkName()+" DTN framework");
                break;

            default:
                callbackPath = message.getId();
        }

        // TODO even if there is no provided HttpResponse object, there might be a callback URI (i.e, if DTNAdapter.forwardRequest is used directly)
        // TODO needs some more testing
        if (response != null) { // no response object to write response into
            // TODO store handler for callback too ?
            dtnCallbackManager.addRequestHandler(callbackPath, rc -> {
                // TODO handler should be passed in forward method parameter ?
                String reqBody = rc.getBodyAsString();
                rc.response().end();
                logger.info("DTN response received on callback URI: " + reqBody);
            });
        }

        return message.getId();

    }

    @Override
    public void forwardRequest(DTNRequestMetadata metadata, Map<String, String[]> requestParams, HttpServletResponse response) throws DTNUnsupportedComModeException {
        Message message = wrapRequest(metadata, requestParams);

        CommunicationMode comMode = DTNUtils.getCommunicationModeFromMetadata(metadata);
        String functionalityTopic = DTNUtils.getFunctionalityFromMetadata(metadata); // extract functionality name from REST URI path

        System.out.println("Forwarding Request - COM MODE : "+comMode);
        switch(comMode) {
            case BROADCAST:
                msgService.publishMessage(functionalityTopic, message);
                break;
            case ANYCAST:
                msgService.publishMessage(functionalityTopic, message);
//                throw new DTNUnsupportedComModeException(comMode.name()+" is not supported by "+getDTNFrameworkName()+" DTN framework");
                break;
            case MULTICAST:
                msgService.publishMessage(functionalityTopic, message);
                break;
            default: // unicast
                msgService.sendMessage(DTNUtils.getHost(metadata), serverChannelPort, message);
        }

    }

    @Override
    public EnumSet<CommunicationMode> getSupportedCommunicationModes() {
        return EnumSet.allOf(CommunicationMode.class);
    }

    @Override
    public void purgeMessage(String messageId) {
        // send a DROP message and removes message from local cache

//      Message dropMessage = msgService.createMessage(MessageForwardingStrategy.WILDCARD_DESTINATION, MessageType.DROP, messageId.getBytes(), MessageContentType.TEXT);
//      msgService.forward(MessageForwardingStrategy.WILDCARD_DESTINATION, dropMessage);
//      C3PO.OPP_COM_MODULE.getMessageCache().remove(messageId);
        C3PO.OPP_COM_MODULE.dropMessage(messageId);

    }

    @Override
    public String getHost() {
        // return IPv6 host.
        // get Connectivity Type as defined in configuration
        ConnectivityType ct = ConnectivityType.valueOf(C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(C3POConfigurationProperties.NETMAN_CONNECTIVITY_USER_CHOICE).getValue());
        /* FIXME : not working for now
        // retrieve Network node info
        NetworkNode node = NetworkTopologyManager.getInstance().getNetworkNode(getNodeId(), ct);
        return node.getAddress()*/;
        String netInterface="";
        switch (ct) {
            case WIFI_ADHOC:
                netInterface = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(
                        C3POConfigurationProperties.NETMAN_WIFI_ADHOC_INTERFACE).getValue();
                break;
            case WIFI_AP:
                // TODO
                netInterface = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(
                        C3POConfigurationProperties.NETMAN_WIFI_ADHOC_INTERFACE).getValue();
                break;
            case WIFI_P2P:
                // TODO
                break;
            case WIFI_LEGACY:
                // TODO
                break;
            case ETHERNET:
                // TODO
                netInterface = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(
                        C3POConfigurationProperties.NETMAN_INFRASTRUCTURE_NET_INTERFACE).getValue();
                break;
            case INFRASTRUCTURE:
                netInterface = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(
                        C3POConfigurationProperties.NETMAN_INFRASTRUCTURE_NET_INTERFACE).getValue();
                break;
            default:
                netInterface = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(
                        C3POConfigurationProperties.NETMAN_INFRASTRUCTURE_NET_INTERFACE).getValue();
        }
        // get network interface as defined in configuration
        // false = ipv6
        return getInetAddress(netInterface, false).getHostAddress();
    }

    @Override
    public void onAdd(FunctionalityProviderBean fb) {
        // register a subscriber on this topic
        registerMulticastSubscriber(fb.getInterfaceName()); // TODO might also use uri path
    }

    @Override
    public void onRemove(FunctionalityProviderBean fb) {
        unregisterMulticastSubscriber(fb.getInterfaceName());
    }

    @Override
    public void onUpdate(FunctionalityProviderBean fb) {
        // interface name (i.e., topic) should not change, so there is nothing to do here
    }
    
    private InetAddress getInetAddress(String netIfName, boolean ipV4) {
        InetAddress res = null;
        NetworkInterface netIf = getNetworkInterface(netIfName);
        if (netIf != null) {
            Enumeration<InetAddress> nifAddresses = netIf.getInetAddresses();
            InetAddress ipv4Address = null,
                        ipv6Address = null;
            while (nifAddresses.hasMoreElements()) {
                InetAddress inetAddr = nifAddresses.nextElement();
                //if (inetAddr.getHostAddress().matches(IPV4_REGEX) && ipVersion) {
                if (inetAddr instanceof Inet4Address) {
                    ipv4Address = inetAddr; // IPv4 address
                } else if (!inetAddr.isLinkLocalAddress()) {
                    ipv6Address = inetAddr; // IPv6 address
                }
            }

            res = (!ipV4 && ipv6Address != null) ? ipv6Address : (ipv4Address != null) ? ipv4Address : null;
        }

        if (res == null) {
            throw new RuntimeException("No address was found for network interface ["+netIfName+"]. Check your C3PO configuration");
        } else {
            return res;
        }
    }
    
    private NetworkInterface getNetworkInterface(String netIfName) {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            NetworkInterface netIf;
            while (interfaces.hasMoreElements()) {
                netIf = interfaces.nextElement();
                if (netIf.getDisplayName().startsWith(netIfName)) {
                    return netIf;
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void subscribeToMulticastGroups() {

        // subscribe to configured multicast groups
        registerConfiguredTopicSubscribers();

        // subscribe to functionality topics
        registerFunctionalityTopicSubscribers();
    }

    private void unsubscribeAndCloseTopic(Topic topic) {
        topics.remove(topic.getName());
        topic.close();
    }


    private void unsubscribeAndCloseTopic(String topicName) {
        if (topics.containsKey(topicName)) {
            topics.remove(topicName).close();
        }

//        TopicSubscriber subToRemove = null;
//        for (TopicSubscriber sub : subscribers) {
//            if (sub.getTopic().equals(topicName)) {
//                subToRemove = sub;
//                break;
//            }
//        }
//        if (subToRemove != null) {
//            subToRemove.unsubscribe();
//            subscribers.remove(subToRemove);
//            msgService.getTopic(topicName).close();
//        }
    }

    private Message createRequestMessage(DTNRequestMetadata metadata, MultiMap requestParams, String requestBody) {

        // encapsulate metadata and parameters in a C3PO message object
        Map<String, String[]> params = DTNUtils.multiMapToSimpleMap(requestParams);
        HttpRequestWrapper requestObject = new HttpRequestWrapper(metadata, params, requestBody);

        try {
            String destNodeId;
            if (DTNUtils.getCommunicationModeFromMetadata(metadata) == CommunicationMode.UNICAST) {
                destNodeId = DTNUtils.getHost(metadata);
            } else {
                // TODO replace later
                destNodeId = MessageForwardingStrategy.WILDCARD_DESTINATION;
            }


            byte[] msgContent = Serializer.<HttpRequestWrapper>serialize(requestObject);
            Message msg = msgService.createMessage(destNodeId, MessageType.SERVICE_REQUEST, msgContent, MessageContentType.BINARY);

            // TODO TTL, ...
            Long ttl = DTNUtils.getTimeout(metadata);
            if (ttl != null) {
                // TODO sort this out (message lifetime is required header and already set)
//                msg.addHeader(MessageHeader.LIFETIME, ttl.toString());
//                msg.updateHeader(MessageHeader.LIFETIME, ttl.toString());
            }

            // TODO nbHops, ...
            if (metadata.getHopsNb() != null) {
                msg.updateHeader(MessageHeader.NB_HOPS, metadata.getHopsNb().toString());
            }
            // MessageHeader.SOURCE_GPS_LOCATION ...

            return msg;
        } catch (IllegalObjectFormatException e) {
            e.printStackTrace();
            return null; // default message
        } catch (IOException e) {
            e.printStackTrace();
            return null; // default message
        }

    }

    /**
     *
     * @param metadata
     * @param requestParams
     * @return
     */
    private Message wrapRequest(DTNRequestMetadata metadata, Map<String, String[]> requestParams) {

        // encapsulate metadata and parameters in a C3PO message object
        HttpRequestWrapper requestObject = new HttpRequestWrapper(metadata, requestParams, null);

        try {
//            String destNodeId = DTNUtils.getHost(metadata);
            // TODO replace later
            String destNodeId = MessageForwardingStrategy.WILDCARD_DESTINATION;
            byte[] msgContent = Serializer.<HttpRequestWrapper>serialize(requestObject);
            Message msg = msgService.createMessage(destNodeId, MessageType.SERVICE_REQUEST, msgContent, MessageContentType.BINARY);

            // TODO TTL, ...
            Long ttl = DTNUtils.getTimeout(metadata);
            if (ttl != null) {
                msg.updateHeader(MessageHeader.LIFETIME, ttl.toString());
            }

            // TODO nbHops, ...
            if (metadata.getHopsNb() != null) {
                msg.updateHeader(MessageHeader.NB_HOPS, metadata.getHopsNb().toString());
            }
            // MessageHeader.SOURCE_GPS_LOCATION ...

            return msg;
        } catch (IllegalObjectFormatException e) {
            e.printStackTrace();
            return null; // default message
        } catch (IOException e) {
            e.printStackTrace();
            return null; // default message
        }

    }

    @Override
    public void registerMulticastSubscriber(String groupName) {
        C3PORequestMessageHandler msgHandler = new C3PORequestMessageHandler(msgService, vertxHttpClient, restServiceRegistry, null);
        System.out.println("******** registering TOPIC : "+groupName+" /// REQ MSG handler created");

        Topic topic = msgService.getTopic(groupName);
        topic.addSubscriber(msgHandler::handle);
        topics.put(groupName, topic);

        logger.info("Subscribing to anycast/broadcast/multicast topic ["+groupName+"]");


    }

    @Override
    public void unregisterMulticastSubscriber(String topic) {
        unsubscribeAndCloseTopic(topic);
        logger.info("Unsubscribing to anycast/broadcast/multicast topic ["+topic+"]");
    }

    @Override
    public void addNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener) {
        this.neighborDiscoveryListeners.add(listener);
    }

    @Override
    public void removeNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener) {
        this.neighborDiscoveryListeners.remove(listener);
    }

    private void registerNeighborDiscoveryListener() {
        c3poNeighborDiscoveryListener = new NeighborDiscoveryListener() {

            @Override
            public void onNeighborsDiscovered(NeighborDiscoveryManager neighborDiscoveryManager, Map<String, NetworkNode> map) {
                // nothing to do ?
            }

            @Override
            public void onNeighborDisappeared(NeighborDiscoveryManager neighborDiscoveryManager, String nodeId) {
                neighborDiscoveryListeners.forEach(l -> l.onNeighborLoss(nodeId));
            }

            @Override
            public void onNeighborDiscovered(NeighborDiscoveryManager neighborDiscoveryManager, NetworkNode nn) {
                neighborDiscoveryListeners.forEach(l -> l.onNeighborContact(nn.getNodeId()));
            }
        };

        C3PO.NET_MAN_MODULE.addNeighborDiscoveryListener(c3poNeighborDiscoveryListener);
    }

    private void unregisterNeighborDiscoveryListener() {
        C3PO.NET_MAN_MODULE.removeNeighborDiscoveryListener(c3poNeighborDiscoveryListener);
        c3poNeighborDiscoveryListener = null;
    }

    /**
     * store functionalitySubscribers in a local map
     */
    private void registerFunctionalityTopicSubscribers() {
        functionalityManager.getEnabledFunctionalities().forEach(this::registerMulticastSubscriber);
    }

    private void registerConfiguredTopicSubscribers() {
        // get multicast group names from config if any
        ConfigurationParameter netGroupParam = C3PO.CONF_MAN_MODULE.getConfigurationManager().getParameter(C3POConfigurationProperties.NETWORK_GROUP_IDS);
        if (netGroupParam != null) {
            String groupList = netGroupParam.getValue();
            if (!StringUtil.isNullOrEmpty(groupList)) {
                for (String group : groupList.split(",")) {
                    registerMulticastSubscriber(group);
                }
            }
        }
    }


    private void openServerChannel() {
        serverChannel = msgService.getServerChannel(serverChannelPort);

        // blocking code, so execute in a worker thread pool
        vertx.executeBlocking(future -> {

            // FIXME: temporary hacking to avoid annoying Warnings (see below)
            LoggerFactory.removeLogger(io.vertx.core.impl.BlockedThreadChecker.class.getName());
            // or JUL way
            java.util.logging.Logger.getLogger(io.vertx.core.impl.BlockedThreadChecker.class.getName()).setLevel(Level.OFF);

            while (!serverChannel.isClosed()) {
                // TODO FIXME: this blockig queue is blocking Vert.X for too long since vert.x 3.4.0 (60s time limit). Might use VertxOptions.setMaxWorkerExecuteTime
                Channel c = serverChannel.accept();
                if (!channels.containsKey(c.getRemoteNodeID())) {
                    newChannel(c);
                } // else this channel already exists (if we send something previously ?)... in this case this should not happen, accept would not trigger.

//                Channel replyChannel = msgService.getChannel(c.getRemoteNodeID(), replyChannelPort);

//                C3PORequestMessageHandler msgHandler = new C3PORequestMessageHandler(msgService, vertxHttpClient, restServiceRegistry, c);
//
//                c.getReceiver().addMessageListener(msgHandler::handle);
            }
            future.complete();
            }, res -> {
                if (res.failed()) {
                    logger.error("something bad happened here: OpenServerChannel"); // TODO
                }
            }
        );

    }

    private void newChannel(Channel c) {
        channels.put(c.getRemoteNodeID(), c);
        // TODO better instantiate an iPojo instance ? instead of passing service references ?
        MessageDispatcher md = new MessageDispatcher(new C3PORequestMessageHandler(msgService, vertxHttpClient, restServiceRegistry, c));
        dispatchers.put(c.getRemoteNodeID(), md);
        c.setMessageListener(md::onReceive);

        logger.info("Listenning on new Channel " + c.getPort() + " - local node " + getNodeId() + " - remote neighbor " + c.getRemoteNodeID());
    }

    private void closeChannels() {
        if (serverChannel != null) {
            serverChannel.close();
        }
//        responseHandlersByChannelId.clear();
        channels.values().forEach(channel -> {
            channel.close();
        });
        channels.clear();
        dispatchers.clear();
    }


    private void closeTopics() {
        topics.values().forEach(topic -> {
            topic.close();
        });
        topics.clear();
    }

}

package asawoo.services.c3poadapter;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import fr.c3po.message.Message;
import fr.c3po.message.MessageHeader;
import fr.c3po.message.MessageListener;
import io.vertx.core.Handler;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 09/06/16.
 */
public class MessageDispatcher implements MessageListener {

    private Handler<Message> requestMessageHandler;
    private Map<String, Handler<Message>> responseMessageHandlers;

    public MessageDispatcher(C3PORequestMessageHandler requestMessageHandler) {
        this.requestMessageHandler = requestMessageHandler;
        responseMessageHandlers = new HashMap<>();
    }

//    public void setRequestMessageHandler(Handler<Message> requestMessageHandler) {
//        this.requestMessageHandler = requestMessageHandler;
//    }

    public void addResponseMessageHandler(String responseId, Handler<Message> responseMessageHandler) {
        this.responseMessageHandlers.put(responseId,responseMessageHandler);
    }

    @Override
    public void onReceive(Message message) {

        switch(message.getMessageType()) {
            case SERVICE_REQUEST:
                requestMessageHandler.handle(message);
                break;
            case SERVICE_RESPONSE:
                String replyId = message.getHeader(MessageHeader.SERVICE_REQUEST_ID);
                if (responseMessageHandlers.containsKey(replyId)) {
                    responseMessageHandlers.remove(replyId).handle(message);
                } else {
                    // TODO problem here
                }
                break;
            default:
        }
    }



}

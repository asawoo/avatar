package asawoo.services.c3poadapter;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import fr.c3po.service.C3POService;
import org.apache.felix.ipojo.annotations.*;
import org.osgi.framework.BundleContext;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 21/03/16.
 */
@Component
@Instantiate
public class C3POActivator {

    @Context
    private BundleContext bundleContext;

    @Requires
    private C3POService c3poService;

    @Validate
    public void start() {

        // pass a valid C3PO config file as well as a connectivityManager to initialize C3PO framework
        // c3po config file Inputstream
        try {
            InputStream cfgIS = bundleContext.getBundle().getResource("/c3pocfg.json").openStream();
            c3poService.init(cfgIS);
        } catch (IOException e) {
            c3poService.init();
            e.printStackTrace();
        }

        c3poService.start();

    }

    @Invalidate
    public void stop() {
        // c3poService reference should be null unless it has been stopped manually
        if (c3poService != null)
            c3poService.stop();
    }

}

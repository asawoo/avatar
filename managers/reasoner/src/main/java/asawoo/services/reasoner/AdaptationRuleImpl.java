package asawoo.services.reasoner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.jena.vocabulary.RDF;

import asawoo.core.config.SemanticReasonerConfiguration;
import asawoo.core.reasoner.AdaptationRule;

public class AdaptationRuleImpl implements AdaptationRule {
	
	private String name;
	private List<String> contextualInstances = new ArrayList<String>();
	private List<List<String>> scoredAdaptationPossibilities = new ArrayList<List<String>>();
	
	public AdaptationRuleImpl(List<String> instancesURI, String name) {
		this.setContextualInstances(instancesURI);
		this.name = name;
	}
	
	@Override
	public void setContextualInstances(List<String> instancesURI) {
		Iterator<String> it = instancesURI.iterator();
		this.contextualInstances = new ArrayList<String>();
		
		while(it.hasNext()) {
			contextualInstances.add(it.next() + " " + RDF.type + " " + SemanticReasonerConfiguration.ASAWOO_CTX_PREFIX + "ContextualInstance");
		}
	}
	
	@Override
	public void addInferredPossibility(String adapted, String purposePredicate, String candidate, String score, Integer blankNodeCount) {
		List<String> reif = new ArrayList<String>();
		String bn = "_bn" + blankNodeCount;// + this.scoredAdaptationPossibilities.size();
		reif.add(bn + " " + RDF.subject + " " + adapted);
		reif.add(bn + " " + RDF.predicate + " " + purposePredicate);
		reif.add(bn + " "  + RDF.object + " " + candidate);
		reif.add(bn + " "  + RDF.value + " " + score);
		this.scoredAdaptationPossibilities.add(reif);
	}
	
	@Override
	public String toJenaRule() {
		String jenaRule = "[" + this.name + ": ";
		Iterator<String> instanceIt = this.contextualInstances.iterator();
		Iterator<List<String>> adPIt = this.scoredAdaptationPossibilities.iterator();
		
		while (instanceIt.hasNext()) {
			jenaRule += "(" + instanceIt.next() + ") ";
		}
		
		jenaRule += " -> ";
		
		while (adPIt.hasNext()) {
			Iterator<String> reifIt = adPIt.next().iterator();
			while (reifIt.hasNext()) {
				jenaRule += "(" + reifIt.next() + ") ";
			}
		}
		
		jenaRule += " ] ";
		return jenaRule;
	}
}

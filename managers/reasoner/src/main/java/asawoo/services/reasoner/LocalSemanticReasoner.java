package asawoo.services.reasoner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import asawoo.services.semanticrepo.SemanticRepo;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.felix.ipojo.annotations.*;
import org.apache.jena.graph.Graph;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.InfGraph;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shared.Lock;
import org.apache.jena.update.UpdateAction;
import org.osgi.framework.BundleContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import asawoo.core.config.SemanticReasonerConfiguration;
import asawoo.core.reasoner.AdaptationRule;
import asawoo.core.reasoner.SemanticReasoner;

@Component(name="reasoner")
@Provides
@Instantiate
public class LocalSemanticReasoner implements SemanticReasoner {

	@Context
	private BundleContext bundleContext;

	@Property(name = SemanticReasonerConfiguration.FUNCT_ONTO_FILE)
	private InputStream functionalityOntologyFile;

	@Requires
	private SemanticRepo semanticRepo;

	private Dataset dataset;
	private Model baseModel;

	private Reasoner owlReasoner;
	private Reasoner businessRuleReasoner;

	private String adaptationRulesStr;

	private Logger logger = LoggerFactory.getLogger(LocalSemanticReasoner.class);

	private static final String BASE_MODEL_SPARQL = "" +
			"CONSTRUCT { ?subject ?predicate ?object }\n"+
			"FROM <http://liris.cnrs.fr/asawoo/vocab>\n"+
			"FROM <http://liris.cnrs.fr/asawoo/vocab/context>\n"+
			"FROM <http://liris.cnrs.fr/asawoo/functionalities>\n"+
			"WHERE {\n"+
			"  ?subject ?predicate ?object\n"+
			"}";

	private static final String ADAPTATION_RULES_SPARQL = SemanticReasonerConfiguration.PREFIXES +
			"SELECT ?l\n"+
			"FROM <" + SemanticReasonerConfiguration.CTX_GRAPH_URI + ">\n"+
			"WHERE {\n"+
			"  ?rl a asawoo-ctx:AdaptationRuleList . \n"+
			"  ?rl rdfs:label ?l . \n"+
			"}";

	private static final String RAW_DATA_INIT_SPARQL = SemanticReasonerConfiguration.PREFIXES +
			"INSERT \n { "+
			"  ?dt rdf:value \"" + SemanticReasonerConfiguration.INIT_VALUE + "\" . \n" +
			" } WHERE {\n"+
			"  ?d asawoo-ctx:hasRawDataType ?dt . \n" +
			"}";

	public LocalSemanticReasoner() throws IOException {}

	@Validate
	private void start() {

		if (functionalityOntologyFile != null) {
			logger.info("Start reasoner with model from ontology file (deprecated compabilitity mode)");

			this.baseModel = ModelFactory.createDefaultModel();
			try {
				baseModel.read(functionalityOntologyFile, null, "JSONLD");
				logger.info("Ontology successfully loaded.");
			} catch(Exception ex) {
				logger.error("Error reading the ontology file: " + ex.toString());
			}
		} else {
			logger.info("Start reasoner with model from semantic repo");

			this.baseModel = semanticRepo.construct(BASE_MODEL_SPARQL);
			try {
				this.adaptationRulesStr = semanticRepo.select(ADAPTATION_RULES_SPARQL).nextSolution().getLiteral("l").getString();
				//logger.info("LocalSemanticReasoner.start init businessRuleReasoner with adapatation rules " + adaptationRulesStr);
				this.businessRuleReasoner = new GenericRuleReasoner(Rule.parseRules(this.adaptationRulesStr));
			} catch (Exception ex) {
				logger.error("Cannot retrieve or instantiate adaptation rules from repo.");
				ex.printStackTrace();
			}
		}

		this.owlReasoner = ReasonerRegistry.getOWLReasoner();
		this.dataset = DatasetFactory.create();
		this.dataset.addNamedModel(SemanticReasonerConfiguration.DEFAULT_GRAPH_URI, baseModel);
	}

	@Override
	public synchronized void initReasonerForAvatar(String avatarURI) {
		Model avatarModel = ModelFactory.createOntologyModel().add(this.baseModel);
		this.dataset.addNamedModel(avatarURI, avatarModel);
		this.owlReasoner.bindSchema(avatarModel);
		this.launchUpdateQuery(this.RAW_DATA_INIT_SPARQL, avatarURI);
	}

	@Override
	public synchronized void loadToGraph(InputStream[] files, String graph, String format) {
		Model model = ModelFactory.createDefaultModel();
		for (int i = 0; i < files.length; i++) {
			model.read(files[i], null, format);
		}
		// System.out.println("Load to graph from files " + graph + " " + model);
		this.dataset.addNamedModel(graph, model);
	}

	@Override
	public synchronized void loadToGraph(String data, String graph, String format) {
		Model model;
		if (this.dataset.containsNamedModel(graph)) {
			model = this.dataset.getNamedModel(graph);
		} else {
			model = ModelFactory.createDefaultModel();
		}

		try {
			StringReader sr = new StringReader(data);
			model.read(sr, null, format);
			// System.out.println("Load to graph from string " + graph + " " + model);
			this.dataset.addNamedModel(graph, model);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeReasonerForAvatar(String avatarURI) {
		this.dataset.removeNamedModel(avatarURI);
	}

	@Override
	public synchronized Map<?, ?> launchSelectQuery(String query, String graph) {
		Query sparqlQuery = QueryFactory.create(query);
		Graph avatarGraph = this.dataset.getNamedModel(graph).getGraph();
		InfGraph infGraph = owlReasoner.bind(avatarGraph);
		InfModel infModel = ModelFactory.createInfModel(infGraph);

		infModel.enterCriticalSection(Lock.READ);
		try {
			try (QueryExecution qexec = QueryExecutionFactory.create(sparqlQuery, infModel)) {
				ResultSet results = qexec.execSelect();
				return formatSPARQLResults(results);
			}
		} finally {
			infModel.leaveCriticalSection();
		}
	}

	@Override
	public synchronized List<String> launchContextSelectQuery(String query, String graph) {

		Query sparqlQuery = QueryFactory.create(query);
		Graph avatarGraph = this.dataset.getNamedModel(graph).getGraph();
		// logger.info("LocalSemanticReasoner.launchContextSelectQuery query " + query);
		InfGraph infGraph = businessRuleReasoner.bind(avatarGraph);
		// logger.info("Avatar graph is empty " + graph + " ?" + infGraph.isEmpty());
		InfModel infModel = ModelFactory.createInfModel(infGraph);

		List<String> candidates = new ArrayList<String>();
		try (QueryExecution qexec = QueryExecutionFactory.create(sparqlQuery, infModel)) {
			ResultSet results = qexec.execSelect();
			while (results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				candidates.add(solution.get("candidate").toString());
			}
		}
		logger.info("LocalSemanticReasoner.launchContextSelectQuery candidates " + candidates);
		return candidates;
	}

	public synchronized Map<?, ?> formatSPARQLResults(ResultSet results) {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    ResultSetFormatter.outputAsJSON(outputStream, results);
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    return (Map<?, ?>) gson.fromJson(new String(outputStream.toByteArray()), Map.class);
	}

	@Override
	public synchronized String constructAsNTriples(String query, String graph) throws FileNotFoundException, UnsupportedEncodingException {
		Query sparqlQuery = QueryFactory.create(query);
		Model model = this.dataset.getNamedModel(graph);

		try (QueryExecution qexec = QueryExecutionFactory.create(sparqlQuery, model)) {
		    Model results = qexec.execConstruct();
		    ByteArrayOutputStream o = new ByteArrayOutputStream();
		    RDFDataMgr.write(o, results, Lang.NTRIPLES);
		    return o.toString("utf-8");
		}
	}

	@Override
	public synchronized void launchUpdateQuery(String query, String graph) {
		UpdateAction.parseExecute(query, this.dataset.getNamedModel(graph));
	}

	@Override
	public synchronized void insertTriples(String queryBody) {
		// System.out.println("Insert into default graph " + queryBody);
		String sparqlQuery = "INSERT DATA { " + queryBody + " }";
		this.launchUpdateQuery(sparqlQuery, SemanticReasonerConfiguration.DEFAULT_GRAPH_URI);
	}

	public synchronized void deleteTriples(String queryBody) {
		String sparqlQuery = "DELETE DATA { " + queryBody + " }";
		this.launchSelectQuery(sparqlQuery, SemanticReasonerConfiguration.DEFAULT_GRAPH_URI);
	}

	@Override
	public synchronized void insertTriplesToGraph(String queryBody, String graph) {
		// System.out.println("Insert into avatar graph " + queryBody);
		String sparqlQuery = "INSERT DATA { " + queryBody +  " }";
		this.launchUpdateQuery(sparqlQuery, graph);
	}

	public synchronized void deleteTriplesFromGraph(String queryBody, String graph) {
		String sparqlQuery = "DELETE DATA { " + queryBody +  " }";
		this.launchUpdateQuery(sparqlQuery, graph);
	}

	@Override
	public synchronized void insertNewFunctionalityForAvatar(String functionalityInstance, String functionalityType, String avatarURI) {
		logger.info("********************** SEMANTIC : NEW functionality detected : "+functionalityInstance+" *********");
		if (!this.dataset.containsNamedModel(avatarURI)) {
			this.initReasonerForAvatar(avatarURI);
		}
		String query = "<" + functionalityInstance + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <" + functionalityType + "> . ";
		this.insertTriplesToGraph(query, avatarURI);
	}

	@Override
	public synchronized void insertNewFunctionalitiesForAvatar(Map<String,String> functionalityInstanceType, String avatarURI) {
		if (!this.dataset.containsNamedModel(avatarURI)) {
			this.initReasonerForAvatar(avatarURI);
		}
		for (Entry<String, String> e : functionalityInstanceType.entrySet()) {
			this.insertTriplesToGraph("<" + e.getKey() + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <" + e.getValue() + "> . ", avatarURI);
		}

	}

	@Override
	public synchronized void removeFunctionalityForAvatar(String functionalityInstance, String avatarURI) {
		this.launchUpdateQuery("DELETE WHERE { <" + functionalityInstance + "> ?p ?o }", avatarURI);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public synchronized List<String> getAvailableFunctionalities(String avatarURI) {
		Map<String,Map> results = (Map<String, Map>) this.launchSelectQuery(""
				+ SemanticReasonerConfiguration.PREFIXES
				+ "SELECT DISTINCT ?functClass { "
				+ " {"
				+ "		?funct a asawoo:Functionality . "
				+ "     ?funct a ?functClass . "
			 	+ " } UNION { "
			 	+ "		?functClass asawoo:isComposedOf [ "
                + " 		owl:unionOf/rdf:rest*/rdf:first ?atomicFunctClass ] . "
                + "		?funct a ?atomicFunctClass . "
			 	+ " } MINUS {"
			 	+ "		?functClass asawoo:isComposedOf [ "
                + " 		owl:unionOf/rdf:rest*/rdf:first ?lowerFunctClass ] . "
			 	+ "		FILTER NOT EXISTS { "
			 	+ "			?funct a ?lowerFunctClass . "
				+ "		}"
				+ " }"
			 	+ SemanticReasonerConfiguration.FILTER_FUNCT_CLASS
				+ "}", avatarURI);

		ArrayList<Map> bindings = (ArrayList<Map>) results.get("results").get("bindings");
		return availableFunctBindingsAsArray(bindings, "functClass");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public synchronized Map<String,String[]> getIncompleteFunctionalities(String avatarURI) {
		Map<String,Map> results = (Map<String, Map>) this.launchSelectQuery(""
				 + SemanticReasonerConfiguration.PREFIXES
				 + "SELECT ?functClass (GROUP_CONCAT(DISTINCT ?missingFunctClass) as ?missingFunctClasses) { "
				 + "	?funct a asawoo:Functionality . "
				 + "	?funct a ?lowerFunctClass . "
				 + "	?functClass asawoo:isComposedOf [ "
                + " 		owl:unionOf/rdf:rest*/rdf:first ?lowerFunctClass ] ."
				 + "	?functClass asawoo:isComposedOf [ "
                + " 		owl:unionOf/rdf:rest*/rdf:first ?missingFunctClass ] ."
				 + "    FILTER NOT EXISTS { "
				 + "		?missingFunct a ?missingFunctClass . "
				 + "	}"
				 + SemanticReasonerConfiguration.FILTER_FUNCT_CLASS
				 + "} GROUP BY ?functClass", avatarURI);

		ArrayList<Map> bindings = (ArrayList<Map>) results.get("results").get("bindings");
		return incompleteFunctBindingsAsArray(bindings, "functClass", "missingFunctClasses");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public synchronized List<String> availableFunctBindingsAsArray(List<Map> bindings, String varName) {
		List<String> availableFunctionalities = new ArrayList<String>();
		Iterator<Map> bindingsIt = bindings.iterator();
		while (bindingsIt.hasNext()) {
			Map currentBinding = bindingsIt.next();
			if (!currentBinding.isEmpty()) {
				String functionality = ((Map<String,String>) currentBinding.get(varName)).get("value");
				availableFunctionalities.add(functionality);
			}
		}
		return availableFunctionalities;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public synchronized Map<String,String[]> incompleteFunctBindingsAsArray(List<Map> bindings, String groupByVar, String concatVar) {
		Map<String,String[]> incompleteFunctionalities = new HashMap<String,String[]>();
		Iterator<Map> bindingsIt = bindings.iterator();
		while (bindingsIt.hasNext()) {
			Map currentBinding = bindingsIt.next();
			if (!currentBinding.isEmpty()) {
				String incompleteFunctionality = ((Map<String, String>) currentBinding.get(groupByVar)).get("value");
				String[] missingFunctionalities = ((Map<String,String>) currentBinding.get(concatVar)).get("value").split(" ");
				incompleteFunctionalities.put(incompleteFunctionality, missingFunctionalities);
			}
		}
		return incompleteFunctionalities;
}

	@SuppressWarnings("unchecked")
	@Override
	public synchronized void registerAdaptationRules(Collection<?> rules) {
		List<Rule> parsedRules = new ArrayList<Rule>();
		Iterator<AdaptationRule> adIt = (Iterator<AdaptationRule>) rules.iterator();
		while (adIt.hasNext()) {
			Rule parsedRule = Rule.parseRule(adIt.next().toJenaRule());
			parsedRules.add(parsedRule);
		}

		this.businessRuleReasoner = new GenericRuleReasoner(parsedRules);
	}

	public synchronized void insertRawData(Object data, String sourceTypeURI) {
		String literal = "\"" + String.valueOf(data) + "\"";
		String queryBody = ""
				+ SemanticReasonerConfiguration.PREFIXES
				+ "<" + sourceTypeURI + "> "
				+ "asawoo-ctx:sent "
				+ literal;
		this.insertTriplesToGraph(queryBody, SemanticReasonerConfiguration.CTX_GRAPH_URI);

	}

}

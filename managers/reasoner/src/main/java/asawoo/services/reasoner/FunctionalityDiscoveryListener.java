package asawoo.services.reasoner;

import asawoo.core.util.AsawooUtils;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Context;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Validate;
import org.osgi.framework.BundleContext;

import asawoo.core.functionality.FunctionalityManagerListener;
import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.reasoner.SemanticReasoner;

@Component
@Instantiate
@Provides
public class FunctionalityDiscoveryListener implements FunctionalityManagerListener {

	@Context
	private BundleContext bundleContext;
	
	@Requires
	private SemanticReasoner reasoner;
	
	@Validate
	private void start() {
		//
	}
	
	@Invalidate
	private void stop() {
		//
	}
	
//	private String buildAvatarURI(String avatarId) {		
//		return SemanticReasonerConfiguration.ASAWOO_PREFIX + avatarId;
//	}

	@Override
	public void onAdd(FunctionalityProviderBean elt) {
		String avatarURI = AsawooUtils.buildAvatarURI(elt.getAvatarId());
		String functionalityType = elt.getSemanticType();
		// Todo: put a convenient method to asawoo utils for build functionality instance URI
		String functionalityInstance = avatarURI.replaceAll("/"+elt.getAvatarId(), "") + elt.getBaseUri();
		// (end Todo)
		reasoner.insertNewFunctionalityForAvatar(functionalityInstance, functionalityType, avatarURI);
	}

	@Override
	public void onRemove(FunctionalityProviderBean elt) {
		String functionalityInstance = elt.getBaseUri();
		String avatarURI = AsawooUtils.getAvatarURI(elt);
		reasoner.removeFunctionalityForAvatar(functionalityInstance, avatarURI);		
	}

	@Override
	public void onUpdate(FunctionalityProviderBean elt) {
		//
	}
}

package asawoo.services.reasoner;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Context;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.osgi.framework.BundleContext;

import asawoo.core.config.SemanticReasonerConfiguration;
import asawoo.core.reasoner.AdaptationRule;
import asawoo.core.reasoner.SemanticReasoner;

/**
 * Generates adaptation rules
 * from domain and context information.
 * @author Mehdi Terdjimi
 *
 */
@Component(name="meta-adaptation-engine")
@Instantiate
@Provides(strategy="INSTANCE")
public class MetaAdaptationRuleEngine {
	
	@Context
	private BundleContext bundleContext;
	
	@Requires(filter="(factory.name=reasoner)")
	public SemanticReasoner reasoner;	
	
	/**
	 * Files required to build adaptation rules(contain triples)
	 */
	public static final String FUNCT_ONTO_FILE = "funct-onto-file";
	public static final String DIMENSIONS_INSTANCES_FILE = "ctxt-dim-inst-file";
	public static final String PURPOSES_FILE = "adapt-purp-file";
	public static final String SCORING_FUNCTIONS_FILE = "ctxt-purp-file";
	
	public static final String PREFIXES =
			"PREFIX asawoo-vocab: <http://liris.cnrs.fr/asawoo/vocab#> \n" +
			"PREFIX asawoo-ctx: <" + SemanticReasonerConfiguration.ASAWOO_CTX_PREFIX + "> \n " +
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n" +
			"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
			"PREFIX owl: <http://www.w3.org/2002/07/owl#> \n" +
			"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n";
	
	public static final String META_ADAPTATION_GRAPH = "http://liris.cnrs.fr/asawoo/meta_adaptation_graph";
	
	@Property(name = MetaAdaptationRuleEngine.FUNCT_ONTO_FILE)
	private InputStream functionalityOntologyFile;	
	
	@Property(name = MetaAdaptationRuleEngine.DIMENSIONS_INSTANCES_FILE)
	private InputStream contextualDimensionsInstancesFile;
	
	@Property(name = MetaAdaptationRuleEngine.PURPOSES_FILE)
	private InputStream adaptationPurposesFile;
	
	@Property(name = MetaAdaptationRuleEngine.SCORING_FUNCTIONS_FILE)
	private InputStream scoringFunctionsFile;
	
	public MetaAdaptationRuleEngine() {
		InputStream[] files = new InputStream[4];
		files[0] = this.functionalityOntologyFile;
		files[1] = this.contextualDimensionsInstancesFile;
		files[2] = this.adaptationPurposesFile;
		files[3] = this.scoringFunctionsFile;
		
		reasoner.loadToGraph(files, MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH, "JSONLD");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getDimensions() {
		Map<String,Map> results = (Map<String, Map>) reasoner.launchSelectQuery(
				MetaAdaptationRuleEngine.PREFIXES +
				"SELECT ?dimension { ?dimension a asawoo-ctx:ContextualDimension . }",
				MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH);
		
		ArrayList<Map> bindings = (ArrayList<Map>) results.get("results").get("bindings");		
		return reasoner.availableFunctBindingsAsArray(bindings, "dimension");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getPurposes() {
		Map<String,Map> results = (Map<String, Map>) reasoner.launchSelectQuery(
				MetaAdaptationRuleEngine.PREFIXES +
				"SELECT ?purpose { ?purpose a asawoo-ctx:AdaptationPurpose . }",
				MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH);
		
		ArrayList<Map> bindings = (ArrayList<Map>) results.get("results").get("bindings");		
		return reasoner.availableFunctBindingsAsArray(bindings, "purpose");
	}
	
	public String generateSPARQLSituationQuery(String purpose) {
		String bodyQuery = "";
		String variables = "";

		ArrayList<String> dimensions = (ArrayList<String>) this.getDimensions();
		Iterator<String> dimIt = dimensions.iterator();
		
		ArrayList<String> instances = new ArrayList<String>();
		int k = 0;

		while (dimIt.hasNext()) {
			String i = "?i" + k;
			instances.add(i);

			bodyQuery += " OPTIONAL { " +
				i + " a asawoo-ctx:ContextualInstance . \n" +
				i + " asawoo-ctx:instanceForPurpose <" + purpose + "> . \n" +		
				i + " asawoo-ctx:instanceFromDimension <" + dimIt.next() + "> . \n\n";

			bodyQuery += " } ";

			variables += i + ' ';
			k++;
		}		
		return MetaAdaptationRuleEngine.PREFIXES + " SELECT DISTINCT " + variables + " { " + bodyQuery + " } ";
	}
	
	public List<String> generateSPARQLSituationQueries() {
		ArrayList<String> queries = new ArrayList<String>();
		ArrayList<String> purposes = (ArrayList<String>) this.getPurposes();
		Iterator<String> purpIt = purposes.iterator();
		
		while (purpIt.hasNext()) {
			String query = this.generateSPARQLSituationQuery(purpIt.next());
			queries.add(query);
		}
		return queries;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String generateSituations() {
		String situations = "";
		ArrayList<String> queries = (ArrayList<String>) this.generateSPARQLSituationQueries();
		Iterator<String> queryIt = queries.iterator();
		
		int k = 0;
		while (queryIt.hasNext()) {				
			Map<String,Map> responses = (Map<String, Map>) this.reasoner.launchSelectQuery(queryIt.next(), MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH);
			ArrayList<Map> bindings = (ArrayList<Map>) responses.get("results").get("bindings");
			Iterator<Map> bIt = bindings.iterator();			
			
			while (bIt.hasNext()) {
				String currentSituation = "<" + SemanticReasonerConfiguration.ASAWOO_CTX_PREFIX + "/S-"+k+"> ";				
				situations += currentSituation + " <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <" + SemanticReasonerConfiguration.ASAWOO_CTX_PREFIX + "/ContextualSituation> . \n";
				Map<String,Map> binding = bIt.next();
					
				for (Map.Entry<String, Map> entry: binding.entrySet()) {
					situations += 	currentSituation +
									"<" + SemanticReasonerConfiguration.ASAWOO_CTX_PREFIX + "/containsInstance> " +
									"<" + entry.getValue().get("value") + "> . \n";

				}	
				k++;
			}	
		}		
		return situations;
	}
	
	public String generateAdaptationPossibilities() throws FileNotFoundException, UnsupportedEncodingException {
		return reasoner.constructAsNTriples(MetaAdaptationRuleEngine.PREFIXES +
				"CONSTRUCT { ?adapted ?possibilityPred ?candidate } " +
				"WHERE { " +
			 	"?purpose asawoo-ctx:purposePredicate ?possibilityPred . " +	
				"?purpose a asawoo-ctx:AdaptationPurpose . " +
			 	
			 	"?possibilityPred rdfs:domain asawoo-vocab:Functionality . " + 			
			 	"?possibilityPred rdfs:range ?candidateClass . " +
			 	
			 	"?adapted a asawoo-vocab:Functionality . " +
				"?candidate a ?candidateClass . } ",
				MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Map> calculateScores() throws FileNotFoundException, UnsupportedEncodingException {
		String situations = this.generateSituations();
		String possibilities = this.generateAdaptationPossibilities();
		reasoner.loadToGraph(situations + possibilities, MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH, "NTRIPLES");
		Map<?,?> results = reasoner.launchSelectQuery(PREFIXES +
				"SELECT DISTINCT ?adapted ?purposePred ?candidate " +
				"	(SUM(xsd:float(?score)) AS ?candidateScore) " +
				"	(GROUP_CONCAT(?contextualInstance) AS ?instances) { " +
				"		?adapted ?purposePred ?candidate . " +
				"		?purpose asawoo-ctx:purposePredicate ?purposePred .	" +
				"		    ?contextualSituation asawoo-ctx:containsInstance ?contextualInstance . " +	
							
				"			?scoringFunction asawoo-ctx:scores ?BN . " +
				"			?scoringFunction asawoo-ctx:applicableTo ?purpose . " +    
				"			?BN asawoo-ctx:withInstance ?contextualInstance . " +
				"			?BN asawoo-ctx:forCandidate ?candidate . " +
				"			?BN asawoo-ctx:forAdapted ?adapted . " +
				"			?BN rdf:value ?score .	" +
						 
				"} GROUP BY ?adapted ?purposePred ?candidate ?contextualSituation",
				MetaAdaptationRuleEngine.META_ADAPTATION_GRAPH);
		return (Map<String, Map>) results;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<AdaptationRule> generateScoredAdaptationRules() throws FileNotFoundException, UnsupportedEncodingException {
		Map<String, Map> scores = this.calculateScores();
		ArrayList<Map> bindings = (ArrayList<Map>) scores.get("results").get("bindings");
		Iterator<Map> bIt = bindings.iterator();		
		
		Map<String , AdaptationRule> adaptationRules = new HashMap<String, AdaptationRule>();
		Integer count = 0;
		
		while(bIt.hasNext()) {
			count++;
			Map<String,Map> binding = bIt.next();
			String iStr = (String) binding.get("instances").get("value");
			
			String currentAdapted = (String) binding.get("adapted").get("value");			
			String currentPurposePredicate = (String) binding.get("purposePred").get("value");			
			String currentCandidate = (String) binding.get("candidate").get("value");
			String currentScore = "\"" + ((String)  binding.get("candidateScore").get("value")) + "\"";
			
			List<String> instances = Arrays.asList(iStr.split(" "));			
			
			if (adaptationRules.get(iStr) == null) {
				AdaptationRule adRule = new AdaptationRuleImpl(instances, Integer.toString(iStr.hashCode()));				
				adaptationRules.put(iStr, adRule);				
			}
			adaptationRules.get(iStr).addInferredPossibility(
					currentAdapted,
					currentPurposePredicate,
					currentCandidate,
					currentScore, count);
		}
		
		return adaptationRules.values();
	}
	
	
	
	
	
	
	
	
	
	
	
	

}

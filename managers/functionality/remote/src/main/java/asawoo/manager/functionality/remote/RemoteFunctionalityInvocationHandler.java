package asawoo.manager.functionality.remote;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */


import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.util.AsawooUtils;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.List;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 20/07/17.
 */
public class RemoteFunctionalityInvocationHandler implements InvocationHandler {


    private FunctionalityProviderBean functionality;
    private HttpClient httpClient;

    public RemoteFunctionalityInvocationHandler(FunctionalityProviderBean functionality, HttpClient httpClient) {

        this.functionality = functionality;
        this.httpClient = httpClient;

    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {

        // requirements:
        // object types
        // functionality Uris
        // cast java objects into json parameters

        // FIXME asynch issues : if a return value is expected (no void) ?
        // FIXME : or make it synchronous ? by listening to result in another thread and waiting for it in the main thread ?



        Method[] methods = o.getClass().getDeclaredMethods();

        for (Method m : methods) {
            if (m.getName().equals(method.getName())) {
                // cast parameters then invoke through HTTP client

                if (m.getReturnType().equals(Void.TYPE)) {
                    // easy case, we do not care about asynchronicity
                    FunctionalityRequest fr = functionality.getMethods().get(m.getName());

                    URI uri = URI.create(AsawooUtils.getFullURI(functionality, fr.url));

                    String jsonBody = buildJsonBodyFromParameters(method, fr.parameters, objects);

                    switch (fr.verb) {
                        case "GET" : ;
                            HttpClientRequest request = httpClient.get(uri.toString());
                            if (jsonBody != null && jsonBody.length() > 0) {
                                request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                        .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(jsonBody.length()))
                                        //.handler(responseHandler)
                                        .write(jsonBody);
                            }
                            request.end();

                            break;
                    }
                }

            }
        }




        return null;
    }

    private String buildJsonBodyFromParameters(Method method, List<String> parameters, Object[] paramValues) {

        JsonObject paramMap = new JsonObject();

        for (int i=0; i < parameters.size(); i++) {
            // TODO FIXME : test and refine
            String param = parameters.get(i);
            paramMap.put(param, paramValues[i]);
        }

        return paramMap.encode();

    }
}

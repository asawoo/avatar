package asawoo.manager.functionality.remote;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityInterface;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 19/07/17.
 */
public interface FunctionalityProxy<T extends FunctionalityInterface> {



}

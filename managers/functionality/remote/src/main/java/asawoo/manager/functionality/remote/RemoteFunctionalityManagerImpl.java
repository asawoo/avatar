package asawoo.manager.functionality.remote;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.net.rest.RestRegistryListener;
import asawoo.core.net.rest.RestServiceRegistry;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import org.apache.felix.ipojo.annotations.*;
import org.osgi.framework.BundleContext;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 19/07/17.
 */
@Component
@Provides
@Instantiate
public class RemoteFunctionalityManagerImpl /*implements FunctionalityManager*/ implements RestRegistryListener {

    @Context
    BundleContext bundleContext;

    @Requires
    private RestServiceRegistry registry;

    @Requires
    private Vertx vertx;

    private HttpClient vertxHttpClient;

    private List<FunctionalityProxy> functionalityProxies;

    @Validate
    private void start() {
        registry.addListener(this);
        vertxHttpClient = vertx.createHttpClient();
    }

    @Invalidate
    private void stop() {
        registry.removeListener(this);
    }

    private String getFunctionalityInstanceName(String avatarId, String functionalityName) {
        return avatarId+functionalityName;
    }

    @Override
    public void onServiceAdded(RegistryEntryBean reb) {
        if (!reb.isLocal()) {
            Class funcClass = reb.getImplementedFunctionality(); // FIXME: this field is transient, better do the following:
            try {
                funcClass = bundleContext.getBundle().loadClass(reb.getInterfaceName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            /* TODO
            1. instantiate a generic proxy for this particular functionality
            2. publish it as an OSGi service
            3. with specific service properties (avatarId, exposable=false to avoid republication in registry)
             */

            FunctionalityInterface proxyFunctionality = (FunctionalityInterface) FunctionalityProxyImpl.newProxyInstance(this.getClass().getClassLoader(),
                    new Class[]{funcClass},
                    new RemoteFunctionalityInvocationHandler(reb, vertxHttpClient));

            Dictionary serviceProperties = new Hashtable<>();
            // TODO add specific service properties
            serviceProperties.put(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID, reb.getAvatarId());
            serviceProperties.put("exposable", false);

            bundleContext.registerService(funcClass.getName(), proxyFunctionality, serviceProperties);

//            FunctionalityProxy<funcClass> proxy = new FunctionalityProxyImpl<>();
            try {
                funcClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onServiceModified(RegistryEntryBean reb) {

    }

    @Override
    public void onServiceRemoved(RegistryEntryBean reb) {
        if (!reb.isLocal()) {

        }
    }
}

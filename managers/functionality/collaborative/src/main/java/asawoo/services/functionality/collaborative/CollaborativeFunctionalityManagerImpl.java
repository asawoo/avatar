/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.functionality.collaborative;

import asawoo.core.annotation.Functionality;
import asawoo.core.functionality.CollaborativeFunctionalityManager;
import asawoo.core.functionality.FunctionalityBean;
import asawoo.core.functionality.FunctionalityManager;
import asawoo.core.reasoner.SemanticReasoner;
import asawoo.core.util.AsawooUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Provides
@Instantiate
public class CollaborativeFunctionalityManagerImpl implements CollaborativeFunctionalityManager {

	private Logger logger = LoggerFactory.getLogger(CollaborativeFunctionalityManagerImpl.class);

	@Requires(optional = true, proxy = false)
	private SemanticReasoner reasoner;

	@Requires(proxy=false)
	private FunctionalityManager functionalityManager;

	/**
	 * Get incomplete collaborative functionalities that
	 * are not yet associated to an avatar
	 * and that can be completed by other avatars
	 *
	 * @param avatarId
	 * @return
	 */
	public Collection<FunctionalityBean> getAvailableCollaborativeFunctionalities(String avatarId) {
		Collection<FunctionalityBean> funcs = Sets.newHashSet();

		Map<String,String[]> incompleteFunctionalityUris = reasoner.getIncompleteFunctionalities(AsawooUtils.buildAvatarURI(avatarId));
		List<String> activeFunctionalitiesUris = functionalityManager.getFunctionalities().stream().map(f -> f.getSemanticType()).collect(Collectors.<String> toList());
		incompleteFunctionalityUris.keySet().forEach(incompleteF -> {
			String[] missingFunctionalities = incompleteFunctionalityUris.get(incompleteF);
			Boolean possibleCollaboration = true;

			// Already taken by another avatar : no collaborative instantiation
			if (activeFunctionalitiesUris.contains(incompleteF)) {
				// logger.info("No collaboration for " + incompleteF + ", an instance is already active");
				possibleCollaboration = false;
			}

			// Not all missing parents are present : no collaborative instantiation
			for(int i = 0; i < missingFunctionalities.length; i++) {
				String missingF = missingFunctionalities[i];
				if (!activeFunctionalitiesUris.contains(missingF)) {
					// logger.info("No collaboration for "+ incompleteF + " no active instance of parent " + missingF);
					possibleCollaboration = false;
				}
			}

			if (possibleCollaboration) {
				logger.info("Add incomplete functionality to list of available functionalities for collaborative mode " + incompleteF);
				funcs.addAll(functionalityManager.getFunctionalityBySemanticType(incompleteF));
			}
		});
		return funcs;
	}
}

/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.functionality.local;

import asawoo.core.annotation.Functionality;
import asawoo.core.annotation.Parameter;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.*;
import asawoo.core.reasoner.SemanticReasoner;
import asawoo.core.util.AsawooUtils;
import asawoo.core.util.CollectionListener;
import asawoo.core.util.PlatformUtils;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.bundlerepository.*;
import org.apache.felix.ipojo.Factory;
import org.apache.felix.ipojo.Nullable;
import org.apache.felix.ipojo.annotations.*;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;

import javax.print.attribute.ResolutionSyntax;
import javax.ws.rs.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author lesommer, lionel touseau <lionel.touseau at univ-ubs.fr>
 *
 */
@Component
@Provides
@Instantiate
public class LocalFunctionalityManager implements FunctionalityManager {

    private static final String DEPRECATED_AVATAR_ID = "deprecated-avatar";

	private Logger logger = LoggerFactory.getLogger(LocalFunctionalityManager.class);

	@Context
    private BundleContext bundleContext;

	@Requires(optional = true, proxy = false)
	private SemanticReasoner reasoner;

	// <avatarId, <functionalityName, bean> or <functionalityInterface, bean> >
//	private Map<String, Map<String, FunctionalityProviderBean>> functionalitiesByAvatar;

	private Map<String, FunctionalityBean> availableFunctionalities;

    /**
     * functionality to factories view of this many-to-many relationship.
     * Several factories can create instances implementing a same functionality
     */
    private Multimap<FunctionalityBean, Factory> functionality2Factories;

    /**
     * factory to functionalities view of this many-to-many relationship.
     * A factory may create instances that implement multiple functionalities
     */
    private Multimap<Factory, FunctionalityBean> factory2functionalities;

	private Multimap<String, FunctionalityProviderBean> functionalities;

	/* Map<AvatarId, Listeners> */
	private Map<String, Set<FunctionalityManagerListener>> avatarListeners;

	// global listeners
	private Set<FunctionalityManagerListener> listeners;
	private Set<CollectionListener> factoryListeners;

	public LocalFunctionalityManager() {
		this.availableFunctionalities = Maps.newHashMap();
        this.functionality2Factories = HashMultimap.create();
        this.factory2functionalities = HashMultimap.create();
		this.functionalities = ArrayListMultimap.create();
		this.avatarListeners = Maps.newHashMap();
		this.listeners = Sets.newHashSet();
		this.factoryListeners = Sets.newHashSet();
	}

    @Bind(optional = true, aggregate = true)
    private void bindListener(FunctionalityManagerListener listener) {
        // register this listener
        addListener(listener);
    }

    @Unbind(optional = true, aggregate = true)
    private void unbindListener(FunctionalityManagerListener listener) {
        // register this listener
        removeListener(listener);
    }

	/**
	 * iPOJO callback method: when Factory of components implementing FunctionalityInterface are discovered, the map ov available functionalities is updated
	 * @param factory
     */
	@Bind(aggregate = true, optional = true) // TODO how to set up a dynamic filter ?? instead of binding to each factory ?
	private void bindFunctionalityFactory(Factory factory) {

		if (!AsawooUtils.isFunctionalityFactory(factory)) {
			return;
		}

		// Prevent creating duplicates
        if (factory2functionalities.containsKey(factory)) {
			return;
		}

		try {
			// if this factory is a functionality factory, parse its metadata and adds it to available functionalities
			Class componentClass = factory.getBundleContext().getBundle().loadClass(factory.getName());

			Class[] implementedFunctionalities = componentClass.getInterfaces();
			for (Class iface : implementedFunctionalities) {
				if (FunctionalityInterface.class.isAssignableFrom(iface)) {

					if (iface.isAnnotationPresent(Functionality.class)) {

                        FunctionalityBean fb = parseFunctionality(iface);

						availableFunctionalities.put(fb.getName(), fb);

                        functionality2Factories.put(fb, factory);

						factory2functionalities.put(factory, fb);
					}
				}
			}

			for (CollectionListener l : this.factoryListeners) {
				l.onAdd(factory);
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

    @Unbind(optional = true, aggregate = true)
    public void unbindFunctionalityFactory(Factory factory) {
        if (!AsawooUtils.isFunctionalityFactory(factory)) {
            return;
        }

        factory2functionalities.removeAll(factory);

        Iterator<Map.Entry<FunctionalityBean, Factory>> iterator = functionality2Factories.entries().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getValue().equals(factory)) {
                iterator.remove();
            }
        }

    }

    @Override
    public Collection<Factory> getImplementingFactories(String functionalityName) {
        Multimap<FunctionalityBean, Factory> filteredFactories = Multimaps.filterKeys(functionality2Factories, (Predicate<FunctionalityBean>) input -> (input.getName().equals(functionalityName)));
        if (filteredFactories != null && !filteredFactories.isEmpty()) {
            return filteredFactories.values();
        } else {
            return null;
        }
    }

    @Override
    public Factory getFunctionalityFactory(String factoryName) {
        Set<Factory> factories = Sets.filter(factory2functionalities.keySet(),
                (Predicate<Factory>) input -> input.getName().equals(factoryName));

        if (factories == null || factories.isEmpty()) {
            return null;
        } else {
            return factories.iterator().next();
        }

    }

    @Override
    public Collection<FunctionalityBean> getFunctionalityByFactory(String factoryName) {
        Multimap<Factory, FunctionalityBean> filteredFunctionalities = Multimaps.filterKeys(factory2functionalities, (Predicate<Factory>) input -> (input.getName().equals(factoryName)));
        if (filteredFunctionalities != null && !filteredFunctionalities.isEmpty()) {
            return filteredFunctionalities.values();
        } else {
            return null;
        }
    }

    @Override
    public Collection<FunctionalityBean> getFunctionalityByFactory(Factory factory) {
        return factory2functionalities.get(factory);
    }

	@Override
	public Collection<FunctionalityBean> getFunctionalityBySemanticType(String uri) {
		return Collections2.filter(functionality2Factories.keySet(), (Predicate<FunctionalityBean>) input -> uri.equals(input.getSemanticType()));
	}

	@Override
    public FunctionalityBean getFunctionalityBeanByInterface(Class<? extends FunctionalityInterface> functionalityInterface) {
        Set<FunctionalityBean> availableFunctionalities = functionality2Factories.keySet();
        FunctionalityBean matchingAvailableFunc = null;
        for (FunctionalityBean fb : availableFunctionalities) {
            if (fb.getClass().equals(functionalityInterface)) {
                matchingAvailableFunc = fb;
            }
        }
        return matchingAvailableFunc;

    }

	@Override
	public void addFunctionality(String avatarId, FunctionalityProviderBean func) {

		functionalities.put(avatarId, func);

		//notification of listeners
		if (this.avatarListeners.containsKey(avatarId)) {
			for (FunctionalityManagerListener l : this.avatarListeners.get(avatarId)) {
				l.onAdd(func);
			}
		}
		// as well as global listeners
		for (FunctionalityManagerListener l : this.listeners) {
			l.onAdd(func);
		}
	}

    @Override
    public Collection<FunctionalityProviderBean> getFunctionalities() {
        return functionalities.values();
    }

    @Override
    public Collection<FunctionalityProviderBean> getFunctionalitiesByAvatar(String avatarId) {
        return functionalities.get(avatarId);
    }

    @Override
	public Collection<String> getAvailableFunctionalities() {
		return Collections2.transform(getAvailableFunctionalities(null), input -> input.getName());
	}

    @Override
    public Collection<FunctionalityBean> getAvailableFunctionalities(String avatarId) {

        // filter available functionalities
        Collection<FunctionalityBean> availableFunctionalities = Sets.newHashSet();

        if (reasoner != null && !(reasoner instanceof Nullable)) {
            // System.out.println("///// REASONER NOT NULL /////");
            // FIXME might also get FunctionalityProviderBean for this avatar and extract avatarUri
            List<String> functionalityUris = reasoner.getAvailableFunctionalities(AsawooUtils.buildAvatarURI(avatarId));
            availableFunctionalities.addAll(Collections2.filter(functionality2Factories.keySet(),
                    (Predicate<FunctionalityBean>) input -> functionalityUris.contains(input.getSemanticType())));

        } else {
            // System.out.println("///// NOT USING REASONER (NULL) /////");
            availableFunctionalities.addAll(functionality2Factories.keySet());
        }

        return availableFunctionalities;
    }

    @Override
	public Collection<String> getEnabledFunctionalities() {
		return getEnabledFunctionalities(null);
	}

    public Collection<String> getEnabledFunctionalities(String avatarId) {

		Collection<FunctionalityProviderBean> funcBeans;
		if (avatarId != null && this.functionalities.containsKey(avatarId)) {
			funcBeans = this.functionalities.get(avatarId);
		} else {
			funcBeans =  this.functionalities.values();
		}
		return Collections2.transform(funcBeans, new Function<FunctionalityProviderBean, String>() {
			@Override
			public String apply(FunctionalityProviderBean input) {
				return input.getInterfaceName();
			}
		});

    }

	@Override
	public void removeFunctionality(FunctionalityProviderBean functionality) {
		functionalities.remove(functionality.getAvatarId(), functionality);
	}

	@Override
    public void removeFunctionality(String avatarId, String name) {

		Collection<FunctionalityProviderBean> funcs = functionalities.get(avatarId);

    	if (funcs != null) {

            for (FunctionalityProviderBean fb : funcs) {
                if (name.equals(fb.getName())) {
                    funcs.remove(fb);

                    // notify listeners
                    if (this.avatarListeners.containsKey(avatarId)) {
                        for (FunctionalityManagerListener l : this.avatarListeners.get(avatarId)) {
                            l.onRemove(fb);
                        }
                    }
                    // as well as global listeners
                    for (FunctionalityManagerListener l : this.listeners) {
                        l.onRemove(fb);
                    }

                }
            }

		}
    }

	@Override
    @Deprecated
	public FunctionalityProviderBean getFunctionalityByInterface(String avatarId, Class<? extends FunctionalityInterface> functionalityInterface) {

		Collection<FunctionalityProviderBean> filterableFuncs;
		if (avatarId != null && functionalities.containsKey(avatarId)) {
			filterableFuncs = functionalities.get(avatarId);
		} else {
			// all
			filterableFuncs = functionalities.values();
		}

		FunctionalityProviderBean func = null;
		Collection<FunctionalityProviderBean> matchingFuncs = Collections2.filter(filterableFuncs,
				(Predicate<FunctionalityProviderBean>) input -> (input.getInterfaceName().equals(functionalityInterface.getName())));

		// TODO do not take the first one, several functionality providers may match
		if (matchingFuncs != null && matchingFuncs.iterator().hasNext()) {
			func = matchingFuncs.iterator().next();
		}



		// new functionality
		if (func == null) {
			// parse the functionality annotations
			Functionality funcAnnotation = null;
			if (functionalityInterface.isAnnotationPresent(Functionality.class)) {
				funcAnnotation = (Functionality) functionalityInterface.getAnnotation(Functionality.class);
			} else {
				// try to look for implemented interfaces
				for (Class funcItf : functionalityInterface.getInterfaces()) {
					if (funcItf.isAnnotationPresent(Functionality.class)) {
						funcAnnotation = (Functionality) funcItf.getAnnotation(Functionality.class);

						// TODO do not break, instead return an array of FunctionalityProviderBean ?? or keep this method for a single interface ?
						break;
					}
				}
			}

			if (funcAnnotation == null) {
				System.out.println("NOT ANNOTATED INTERFACE - "+functionalityInterface.getAnnotations().length+" annotations found");
				return null;
			}

			String functionalityName;
			String functionalityDescription;

			if (funcAnnotation.name() != null && !funcAnnotation.name().isEmpty()) {
				functionalityName = funcAnnotation.name();
			} else {
				functionalityName = functionalityInterface.getSimpleName();
			}

			functionalityDescription = funcAnnotation.description();

			func = new FunctionalityProviderBean(functionalityName,
                                                functionalityDescription,
                                                functionalityInterface);

			addFunctionality(DEPRECATED_AVATAR_ID, func);
		}

		return func;
	}

	@Override
	public FunctionalityProviderBean getOrAddFunctionality(String avatarId,
                                                           Class implementedFunctionalityClazz,
                                                           FunctionalityInterface functionalityInterface,
                                                           String instanceName,
                                                           Map<String, Object> serviceProperties) {

        FunctionalityProviderBean func = null;

        Collection<FunctionalityProviderBean> filterableFuncs;
		if (avatarId != null && functionalities.containsKey(avatarId)) {
			filterableFuncs = functionalities.get(avatarId);

			// filter functionalities on functionality Interface classname as well as provider ID (i.e., instance name)
			Collection<FunctionalityProviderBean> matchingFuncs = Collections2.filter(filterableFuncs,
					(Predicate<FunctionalityProviderBean>) input -> (input.getInterfaceName().equals(implementedFunctionalityClazz.getName())
							&& input.getInstanceName().equals(instanceName)));

			// Filtering should leave only one element
			if (matchingFuncs != null && matchingFuncs.iterator().hasNext()) {
				func = matchingFuncs.iterator().next();
			}

		} else {
			// TODO ignore if not found
//			// all
//			filterableFuncs = functionalities.values();
		}

		// new functionality
		if (func == null) {

            // TODO get FunctionalityBean, then build FPB around it
            FunctionalityBean fb = getFunctionalityBeanByInterface(implementedFunctionalityClazz);
            if (fb == null) {
                // FIXME that cas should never happen...
                fb = parseFunctionality(implementedFunctionalityClazz);
                // TODO add to available functionalities
            }

            /*
			// parse the functionality annotations
			Functionality funcAnnotation = null;
			if (implementedFunctionalityClazz.isAnnotationPresent(Functionality.class)) {
				funcAnnotation = (Functionality) implementedFunctionalityClazz.getAnnotation(Functionality.class);
			} else {
				// TODO this case should never happen if a valid functionalityInterface is given as parameter
				// try to look for implemented interfaces
				for (Class funcItf : implementedFunctionalityClazz.getInterfaces()) {
					if (funcItf.isAnnotationPresent(Functionality.class)) {
						funcAnnotation = (Functionality) funcItf.getAnnotation(Functionality.class);

						// TODO do not break, instead return an array of FunctionalityProviderBean ?? or keep this method for a single interface ?
						break;
					}
				}
			}

			if (funcAnnotation == null) {
				System.out.println("NOT ANNOTATED INTERFACE - "+implementedFunctionalityClazz.getAnnotations().length+" annotations found");
				return null;
			}

			String functionalityName;
			String functionalityDescription;
            String functionalitySemanticType;

			if (funcAnnotation.name() != null && !funcAnnotation.name().isEmpty()) {
				functionalityName = funcAnnotation.name();
			} else {
				functionalityName = implementedFunctionalityClazz.getSimpleName();
			}

			functionalityDescription = funcAnnotation.description();

            functionalitySemanticType = funcAnnotation.semanticType();
            */

			func = new FunctionalityProviderBean(fb,
                                                avatarId,
                                                functionalityInterface,
                                                instanceName,
                                                AsawooUtils.buildFunctionalityBaseUri(avatarId, fb.getName()),
                                                serviceProperties);

			addFunctionality(avatarId, func);
		}

		return func;
	}

    @Override
	@Deprecated
	public FunctionalityInterface getFunctionality(String name) {
        try {
            return getFunctionalityByInterface(DEPRECATED_AVATAR_ID, (Class<? extends FunctionalityInterface>) Class.forName(name)).getFunctionalityProvider();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
	}


	@Override
	@Deprecated
	public FunctionalityInterface spawnFunctionality(Dictionary<String,String> props) {
		String name = props.get("name");
		String factoryClassName = props.get("factory");
		if (name==null) {
			System.out.println("ERROR LocalFunctionalityManager.spawn: instance name not defined");
			return null;
		}
		if (factoryClassName==null) {
			System.out.println("ERROR LocalFunctionalityManager.spawn: factoryClassName not defined");
			return null;
		}

		FunctionalityFactory factory=(FunctionalityFactory) getService(factoryClassName);
		if (factory==null) {
			System.out.println("ERROR LocalFunctionalityManager: Spawn: could not obtain factory "+factoryClassName);
			return null;
		}
		else {
			System.out.println("LocalFunctionalityManager: obtained factory "+factoryClassName);
		}

		FunctionalityInterface spawned=factory.instantiate(props);
		if (spawned==null) {
			System.out.println("LocalFunctionalityManager: ERROR Factory returned null for "+factoryClassName);
			return null;
		}
		else {
			System.out.println("LocalFunctionalityManager: spawned from"+factoryClassName);
		}
        // TODO // FIXME: 28/03/17 dummy avatarId
        this.addFunctionality(DEPRECATED_AVATAR_ID, new FunctionalityProviderBean(name, name, null, DEPRECATED_AVATAR_ID, spawned, name, null, null, null));
		return spawned;
	}

    @Override
    public Object invoke(String instanceName,String method,Dictionary<String,String> parameters) {

        FunctionalityInterface func=null;

        try {
            FunctionalityProviderBean fb = getFunctionalityByInterface(DEPRECATED_AVATAR_ID, (Class<? extends FunctionalityInterface>) Class.forName(instanceName));
            func = fb.getFunctionalityProvider();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    	Object ret=null;
        if (func!=null) {
        	try {
				Method[] methods=func.getClass().getMethods();
				Method m=null;
				for (int i=0;i<methods.length;i++) {
					if (methods[i].getName().equals(method)) {
						m=methods[i];
						break;
					}

				}
				if (m!=null) {
					Class<?> ptypes[]=m.getParameterTypes();
        			int nargs=m.getParameterTypes().length;
        			Method imethod=getInterfaceMethod(func,method);
        			Annotation[][] anns=(Annotation[][])imethod.getParameterAnnotations();
        			ArrayList<String> ordered=new ArrayList<String>();
        			for (int i=0;i<nargs;i++) {
        				String pname=((Parameter)anns[i][0]).name();
        				ordered.add(parameters.get(pname));
        			}


					ArrayList<Object> args=new ArrayList<Object>();
					for (int i=0;i<ordered.size();i++) {
						String v=ordered.get(i);
						if (ptypes[i].equals(int.class)) {
							args.add(Integer.parseInt(v));
							System.out.println("Adding int argument "+v);
						}
						else if (ptypes[i].equals(float.class)) {
							args.add(Float.parseFloat(v));
							System.out.println("Adding float argument "+v);
						}
						else if (ptypes[i].equals(double.class)) {
							args.add(Double.parseDouble(v));
							System.out.println("Adding double argument "+v);
						}
						else  {
							args.add(v);
							System.out.println("Adding string argument "+v);
						}
					}

					ret=m.invoke(func, args.toArray());
				}
				else {
					System.out.println("ERROR: No method "+method+" found for appliance "+instanceName);
				}

			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

    	return ret;
    }

    public Method getInterfaceMethod(Object o,String methodname) {
    	return getInterfaceMethod(o.getClass(), methodname);
    }

    public Method getInterfaceMethod(Class cls,String methodname) {
    	Class<?>[] interfaces=cls.getInterfaces();
    	for (Class<?> iface:interfaces) {
    		if (FunctionalityInterface.class.isAssignableFrom(iface)) {

    			Method[] methods=iface.getMethods();
            	for (Method m:methods) {
            		if (m.getName().equals(methodname)) return m;
            	}
    		}
    	}
    	return null;
    }

    public Object getService(String className) {
    	ServiceReference ref=bundleContext.getServiceReference(className);
		if (ref==null) {
			System.out.println("ERROR LocalFunctionalityManager: could not get service reference to "+className);
			return null;
		}
		Object obj=bundleContext.getService(ref);
		if (obj==null) {
			System.out.println("ERROR LocalFunctionalityManager: could not get service "+className);
			return null;
		}
		return obj;
    }

    public FunctionalityRequest getMethodRequest(String functionality,String appliance,String method) {
    	String filter="(name="+appliance+")";
    	ServiceReference refs[]=null;
		try {
			refs = bundleContext.getServiceReferences(functionality,filter);
		} catch (InvalidSyntaxException e) {
			e.printStackTrace();
			return null;
		}
		Class cls;
		try {
			cls=Class.forName(functionality);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		String host="127.0.0.1";
		String port="8080";
		{
			ServiceReference ref=bundleContext.getServiceReference(HttpService.class.getName());
			if (ref==null) {
				System.out.println("ERROR: Could not obtain service reference to HTTP Service");
				return null;

			}

			port=(String)ref.getProperty("org.osgi.service.http.port");

			/*
			String[] strings=ref.getPropertyKeys();

			  for (String s:strings) {
				Object v=ref.getProperty(s);
				System.out.println("\tProperty:"+s+" value:"+v);
			}
			*/
		}
    	if (refs!=null) {
	    	for (ServiceReference ref: refs) {
	    		Object obj=bundleContext.getService(ref);
	    		if (obj==null) {
	    			System.out.println("ERROR LocalFunctionalityManager: could not get service "+functionality);
	    			continue;
	    		}
	    		else {
	    			for (Method m:cls.getMethods()) {
	    				if (m.getName().equals(method)) {
	    					String exposed=(String)ref.getProperty("exposed");
	    					if (exposed==null || !exposed.equals("true")) {
	    						System.out.println("WARNING: "+functionality+"."+appliance+"."+method+" is not exposed");
	    						continue;
	    					}


	    					/// GENERATE URL
	    					String url=(String)ref.getProperty("org.apache.cxf.rs.httpservice.context");
	    					url="http://"+host+":"+port+""+url;
	    					Path ann=m.getAnnotation(Path.class);
	    					if (ann!=null) {
	    						url+=ann.value();
	    					}
	    					System.out.println("LocalFunctionalityManager: getting URL for "+functionality+"."+appliance+"."+method);
	    					System.out.println("LocalFunctionalityManager: URL obtained "+url);

	    					/// GENERATE VERB
	    					String verb=null;
	    					if (m.getAnnotation(GET.class)!=null) verb="GET";
	    					if (m.getAnnotation(POST.class)!=null) verb="POST";
	    					if (m.getAnnotation(PUT.class)!=null) verb="PUT";
	    					if (m.getAnnotation(DELETE.class)!=null) verb="DELETE";
	    					if (verb==null) {
	    						System.out.println("ERROR: LocalFunctionalityManager: could not find VERB");
	    						continue;
	    					}
	    					System.out.println("LocalFunctionalityManager: VERB obtained "+verb);


	    					/// GENERATE PARAMETER LIST
	    					ArrayList<String> parameters=new ArrayList<String>();

	    					Class<?> ptypes[]=m.getParameterTypes();
	            			int nargs=m.getParameterTypes().length;

	    					Annotation[][] anns=(Annotation[][])m.getParameterAnnotations();

	            			for (int i=0;i<nargs;i++) {
	            				String pname=((Parameter)anns[i][0]).name();
	            				parameters.add(pname);
	    					}
	    					System.out.println("LocalFunctionalityManager: PARAMETERS obtained "+parameters.size());
	    					for (String p:parameters) {
		    					System.out.println("\t"+p);
	    					}

	    					/// Generate and return full request
	    					return new FunctionalityRequest(url,verb,parameters);
	    				}

	    			}

	    		}

	    	}
    	}
    	System.out.println("ERROR LocalFunctionalityManager: could not get URL for "+functionality+"."+appliance+"."+method);
		return null;

	}


	@Override
	public void addListener(String avatarId, FunctionalityManagerListener listener) {
		if (this.avatarListeners.containsKey(avatarId)) {
			this.avatarListeners.get(avatarId).add(listener);
		} else {
			HashSet<FunctionalityManagerListener> ls = new HashSet<>();
			ls.add(listener);
			this.avatarListeners.put(avatarId, ls);
		}

	}

	@Override
	public void removeListener(String avatarId, FunctionalityManagerListener listener) {
		this.avatarListeners.remove(listener);
	}

	@Override
	public Collection<FunctionalityManagerListener> getListeners(String avatarId) {
		return this.avatarListeners.get(avatarId);
	}

	@Override
	public void addListener(FunctionalityManagerListener listener) {
		this.listeners.add(listener);
	}

	@Override
	public void removeListener(FunctionalityManagerListener listener) {
		this.listeners.remove(listener);
	}

	@Override
	public Collection<FunctionalityManagerListener> getListeners() {
		return this.listeners;
	}

	@Override
	public void addFactoryListener(CollectionListener listener) {
		this.factoryListeners.add(listener);
	}

	@Override
	public void removeFactoryListener(CollectionListener listener) {
		this.factoryListeners.remove(listener);
	}

    /***********************
     * Private methods
     ***********************/


    private FunctionalityBean parseFunctionality(Class<? extends FunctionalityInterface> functionalityClass) {
        Functionality funcAnnotation = (Functionality) functionalityClass.getAnnotation(Functionality.class);

        if (funcAnnotation == null) {
            logger.error("NOT ANNOTATED INTERFACE - "+functionalityClass.getAnnotations().length+" annotations found");
            return null;
        }

        String functionalityName;
        String functionalityDescription;
        String functionalitySemanticType;

        if (funcAnnotation.name() != null && !funcAnnotation.name().isEmpty()) {
            functionalityName = funcAnnotation.name();
        } else {
            functionalityName = functionalityClass.getSimpleName();
        }

        functionalityDescription = funcAnnotation.description();

        functionalitySemanticType = funcAnnotation.semanticType();

		boolean exposable = funcAnnotation.exposable(); // defaults to true

        return new FunctionalityBean(functionalityName,
				functionalityDescription,
				functionalityClass,
				functionalitySemanticType,
				parseMethodRequests(functionalityClass),
				exposable);
    }

    private Map<String, FunctionalityRequest> parseMethodRequests(Class<? extends FunctionalityInterface> functionalityClass) {

        Map<String, FunctionalityRequest> frs = new HashMap<>();

        for (Method m:functionalityClass.getMethods()) {

            // GENERATE VERB
            String verb=null;
            if (m.getAnnotation(GET.class)!=null) verb="GET";
            if (m.getAnnotation(POST.class)!=null) verb="POST";
            if (m.getAnnotation(PUT.class)!=null) verb="PUT";
            if (m.getAnnotation(DELETE.class)!=null) verb="DELETE";
            if (verb==null) {
                // not an exposed method
                continue;
            }

            String url = "/";
            Path ann=m.getAnnotation(Path.class);
            if (ann!=null) {
                url = ann.value();
            }

            // GENERATE PARAMETER LIST
            ArrayList<String> parameters=new ArrayList<String>();

            Class<?> ptypes[]=m.getParameterTypes();
            int nargs=m.getParameterTypes().length;



            Annotation[][] anns=(Annotation[][])m.getParameterAnnotations();

            for (int i=0;i<nargs;i++) {
                String pname=((Parameter)anns[i][0]).name();
                parameters.add(pname);
            }

            // Generate and return full request
            FunctionalityRequest fr = new FunctionalityRequest(url,verb,parameters);
            frs.put(m.getName(), fr);
        }

        return frs;

    }

	/**
	 * Exposes a functionality as a REST service, and tag it as enabled.
	 * Maps Vertx HTTP route to the right functionality provider's instance
	 *
	 */
	private void exposeRestFunctionality(String avatarId, String functionality) {

		// get functionalityProvider

		// parse annotations

	}

}

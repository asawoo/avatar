package asawoo.services.net.dtn;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.vertx.VertxConfiguration;
import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.service.DTNCallbackManager;
import asawoo.services.net.dtn.util.DTNConstants;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.felix.ipojo.annotations.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 07/04/16.
 */
@Component(immediate = true)
@Instantiate
@Provides
public class DTNProxy /*extends AbstractVerticle implements DTNCallback*/ { // TODO remove implement Verticle (deploy/undeploy seems to conflic with instance lifeCycle)

    @Requires
    private DTNAdapter dtnAdapter;

    @Requires
    private DTNCallbackManager cbManager;

    @Requires
    private Vertx vertx;
    @Requires
    private Router router;
    @Requires
    private VertxConfiguration vertxConfiguration;

    private HttpServer httpServer;

    private final Set<Route> routes = new HashSet<>();

    private Logger log = LoggerFactory.getLogger(DTNProxy.class);

//    @Override
    @Validate
    public void start() throws Exception {

//        Router router = Router.router(vertx);
//        router.route().handler(BodyHandler.create());

        routes.add(router.route(DTNConstants.DTN_PROXY_URL_MAPPING)
                .handler(new DTNProxyRequestHandler()));



        /* Callback URI - TODO remove from here or consume adequate handler from cbManager */
        String defaultCallbackUri = DTNConstants.DTN_DEFAULT_CALLBACK_URL_MAPPING + "/:" + DTNConstants.DTN_CALLBACK_REQ_ID_PARAMETER;

        routes.add(router.post(defaultCallbackUri) // default callback URI: /callback/:reqId
                .handler(routingContext -> {
                    log.info("POST request received on DTN Callback " + routingContext.request().uri());
                    String reqId = routingContext.request().getParam(DTNConstants.DTN_CALLBACK_REQ_ID_PARAMETER);
                    Handler<RoutingContext> handler = (reqId != null && cbManager.getRequestHandler(reqId) != null) ?
                            cbManager.consumeRequestHandler(reqId)
                            : rc -> { // default handler
                                      // do nothing ?
                                    }
                    ;

                    handler.handle(routingContext);

                })
        );

        int httpPort = vertxConfiguration.getConfig().getInteger(VertxConfiguration.HTTP_PORT, DTNConstants.DTN_DEFAULT_HTTP_PORT);
//        httpServer = getVertx().createHttpServer()
//                    .requestHandler(router::accept)
//                    .listen(httpPort);

        log.info("DTN Proxy started. Listening HTTP requests on "+DTNConstants.DTN_PROXY_URL_MAPPING
                +" and "+defaultCallbackUri
                +" on port "+httpPort
                +".");

    }

//    @Override
    @Invalidate
    public void stop() throws Exception {
        routes.forEach(r -> r.remove());
        routes.clear();
//        httpServer.close();
        log.info("DTN Proxy has been stopped.");
    }

    private class DTNProxyRequestHandler implements Handler<RoutingContext> {

        @Override
        public void handle(RoutingContext routingContext) {

            String requestBody = routingContext.getBodyAsString();

            HttpServerResponse response = routingContext.response();

            // TODO filter on a specific path (i.e. dtn ?) or only forward

            if (routingContext.request().getParam(DTNConstants.DTN_URI) == null) {
                response.setStatusCode(HttpServletResponse.SC_BAD_REQUEST)
                        .end("Bad Request: No DTN URI has been provided as a request parameter. Without dtn_uri parameter, ASAWoO DTN proxy cannot be used.");
                return;
            }

            // else forward the request
            DTNRequestMetadata metadata = DTNUtils.buildMetadataFromHttpRequest(routingContext.request());

//            switch(routingContext.request().method()) {
//                case GET:
//                    //dtnAdapter.forwardRequest();
//                    break;
//                case POST:
//                    break;
//                default:
//
//            }

            try {
                dtnAdapter.forwardRequest(metadata, routingContext.request().params(), requestBody, response);
                // OK - 200
                response.setStatusCode(HttpServletResponse.SC_OK);
            } catch (DTNUnsupportedComModeException e) {
                // HTTP error code 501 (not implemented)
                response.setStatusCode(HttpServletResponse.SC_NOT_IMPLEMENTED);
            }

        }
    }


}

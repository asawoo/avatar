package asawoo.services.net.dtn.util;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.util.AsawooUtils;
import asawoo.core.util.PlatformUtils;
import asawoo.services.net.dtn.DTNProxy;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriBuilder;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Utility class
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 11/03/16.
 */
public class DTNUtils {

/*
    public static DTNRequestMetadata buildMetadataFromHttpRequest(HttpServletRequest req) {
        URI dtnUri = getDtnURIFromRequest(req); // TODO !! can be null
        SchemeBean scheme = null;
        try {
            scheme = parseScheme(dtnUri.getScheme());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        DTNRequestMetadata metadata = new DTNRequestMetadata(dtnUri, scheme, req.getMethod());

        // extract parameters
        Map<String, String[]> params = req.getParameterMap();
        Set<String> paramKeys = params.keySet();
        for (String key : paramKeys) {
            switch (key) {
                case DTNConstants.PROPERTY_TIME_CREATION:
                    metadata.setCreationTime(Long.parseLong(params.remove(key)[0]));
                    break;
                case DTNConstants.PROPERTY_TIME_EXPIRATION:
                    metadata.setExpirationTime(Integer.parseInt(params.remove(key)[0]));
                    break;
                case DTNConstants.PROPERTY_TIME_SYMBOLICAL:
                    metadata.setSymbolicalTime(params.remove(key)[0]);
                    break;
                case DTNConstants.PROPERTY_NB_HOPS:
                    metadata.setHopsNb(Integer.parseInt(params.remove(key)[0]));
                    break;
                case DTNConstants.PROPERTY_SPATIAL_GPS_POINT:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_AREA:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_ADDRESS:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_SYMBOLICAL:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_CACHEABLE:
                    metadata.setCacheable(Boolean.parseBoolean(params.remove(key)[0]));
                    break;
                case DTNConstants.PROPERTY_CALLBACK:
                    metadata.setCallback(URI.create(params.remove(key)[0]));
                    break;
                default:
                    // let the parameters in the map
            }
        }

        return metadata;

    }
*/

    public static DTNRequestMetadata buildMetadataFromHttpRequest(HttpServerRequest req) {

        URI dtnUri = getDtnURIFromRequest(req); // TODO !! can be null
        SchemeBean scheme = null;
        try {
            scheme = DTNUtils.parseScheme(dtnUri.getScheme());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        DTNRequestMetadata metadata = new DTNRequestMetadata(dtnUri, scheme, req.method().name());

        // extract parameters
        MultiMap params = req.params();
        Set<String> paramKeys = params.names();
        for (String key : paramKeys) {
            switch (key) {
                case DTNConstants.PROPERTY_TIME_CREATION:
                    metadata.setCreationTime(Long.parseLong(params.get(key)));
                    params.remove(key);
                    break;
                case DTNConstants.PROPERTY_TIME_EXPIRATION:
                    metadata.setExpirationTime(Integer.parseInt(params.get(key)));
                    params.remove(key);
                    break;
                case DTNConstants.PROPERTY_TIME_SYMBOLICAL:
                    metadata.setSymbolicalTime(params.get(key));
                    params.remove(key);
                    break;
                case DTNConstants.PROPERTY_NB_HOPS:
                    metadata.setHopsNb(Integer.parseInt(params.get(key)));
                    params.remove(key);
                    break;
                case DTNConstants.PROPERTY_SPATIAL_GPS_POINT:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_AREA:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_ADDRESS:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_SPATIAL_SYMBOLICAL:
                    // TODO
                    break;
                case DTNConstants.PROPERTY_CACHEABLE:
                    metadata.setCacheable(Boolean.parseBoolean(params.get(key)));
                    params.remove(key);
                    break;
                case DTNConstants.PROPERTY_CALLBACK:
                    metadata.setCallback(URI.create(params.get(key)));
                    params.remove(key);
                    break;
                default:
                    // let the parameters in the map
            }
        }

        return metadata;

    }

    public static CommunicationMode getCommunicationModeFromMetadata(DTNRequestMetadata metadata) {
        return metadata.getScheme().getCommunicationMode();
    }

    /**
     * TODO Care: last path segment may not be the functionality name
     * @param metadata
     * @return
     */
    public static String getFunctionalityFromMetadata(DTNRequestMetadata metadata) {
        URI uri = metadata.getDtnUri();
        String path = uri.getPath();
        String functionalityName = path.substring(path.lastIndexOf('/') + 1);
        return functionalityName;
    }


    public static boolean requiresDtn(DTNRequestMetadata metadata) {
        return metadata.getScheme().isDtn();
    }

    public static <T extends Serializable> byte[] serialize(T object) throws IOException {
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream()){
            try(ObjectOutputStream oos = new ObjectOutputStream(bos)){
                oos.writeObject(object);
            }
            return bos.toByteArray();
        }
    }

    public static <T extends Serializable> T deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream bis = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream ois = new ObjectInputStream(bis)){
                Object o = (T) ois.readObject();
//                if (o instanceof T) {
                return (T) o;
//                } else {
//                    return null;
//                }
            }
        }
    }

    public static Date getExpirationDate(DTNRequestMetadata metadata) {
        if (getExpirationTimestamp(metadata) != null) {
            return new Date(getExpirationTimestamp(metadata));
        } else {
            return null;
        }
    }

    public static Long getExpirationTimestamp(DTNRequestMetadata metadata) {
        if (metadata.getCreationTime() != null && metadata.getExpirationTime() != null) {
            Long expirationTimestamp = metadata.getCreationTime() + metadata.getExpirationTime();
            return expirationTimestamp;
        } else {
            return null;
        }
    }

    /**
     *
     * @param metadata
     * @return a number of milliseconds TTL
     */
    public static Long getTimeout(DTNRequestMetadata metadata) {
        if (getExpirationTimestamp(metadata) != null) {
            return getExpirationTimestamp(metadata) - System.currentTimeMillis();
        } else {
            return null; // TODO -1 ?
        }
    }

/*
    private static URI getDtnURIFromRequest(HttpServletRequest req) {
        URI dtnURI = null;
        String dtnURIStr = req.getParameter(DTNConstants.DTN_URI);
        if (dtnURIStr != null) {
            System.out.println("DTN URI: " + dtnURIStr);
            // substitute + sign with %2B URL encoding
//            dtnURIStr = dtnURIStr.replace("+", "%2B");
            dtnURI = URI.create(dtnURIStr);
        }
        return dtnURI;
    }
*/

    private static URI getDtnURIFromRequest(HttpServerRequest req) {
        URI dtnURI = null;
        String dtnURIStr = req.getParam(DTNConstants.DTN_URI);
        if (dtnURIStr != null) {
            System.out.println("DTN URI: " + dtnURIStr);
            // substitute + sign with %2B URL encoding
//            dtnURIStr = dtnURIStr.replace("+", "%2B");
            dtnURIStr = dtnURIStr.replace(" ", "+");
            dtnURI = URI.create(dtnURIStr);
            System.out.println("----- SCHEME : " + dtnURI.getScheme());
            System.out.println("----- PATH : " + dtnURI.getPath());
//            try {
//                String encodedURI = URLEncoder.encode(dtnURIStr, "UTF-8");
//                System.out.println("DTN encoded URI : "+encodedURI);
//                dtnURI = URI.create(encodedURI);
//                System.out.println("----- SCHEME : " + dtnURI.getScheme());
//                System.out.println("----- PATH : " + dtnURI.getPath());
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//            dtnURI = UriBuilder.fromUri(dtnURIStr).build(); //URI.create(dtnURIStr);
        }

        return dtnURI;
    }

    private static SchemeBean parseScheme(String uriScheme) throws MalformedURLException {
        if (uriScheme == null) {
            // private method, should not happen
            throw new IllegalArgumentException("uriScheme to parse cannot be null");
        }

        String[] scheme = uriScheme.split(DTNConstants.SCHEME_SEPARATOR);
        String transport = scheme[0]; // TODO check if HTTP or CoAP and throw malformed URL otherwise
        CommunicationMode comMode = null;
        boolean isDTN = false;
        for (int i = 1; i < scheme.length; i++) {
            if (scheme[i].equals(DTNConstants.SCHEME_DTN)) {
                isDTN = true;
            } else if (CommunicationMode.findByCode(scheme[i]) != null) {
                comMode = CommunicationMode.findByCode(scheme[i]);
            } else {
                throw new MalformedURLException("Unknown scheme compound: "+scheme[i]);
            }
        }

        return new SchemeBean(transport, comMode, isDTN);

    }

    public static String getHost(DTNRequestMetadata metadata) {
        return metadata.getDtnUri().getHost();
    }

    public static Map<String, String[]> multiMapToSimpleMap(MultiMap map) {
        Map<String, String[]> res = new HashMap<>();
        if (map != null && !map.isEmpty()) {
            for (String key : map.names()) {
                List<String> values = map.getAll(key);
                res.put(key, values.toArray(new String[values.size()]));
            }
        }
        return res;
    }

    /**
     * Transform base URI to DTNBase URI
     * @param entry
     * @param communicationMode
     * @param nodeId
     * @return
     */
    public static String getDtnUriFromRegistryEntry(RegistryEntryBean entry, CommunicationMode communicationMode, String nodeId) {

        // base URI

        URI straightUri = URI.create(entry.getBaseUri());

        // build DTN uri parameter
        URI dtnUri = null;
        SchemeBean scheme = new SchemeBean(straightUri.getScheme(), communicationMode, true);
        // HOST : address the avatarID (= remote DTN nodeId) or the Functionality topic
        String host = (communicationMode == null || communicationMode == CommunicationMode.UNICAST) ? nodeId : entry.getInterfaceName();
        try {
            dtnUri = new URI(scheme.toString(),
                    straightUri.getUserInfo(),
                    host, straightUri.getPort(),
                    straightUri.getPath(),
                    straightUri.getQuery(),
                    straightUri.getFragment());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        // append to DTN proxy
        return DTNConstants.DTN_PROXY_URL_MAPPING
                +"?"+DTNConstants.DTN_URI
                +"="+dtnUri.toString();

    }

    public static URI getDtnUriFromRegistryEntry(RegistryEntryBean entry, String methodName, CommunicationMode communicationMode, String nodeId) {

        // find straight URI
        FunctionalityRequest fr = entry.getMethods().get(methodName);
        URI straightUri;
        URI dtnUri = null;
        if (fr != null) {
            straightUri = URI.create(AsawooUtils.getFullURI(entry,fr.url));
        } else {
            // TODO throw exception
            return null;
        }
        SchemeBean scheme = new SchemeBean(straightUri.getScheme(), communicationMode, true);
        // HOST : address the avatarID (= remote DTN nodeId) or the Functionality topic
        String host = (communicationMode == null || communicationMode == CommunicationMode.UNICAST) ? nodeId : entry.getInterfaceName();
        try {
            dtnUri = new URI(scheme.toString(),
                            straightUri.getUserInfo(),
                            host, straightUri.getPort(),
                            straightUri.getPath(),
                            straightUri.getQuery(),
                            straightUri.getFragment());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return dtnUri;

    }

    public static URI proxifyDtnURI(URI dtnUri, int dtnProxyPort) {
        String proxifiedUri = "http://localhost:"+dtnProxyPort
                            +DTNConstants.DTN_PROXY_URL_MAPPING
                            +"?"+DTNConstants.DTN_URI
                            +"="+dtnUri.toString();
        return URI.create(proxifiedUri);
    }
    
    /**
     * extract uri host part from string like /dtn?dtn_uri=http+dtn://host/avatar/functionality/
     * 
     * @param dtnUri
     * @return 
     */
    public static String getDTNHostFromDTNUri(String dtnUri) {
        
        String uriPrefix = new String(DTNConstants.DTN_PROXY_URL_MAPPING+"?"+DTNConstants.DTN_URI+"=");
        
        if (!dtnUri.startsWith(uriPrefix)) {
            throw new IllegalArgumentException("Wrong usage. Given URI must start with "+uriPrefix);
        }
        
        // parse
        int beginIndex = uriPrefix.length();
        String uriPart = dtnUri.substring(beginIndex);
        URI uri = URI.create(uriPart);
        return uri.getHost();
    }

}

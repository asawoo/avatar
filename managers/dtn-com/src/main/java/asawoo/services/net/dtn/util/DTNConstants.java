package asawoo.services.net.dtn.util;
/*
* Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
*
* This file is part of the ASAWoO project.
*
* ASAWoO is free software; you can redistribute it and/or modify it under the
* terms of the CeCILL License. See CeCILL License for more details.
*
* ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY.
*/

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 08/02/16.
 */
public interface DTNConstants {

    String DTN_URI = "dtn_uri";
    String DTN_PROXY_URL_MAPPING = "/dtn";
    String DTN_DEFAULT_CALLBACK_URL_MAPPING = "/callback";
    String DTN_CALLBACK_REQ_ID_PARAMETER = "reqId";

    int DTN_DEFAULT_HTTP_PORT = 9090;

    /* SCHEME */
    String SCHEME_SEPARATOR = "\\+";
    String SCHEME_DTN = "dtn";
//    String SCHEME_ANYCAST = "acast";
//    String SCHEME_BROADCAST = "bcast";
//    String SCHEME_MULTICAST = "mcast";

    /* TIME PROPERTIES */
    String PROPERTY_TIME_CREATION = "dtn_ctime";
    String PROPERTY_TIME_EXPIRATION = "dtn_etime";
    String PROPERTY_TIME_SYMBOLICAL = "dtn_stime";

    String PROPERTY_NB_HOPS = "dtn_hops";

    /* SPATIAL PROPERTIES */
    String PROPERTY_SPATIAL_GPS_POINT = "dtn_src_pos";
    String PROPERTY_SPATIAL_AREA = "dtn_area";
    String PROPERTY_SPATIAL_SYMBOLICAL = "dtn_slocation";
    String PROPERTY_SPATIAL_ADDRESS = "dtn_address";

    String PROPERTY_CACHEABLE = "dtn_cacheable";
    String PROPERTY_CALLBACK = "callback";

    // DTN adapter OSGi registration property
    String REGISTRATION_PROPERTY_SUPPORTED_COM_MODE = "communication.modes.supported";

;
}

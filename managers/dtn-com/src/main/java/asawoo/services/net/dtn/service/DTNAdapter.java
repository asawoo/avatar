package asawoo.services.net.dtn.service;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.EnumSet;
import java.util.Map;

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 08/02/16.
 * To implement for each DTN library, in a separate bundle (use Whiteboard pattern)
 */
public interface DTNAdapter {

    String getDTNFrameworkName();

    void setNodeId(String nodeId);

    String getNodeId();

    void addNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener);

    void removeNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener);

    EnumSet<CommunicationMode> getSupportedCommunicationModes();

//    void forwardRequest(URI dtnURI, String httpMethod/*, boolean enableDTN*/, HttpServletResponse resp);

    /**
     * Forwards an HTTP request to the DTN transport middleware. Endpoint (URI) is included in the metadata
     * @param metadata Includes target URI, HTTP method and other properties that can be used by the DTN transport layer
     * @param requestParams HTTP request parameters map
     * <!-- @param request HTTP request that will be forwarded -->
     * @param response
     */
    void forwardRequest(DTNRequestMetadata metadata, Map<String, String[]> requestParams,/*HttpRequestBase request,*/ HttpServletResponse response)
                        throws DTNUnsupportedComModeException;

    /**
     * Forwards an HTTP request to the DTN transport middleware. Endpoint (URI) is included in the metadata.
     * Returns the DTN message identifier
     * @param metadata
     * @param queryParams
     * @param requestBody
     * @param response
     * @return DTN message IDentifier
     * @throws DTNUnsupportedComModeException
     */
    String forwardRequest(DTNRequestMetadata metadata, MultiMap queryParams, String requestBody, HttpServerResponse response)
            throws DTNUnsupportedComModeException;

//    void forwardUnicastRequest(DTNRequestMetadata metadata, String httpMethod, HttpRequestBase request, HttpServletResponse response);
//    void forwardBroadcastRequest(DTNRequestMetadata metadata, String httpMethod, HttpRequestBase request, HttpServletResponse response);
//    void forwardMulticastRequest(DTNRequestMetadata metadata, String httpMethod, HttpRequestBase request, HttpServletResponse response);
//    void forwardAnycastRequest(DTNRequestMetadata metadata, String httpMethod, HttpRequestBase request, HttpServletResponse response);



    void registerMulticastSubscriber(String topic);

    void unregisterMulticastSubscriber(String topic);

    /**
     * Removes a message from local cache and prevents its propagation
     */
    void purgeMessage(String messageId);

    /**
     *
     * @return platform host (IP or hostname)
     */
    String getHost();

}

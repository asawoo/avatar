package asawoo.services.net.dtn.util.http;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.net.dtn.util.SchemeBean;

import java.io.Serializable;
import java.net.URI;

/**
 * This class regroups all DTN-related request metadata.
 * It is meant to be used by DTN adaptation.
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 11/03/16.
 */
public class DTNRequestMetadata implements Serializable {



    private URI dtnUri;

    private SchemeBean scheme;

    private String httpMethod;

    private Boolean cacheable;
    private URI callback;

    /* Time properties */
    private Long creationTime;
    private Integer expirationTime;
    private String symbolicalTime;

    private Integer hopsNb;

    /* Spatial properties */

    public DTNRequestMetadata() {
        
    }

    public DTNRequestMetadata(URI dtnUri, SchemeBean scheme, String httpMethod) {
        this.dtnUri = dtnUri;
        this.scheme = scheme;
        this.httpMethod = httpMethod;
    }

    // TODO add constructors ?

    public URI getDtnUri() {
        return dtnUri;
    }

    public void setDtnUri(URI dtnUri) {
        this.dtnUri = dtnUri;
    }

    public SchemeBean getScheme() {
        return scheme;
    }

    public void setScheme(SchemeBean scheme) {
        this.scheme = scheme;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Integer expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getSymbolicalTime() {
        return symbolicalTime;
    }

    public void setSymbolicalTime(String symbolicalTime) {
        this.symbolicalTime = symbolicalTime;
    }

    public Integer getHopsNb() {
        return hopsNb;
    }

    public void setHopsNb(Integer hopsNb) {
        this.hopsNb = hopsNb;
    }

    public Boolean getCacheable() {
        return cacheable;
    }

    public void setCacheable(Boolean cacheable) {
        this.cacheable = cacheable;
    }

    public URI getCallback() {
        return callback;
    }

    public void setCallback(URI callback) {
        this.callback = callback;
    }
}

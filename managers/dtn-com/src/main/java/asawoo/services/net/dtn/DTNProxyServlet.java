package asawoo.services.net.dtn;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.util.DTNConstants;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import org.apache.felix.ipojo.annotations.*;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.osgi.service.log.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 15/02/16.
 */
//@Component(immediate=true)//(name = "dtnproxy")
//@Instantiate
public class DTNProxyServlet extends HttpServlet {

    @Requires
    private DTNAdapter dtnAdapter;

    @Requires(optional = true)
    LogService logService;

    @Bind
    private synchronized void bindHttpService(HttpService httpService) {
        // registers servlet
        try {
            httpService.registerServlet(DTNConstants.DTN_PROXY_URL_MAPPING, this, null, null);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (NamespaceException e) {
            e.printStackTrace();
        }
    }

    @Unbind
    private synchronized void unbindHttpService(HttpService httpService) {
        // unregister servlet
        httpService.unregister(DTNConstants.DTN_PROXY_URL_MAPPING);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        DTNRequestMetadata metadata = DTNUtils.buildMetadataFromHttpRequest(req);
//        URI dtnURI = metadata.getDtnUri();
//        logService.log(LogService.LOG_DEBUG, "[DTN proxy] Received GET request for " + dtnURI.toString());
//
//        forwardRequest(metadata, req.getParameterMap(), resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        DTNRequestMetadata metadata = DTNUtils.buildMetadataFromHttpRequest(req);
//        URI dtnURI = metadata.getDtnUri();
//        logService.log(LogService.LOG_DEBUG, "[DTN proxy] Received POST request for " + dtnURI.toString());
//
//        forwardRequest(metadata, req.getParameterMap(), resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        DTNRequestMetadata metadata = DTNUtils.buildMetadataFromHttpRequest(req);
//        URI dtnURI = metadata.getDtnUri();
//        logService.log(LogService.LOG_DEBUG, "[DTN proxy] Received PUT request for " + dtnURI.toString());
//        forwardRequest(metadata, req.getParameterMap(), resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        DTNRequestMetadata metadata = DTNUtils.buildMetadataFromHttpRequest(req);
//        URI dtnURI = metadata.getDtnUri();
//        logService.log(LogService.LOG_DEBUG, "[DTN proxy] Received DELETE request for " + dtnURI.toString());
//        forwardRequest(metadata, req.getParameterMap(), resp);
    }


    private void forwardRequest(DTNRequestMetadata metadata, Map<String, String[]> requestParams, HttpServletResponse response) throws ServletException, IOException {
        try {
            dtnAdapter.forwardRequest(metadata, requestParams, response);
            // OK - 200
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (DTNUnsupportedComModeException e) {
            // HTTP error code 501 (not implemented)
            response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }

}

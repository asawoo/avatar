package asawoo.services.net.dtn.util;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 09/02/16.
 */
public enum DTNTimeProperty {

    PROPERTY_TIME_CREATION("dtn_ctime"),
    PROPERTY_TIME_EXPIRATION("dtn_etime"),
    PROPERTY_TIME_SYMBOLICAL("dtn_stime");

    private final String code;

    // Reverse-lookup map
    private static final Map<String, DTNTimeProperty> lookup = new HashMap<String, DTNTimeProperty>();

    static {
        for (DTNTimeProperty p : DTNTimeProperty.values()) {
            lookup.put(p.getCode(), p);
        }
    }

    private DTNTimeProperty(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    private static DTNTimeProperty findByCode(String code) {
        return lookup.get(code);
    }

}

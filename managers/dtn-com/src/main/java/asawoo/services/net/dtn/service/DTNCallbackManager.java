package asawoo.services.net.dtn.service;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 12/04/16.
 */
public interface DTNCallbackManager {

    void addRequestHandler(String requestId, Handler<RoutingContext> requestHandler);

    Handler<RoutingContext> getRequestHandler(String requestId);

    Handler<RoutingContext> consumeRequestHandler(String requestId);

}

package asawoo.services.net.dtn.util.http;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */


import java.io.Serializable;
import java.net.URI;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 21/03/16.
 */
public class HttpRequestWrapper implements Serializable {

    private DTNRequestMetadata metadata;

    private Map<String, String[]> requestParameters;

    private String requestBody;

    public HttpRequestWrapper(DTNRequestMetadata metadata, Map<String, String[]> requestParameters, String requestBody) {
        this.metadata = metadata;
        this.requestParameters = requestParameters;
        this.requestBody = requestBody;
    }

    public DTNRequestMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(DTNRequestMetadata metadata) {
        this.metadata = metadata;
    }

    public URI getURI() {
        return metadata.getDtnUri();
    }

    public Map<String, String[]> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(Map<String, String[]> requestParameters) {
        this.requestParameters = requestParameters;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}

package asawoo.services.net.dtn.util;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import java.io.Serializable;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 09/02/16.
 */
public class SchemeBean implements Serializable {

    private String transportProtocol;
    //Default {@Link CommunicationMode} is CommunicationMode.UNICAST
    private CommunicationMode communicationMode = CommunicationMode.UNICAST;

    private boolean dtn;

    public SchemeBean() {

    }

    public SchemeBean(String transportProtocol, CommunicationMode communicationMode, boolean dtn) {
        this.transportProtocol = (transportProtocol == null) ? "http" : transportProtocol;
        this.communicationMode = (communicationMode == null) ? this.communicationMode : communicationMode;
        this.dtn = dtn;
    }

    public String getTransportProtocol() {
        return transportProtocol;
    }

    public void setTransportProtocol(String transportProtocol) {
        this.transportProtocol = transportProtocol;
    }

    public CommunicationMode getCommunicationMode() {
        return communicationMode;
    }

    public void setCommunicationMode(CommunicationMode communicationMode) {
        this.communicationMode = communicationMode;
    }

    public boolean isDtn() {
        return dtn;
    }

    public void setDtn(boolean dtn) {
        this.dtn = dtn;
    }

    @Override
    public String toString() {
//        if (isDtn()) {
//
//        }
        String comModePart = communicationMode != CommunicationMode.UNICAST ? "+"+communicationMode.getCode() : "";
        return transportProtocol+comModePart;
    }
}

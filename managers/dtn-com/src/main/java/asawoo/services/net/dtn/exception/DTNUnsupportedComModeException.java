package asawoo.services.net.dtn.exception;
/*
* Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
*
* This file is part of the ASAWoO project.
*
* ASAWoO is free software; you can redistribute it and/or modify it under the
* terms of the CeCILL License. See CeCILL License for more details.
*
* ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY.
*/

/**
 * @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 12/03/16.
 */
public class DTNUnsupportedComModeException extends Exception {

    public DTNUnsupportedComModeException(String message) {
        super(message);
    }

    public DTNUnsupportedComModeException(Throwable cause) {
        super(cause);
    }

    public DTNUnsupportedComModeException(String message, Throwable cause) {
        super(message, cause);
    }

}

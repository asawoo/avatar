package asawoo.services.net.dtn;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.net.dtn.util.DTNConstants;
import io.vertx.core.*;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.annotations.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 15/04/16.
 */
//@Component(immediate = true)
//@Instantiate
public class VerticleDeploymentManager {

    private Logger log = LoggerFactory.getLogger(VerticleDeploymentManager.class);

    @Requires
    private Vertx vertx;

//    private Set<Verticle> hostedVerticles;
    private Map<String, Verticle> hostedVerticles;

    private VerticleDeploymentManager() {
        this.hostedVerticles = new HashMap<>(); //HashSet<>();
    }

    @Bind(optional = true, from = "DTNProxy")
    public void bindVerticle(Verticle verticle) {

        vertx.getOrCreateContext().config().put("http.port", DTNConstants.DTN_DEFAULT_HTTP_PORT);

//        DeploymentOptions options = new DeploymentOptions()
//                .setConfig(new JsonObject().put("http.port", DTNConstants.DTN_DEFAULT_HTTP_PORT)
//                );
//        Handler<AsyncResult<String>> h = asyncResult -> {
//            if (asyncResult.succeeded()) {
//                hostedVerticles.put(asyncResult.result(), verticle);
//                log.info("Verticle : " + verticle.getClass().getName()+" deployed");
//            }
//        };
//        vertx.deployVerticle(verticle, options, h);

//        DeploymentOptions options = null;
//        if (verticle instanceof DTNProxy) {
//            options = new DeploymentOptions()
//                    .setConfig(new JsonObject().put("http.port", DTNConstants.DTN_DEFAULT_HTTP_PORT)
//                    );
//            Handler<AsyncResult<String>> h = asyncResult -> {
//                if (asyncResult.succeeded()) {
//                    hostedVerticles.put(asyncResult.result(), verticle);
//                }
//            };
//            vertx.deployVerticle(verticle, options, h);
//        }
//        log.info("Deploying verticle : " + verticle.getClass().getName());

    }

//    @Unbind(aggregate = true, optional = true)
//    public void unbindVerticle(Verticle verticle) {
//        String deploymentId = verticle.getVertx().getOrCreateContext().deploymentID();
//        log.info("Undeploying verticle : " + verticle.getClass().getName());
////        vertx.undeploy(deploymentId);
//        hostedVerticles.remove(deploymentId);
//        /* TODO fix NPE here */
////        /*Vertx.vertx()*/vertx.undeploy(verticle);
////        hostedVerticles.remove(verticle);
//    }

    @Validate
    public void start() {
        // load logging properties file
        // 1. get resource from bundle
    }

    @Invalidate
    public void stop() {
        // undeploy remaining Verticles
        for (String deploymentId : hostedVerticles.keySet()) {
            /* TODO fix NPE here */
//            Vertx.vertx().undeploy(deploymentId);
        }
    }


}

package asawoo.services.net.dtn.service.impl;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.net.dtn.service.DTNCallbackManager;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 11/04/16.
 */
@Component
@Instantiate
@Provides
public class DTNCallbackManagerImpl implements DTNCallbackManager {

    /* Map <requestId, RequestHandler> */
    private Map<String, Handler<RoutingContext>> requestHandlers;

    public DTNCallbackManagerImpl() {
        requestHandlers = new HashMap<>();
    }

    @Override
    public void addRequestHandler(String requestId, Handler<RoutingContext> rcHandler) {
//        if (requestHandlers == null) {
//            requestHandlers = new HashMap<>();
//        }
        if (rcHandler != null)
            requestHandlers.put(requestId, rcHandler);
    }

    @Override
    public Handler<RoutingContext> getRequestHandler(String requestId) {
        return (requestHandlers.containsKey(requestId)) ? requestHandlers.get(requestId)
                : rc -> { /* do nothing */ }; // or null ?
    }

    @Override
    public Handler<RoutingContext> consumeRequestHandler(String requestId) {
        Handler<RoutingContext> h = getRequestHandler(requestId);
        requestHandlers.remove(requestId);
        return h;
    }
}

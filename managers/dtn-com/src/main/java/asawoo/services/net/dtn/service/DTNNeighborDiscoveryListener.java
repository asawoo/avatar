package asawoo.services.net.dtn.service;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

/**
 *  @author Lionel Touseau <lionel.touseau at univ-ubs.fr> on 31/10/16.
 *
 *  DTN neighbor discovery listeners are notified when a neighbor comes into or out of direct communication range (i.e., 1 hop)
 *
 */
public interface DTNNeighborDiscoveryListener {

    void onNeighborContact(String nodeId);

    void onNeighborLoss(String nodeId);

}

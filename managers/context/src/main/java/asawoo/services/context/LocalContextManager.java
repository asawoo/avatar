package asawoo.services.context;

import java.util.List;
import java.util.Map;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Context;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Validate;
import org.osgi.framework.BundleContext;

import asawoo.core.capability.CapabilityManager;
import asawoo.core.config.SemanticReasonerConfiguration;
import asawoo.core.context.ContextManager;
import asawoo.core.functionality.FunctionalityManager;
import asawoo.core.reasoner.SemanticReasoner;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

@Component(name="context-manager")
@Provides
@Instantiate
public class LocalContextManager implements ContextManager {
	
	@Context
	private BundleContext bundleContext;

	@Requires
	private FunctionalityManager functionalityManager;
	
	@Requires
	private SemanticReasoner reasoner;

	@Requires
	private CapabilityManager capabilityManager;
	
	private Logger logger = LoggerFactory.getLogger(LocalContextManager.class);

	public LocalContextManager() {}
	
	@Validate
	public void start() throws InterruptedException {
		/*logger.info("Starts the context manager");
		String uri = "http://test.com/";
		
		this.reasoner.initReasonerForAvatar(uri);		
		
		this.processRawData("alert", "http://liris.cnrs.fr/asawoo/functionalities#PatrolSurveillance", uri);		
		logger.info("doit être BASE");
		this.getPurposeBasedAnswer(SemanticReasonerConfiguration.PRIORITY_PURPOSE, "http://liris.cnrs.fr/asawoo/functionalities#Navigation", uri);
		
		this.processRawData("ok", "http://liris.cnrs.fr/asawoo/functionalities#PatrolSurveillance", uri);		
		logger.info("doit PAS ETRE BASE");
		this.getPurposeBasedAnswer(SemanticReasonerConfiguration.PRIORITY_PURPOSE, "http://liris.cnrs.fr/asawoo/functionalities#Navigation", uri);
		this.processRawData(60, SemanticReasonerConfiguration.TEMPERATURE, uri);		
		this.processRawData(2.9, SemanticReasonerConfiguration.GAS, uri);
		logger.info("doit être BASE");
		this.getPurposeBasedAnswer(SemanticReasonerConfiguration.PRIORITY_PURPOSE, "http://liris.cnrs.fr/asawoo/functionalities#Navigation", uri);
		this.processRawData(30, SemanticReasonerConfiguration.TEMPERATURE, uri);
		logger.info("doit être BASE");
		this.getPurposeBasedAnswer(SemanticReasonerConfiguration.PRIORITY_PURPOSE, "http://liris.cnrs.fr/asawoo/functionalities#Navigation", uri);
		this.processRawData(0, SemanticReasonerConfiguration.GAS, uri);
		logger.info("doit PAS ETRE BASE");
		this.getPurposeBasedAnswer(SemanticReasonerConfiguration.PRIORITY_PURPOSE, "http://liris.cnrs.fr/asawoo/functionalities#Navigation", uri);*/
		
	}

	@Override
	public void processRawData(Object rawData, String sourceTypeURI, String avatarURI) {
		//rawData = rawData.toString();
		String deleteQuery = SemanticReasonerConfiguration.PREFIXES
				+ "DELETE {\n"
				+ " 	<" + sourceTypeURI + "> rdf:value ?v .\n"
				+ "	} WHERE {\n"
				+ "		<" + sourceTypeURI + "> rdf:value ?v .\n"
				+ " }\n";		
		this.reasoner.launchUpdateQuery(deleteQuery, avatarURI);
		
		if (rawData.getClass().equals(String.class)) {
			rawData = "\"" + rawData + "\"";
		}
		
		String insertQuery = SemanticReasonerConfiguration.PREFIXES
				+ "INSERT DATA { <" + sourceTypeURI + "> rdf:value " + rawData + " . } ";
		// logger.info("Insert query into LocalContextManager avatar graph (" + avatarURI + ") : " + insertQuery);
		this.reasoner.launchUpdateQuery(insertQuery, avatarURI);
	}
	
	@Override
	public void updateContextWithInstance(String contextualInstance, String avatarURI) {
		String deleteQuery = SemanticReasonerConfiguration.PREFIXES
				+ "DELETE {\n"
				+ " 	?i <" + SemanticReasonerConfiguration.EFFECTIVE_INSTANCE_PROP + "> xsd:true .\n"
				+ "	} WHERE {\n"
				+ "		?i asawoo-ctx:instanceFromDimension ?d ;\n"
				+ "		  	asawoo-ctx:instanceForPurpose ?p .\n"
				+ " }\n";		
		this.reasoner.launchUpdateQuery(deleteQuery, avatarURI);		
		
		String insertQuery = SemanticReasonerConfiguration.PREFIXES
				+ "INSERT DATA { <" + contextualInstance + "> <" + SemanticReasonerConfiguration.EFFECTIVE_INSTANCE_PROP + "> xsd:true . } ";
		this.reasoner.launchUpdateQuery(insertQuery, avatarURI);
	}

	/**
	 * Answers a purpose-based adaptation question (specifying a strict score = 1)
	 */
	@Override
	public List<String> getPurposeBasedAnswer(String adaptationPurpose, String adaptedFunctionality, String avatarURI, Boolean... strictScore) {
		String closingBracket = " }";
		if (strictScore.length > 0 && strictScore[0].equals(true)) {
			closingBracket = " FILTER(xsd:float(?score) >= 1) }";
		}
		
		String query = SemanticReasonerConfiguration.PREFIXES
				+ "SELECT DISTINCT ?candidate {"
				+ "	[]	rdf:subject <" + adaptedFunctionality + "> ;\n"
				+ "		rdf:predicate <" + adaptationPurpose + "> ;\n"
				+ "		rdf:object ?candidate ;\n"
				+ "		rdf:value ?score ." + closingBracket + " ORDER BY DESC(xsd:float(?score))";
		List<String> res = this.reasoner.launchContextSelectQuery(query, avatarURI);
		return res;
	}

	@Override
	public Map<?, ?> getContext(String avatarURI) {
		String contextQuery = SemanticReasonerConfiguration.PREFIXES
			+ "CONSTRUCT WHERE { "
			+ "?i <" + SemanticReasonerConfiguration.EFFECTIVE_INSTANCE_PROP + "> xsd:true . "
			+ "} ";
		return this.reasoner.launchSelectQuery(contextQuery, avatarURI);
	}
	
	@Override
	@Deprecated
	public void getContext() {		
		// Now requires the avatar context URI
	}
}

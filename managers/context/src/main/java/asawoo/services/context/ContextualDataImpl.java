package asawoo.services.context;

import asawoo.core.context.ContextualData;

public class ContextualDataImpl implements ContextualData {
	String sourceURI;
	String sourceTypeURI;	
	String value;
	
	public ContextualDataImpl(String source, String type, String val) {
		this.sourceURI = source;
		this.sourceTypeURI = type;
		this.value = val;
	}
	
	@Override
	public String getSourceURI() {
		return sourceURI;
	}
	
	@Override
	public String getSourceTypeURI() {		
		return sourceTypeURI;
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
}

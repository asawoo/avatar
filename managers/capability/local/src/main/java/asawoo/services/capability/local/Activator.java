/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.capability.local;

import java.util.Dictionary;
import java.util.Hashtable;

import asawoo.core.functionality.FunctionalityManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;

public class Activator implements BundleActivator{
	
    private ServiceRegistration serviceRegistration;

    private ServiceTracker functionalityTracker;
	
    @Override
    public void start(BundleContext context) throws Exception {

        // open the service tracker
        functionalityTracker = new ServiceTracker(context, FunctionalityManager.class.getName(), null);
        functionalityTracker.open();

        // TODO pass the service object to the capabilityManager
        LocalCapabilityManager capabilityManager = new LocalCapabilityManager(functionalityTracker);
		Dictionary<String, String> props = new Hashtable<>();
	 
	     /*   props.put("service.exported.interfaces", "*");
	        props.put("service.exported.configs", "org.apache.cxf.ws");
	        props.put("org.apache.cxf.ws.address", "http://localhost:9090/capability-manager");*/
	
		this.serviceRegistration = context.registerService(asawoo.core.capability.CapabilityManager.class.getName(), capabilityManager, props);
			
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    	this.serviceRegistration.unregister();

        // close service tracker
        functionalityTracker.close();
    }

}

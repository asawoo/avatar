/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.capability.local;

import asawoo.core.annotation.Capability;
import asawoo.core.annotation.CapabilityProvider;
import asawoo.core.capability.CapabilityBean;
import asawoo.core.capability.CapabilityManager;
import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityManager;
import asawoo.core.util.AsawooUtils;
import asawoo.core.util.CollectionListener;
import asawoo.core.util.Table2;
import org.osgi.util.tracker.ServiceTracker;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * 
 * @author lesommer
 * @author lionel touseau <lionel.touseau@univ-ubs.fr>
 * @version 1.0
 */
public class LocalCapabilityManager implements CapabilityManager {

    /*
    * TODO get a reference of functionality manager service interface (through OSGi service layer, injection or as a constructor parameter)
    * */
    private FunctionalityManager functionalityManager;
    private ServiceTracker functionalityTracker;

//    private HashSet<CapabilityBean> capabilities;
    private HashSet<Object> providers;
    private HashSet<CollectionListener<CapabilityBean>> listeners;
    private Table2<CapabilityBean,Object> linker;

    private Map<Object,Collection<CapabilityBean>> providerToCapabilities;

    public LocalCapabilityManager(ServiceTracker functionalityTracker) {
        this.functionalityTracker = functionalityTracker;

//		this.capabilities = new HashSet<>();
		this.listeners = new HashSet<>();
		this.providers = new HashSet<>(); // TODO keep it ? same as providerToCapabilities.keySet()
		this.linker = new Table2<>(); // TODO keep this linker ?
        this.providerToCapabilities = new HashMap<>();
    }

    @Override
    public synchronized void addListener(CollectionListener<CapabilityBean> listener) {
        System.out.println("LocalCapabilityManager.addListener("+listener+")");
        this.listeners.add(listener);
    }

    @Override
    public synchronized void removeListener(CollectionListener<CapabilityBean> listener) {
        System.out.println("LocalCapabilityManager.removeListener("+listener+")");
        this.listeners.remove(listener);
    }

    @Override
    public Collection<CollectionListener<CapabilityBean>> getListeners() {
	return this.listeners;
    }

    @Override
    public Collection<CapabilityBean> getCapabilities() {
	    //return this.capabilities;
        Collection<Collection<CapabilityBean>> allProvidedCapabilities = providerToCapabilities.values();
        Collection<CapabilityBean> allCapabilities = new HashSet<>();
        for (Collection<CapabilityBean> collection : allProvidedCapabilities) {
            allCapabilities.addAll(collection);
        }
        return allCapabilities;
    }

    @Override
    public synchronized void addCapabilityProvider(Object p) {
		System.out.println("LocalCapabilityManager.addCapabilityProvider("+p.getClass().getName()+")");
		Class providerClass = p.getClass();

		CapabilityProvider cp = (CapabilityProvider) providerClass.getAnnotation(CapabilityProvider.class);
		if(cp != null){
			// p is a CapabilityProvider
			this.providers.add(p);

            Collection<CapabilityBean> capabilities = getCapabilitiesFromProvider(p);
			for(CapabilityBean c : capabilities) {
//				this.capabilities.add(c);
				this.linker.put(c,p);
				//notification of listeners
				for (CollectionListener<CapabilityBean> l : this.listeners) {
					l.onAdd(c);
				}
			}

            this.providerToCapabilities.put(p,capabilities);
		}

    }
	
    @Override
    public synchronized void removeCapabilityProvider(Object p) {
        System.out.println("LocalCapabilityManager.removeCapabilityProvider("+p+")");
        CapabilityProvider cp = p.getClass().getAnnotation(CapabilityProvider.class);
        if(cp != null){
            // p is a CapabilityProvider
            this.providers.remove(p);
            Collection<CapabilityBean> removedCapabilities = this.providerToCapabilities.remove(p);

            //this.capabilities.removeAll(removedCapabilities);

            for (CapabilityBean c : removedCapabilities) {
                //notification of listeners
                for (CollectionListener<CapabilityBean> l : this.listeners) {
                    l.onRemove(c);
                }
            }

        }
    }

    /**
     * Parses capabilityProvider's annotations and inspect implemented functionalities
     * @param p capability provider
     * @return a collection of {@code CapabilityBean}
     */
    private Collection<CapabilityBean> getCapabilitiesFromProvider(Object p) {
        Set<CapabilityBean> capabilities = new HashSet<>();

        CapabilityProvider cp = p.getClass().getAnnotation(CapabilityProvider.class);
        if (cp == null) {
            throw new IllegalArgumentException("Expected an object annotated with @CapabilityProvider tag");
        }

        // from annotations
        Capability[] capabilityAnnotations = cp.capabilities();

        // and from code inspection
        Class providerClass = p.getClass();

        // look for implemented functionalities
        Class[] functionalities = getImplementedFunctionalities(providerClass);
        for (Class functionality : functionalities) {

            // functionality

            // get functionalities by FunctionalityManager service instead of reparsing Functionality interfaces
            FunctionalityManager funcManager = (FunctionalityManager) functionalityTracker.getService();
            // TODO manage capabilities by avatarId
            FunctionalityProviderBean functionalityProviderBean = funcManager.getFunctionalityByInterface(null, functionality);

            if (functionalityProviderBean == null) {
                // TODO Houston, we have a problem.
            }

            // create corresponding capability
            String capabilityName = null;
            String capabilityDescription = null;

            // lookup in capability annotations
            for (Capability c : capabilityAnnotations) {
                if (AsawooUtils.isNotEmpty(c.functionality()) && c.functionality().equals(functionalityProviderBean.getName())) {
                    // a capability annotation exists for this functionality

                    // name
                    if (AsawooUtils.isNotEmpty(c.name())) {
                        capabilityName = c.name();
                    }

                    // description
                    if (AsawooUtils.isNotEmpty(c.description())) {
                        capabilityDescription = c.description();
                    }

                    break;
                }
            }

            if (capabilityName == null) { // = no capability name declared in annotations
                capabilityName = providerClass.getSimpleName() + functionality.getSimpleName();
            }

            if (capabilityDescription == null) {
                if (AsawooUtils.isNotEmpty(functionalityProviderBean.getDescription())) {
                    capabilityDescription = functionalityProviderBean.getDescription() + " (" + providerClass.getSimpleName() + ")";
                }
            }


            CapabilityBean capability = new CapabilityBean(functionalityProviderBean, capabilityName, capabilityDescription);

            capabilities.add(capability);

        }

        return capabilities;
    }

    @Override
    public Collection<CapabilityBean> getCapabilities(Object p) {
        CapabilityProvider cp = p.getClass().getAnnotation(CapabilityProvider.class);
        if(cp != null){
            return providerToCapabilities.get(p);
            // TODO or reparse and inspect ?
            // getCapabilitiesFromProvider(p);
        } else { return null; }
    }

    @Override
    public Collection<Object> getCapabilityProviders() {
	    return this.providerToCapabilities.keySet();
    }

    @Override
    // TODO not used yet, keep this ?
    public Collection<Object> getCapabilityProviders(CapabilityBean c) {
        // TODO maintain linker
        // or parse the map and look for this capability

	    return this.linker.getRowsB(c);
    }

    /**
     * TODO replace by JSON-LD ?
     * @return
     */
    @Override
    public String getOwlCapabilities() {
    	asawoo.core.annotation.Method method = null;
		String ret = "";
		for (CapabilityBean c : getCapabilities()) {
			System.out.println("Found capability : " + c.getName());
//			method = c.method();
//			for (asawoo.core.annotation.Method m : methods) {
//				System.out.println("\t" + method.name());
				ret += "<owl:NamedIndividual rdf:about=\"http://liris.cnrs.fr/asawoo/DATA/functionality.owl#"
						+ c.getName()
						+ "\"><rdf:type rdf:resource=\"http://liris.cnrs.fr/asawoo/DATA/functionality.owl#"
						+ "undefined"
						+ "\"/><implements rdf:resource=\"http://liris.cnrs.fr/asawoo/DATA/functionality.owl#"
						+ "undefined" + "\"/></owl:NamedIndividual>";
//			}
		}
		return ret;
    }
    
    @Override
    public Object executeCapability(String capability){
    	System.out.println("Invoke capability : " + capability);
		boolean end = false;
		Object ret = null;
	/*	asawoo.core.annotation.Method capabilityMethod = null;
		Method method = null;
		for (Capability c : this.capabilities) {
			capabilityMethod = c.method();
//			for (Attribute a : atts) {
//			if (a.name().equals("method") && v.equals(a.value())) {
			if (capability.equals(c.name())) {
					Object o = this.getCapabilityProviders(c).toArray()[0];
					// /!\ Use Reflection API
					// try to get method to invoke capability
					try {
						method = o.getClass().getMethod(capabilityMethod.name());
					} catch (NoSuchMethodException e) {
						System.err
								.println("[NoSuchMethodException] for class \""
										+ o.getClass().getName()
										+ "\" with method "
										+ capabilityMethod.name()
										+ "\"");
					} catch (SecurityException e) {
						System.err
								.println("[SecurityException] for class \""
										+ o.getClass().getName()
										+ "\" with method "
										+ capabilityMethod.name()
										+ "\"");
					}

					// try to invoke method
					try {
						ret = method.invoke(o);
					} catch (IllegalAccessException e) {
						System.err
								.println("[IllegalAccessException] when invoke \""
										+ method.getName()
										+ "\" from class "
										+ o.getClass().getName() + "\"");
					} catch (IllegalArgumentException e) {
						System.err
								.println("[IllegalArgumentException] when invoke \""
										+ method.getName()
										+ "\" from class "
										+ o.getClass().getName() + "\"");
					} catch (InvocationTargetException e) {
						System.err
								.println("[InvocationTargetException] when invoke \""
										+ method.getName()
										+ "\" from class "
										+ o.getClass().getName() + "\"");
					}
					// End use Reflection API
					// Optimisation when a lot of elements are presents
					end = true;
					break;
				}
//			}
//			if (end)
//				break;
		}
		if(!end) System.out.println("Capability " + capability + " not found");
		*/
		return ret;
    }

	/**
	 *
	 * @param objectType
	 * @return an array of functionalities implemented by this objectType or an empty array if none
	 */
	private Class[] getImplementedFunctionalities(Class objectType) {
		List<Class> functionalities = new ArrayList<>();
		Class[] interfaces = objectType.getInterfaces();
		if (interfaces.length < 1) {
			return new Class[]{};
		}
		for (Class anInterface : interfaces) {
			if (anInterface.isAssignableFrom(FunctionalityInterface.class)) {
				functionalities.add(anInterface);
			}
		}

		return functionalities.toArray(new Class[functionalities.size()]);
	}

    /**
     * TODO : remove this method which is not required anymore
     * Uses Reflection and Proxy Invocation Handler to dynamically change annotation's property
     * Piece of code found there: http://stackoverflow.com/questions/14268981/modify-a-class-definitions-annotation-string-parameter-at-runtime
     *
     * Changes the annotation value for the given key of the given annotation to newValue and returns
     * the previous value.
     */
    private Object changeAnnotationValue(Annotation annotation, String key, Object newValue){
        Object handler = Proxy.getInvocationHandler(annotation);
        Field f;
        try {
            f = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        f.setAccessible(true);
        Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) f.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Object oldValue = memberValues.get(key);
        if (oldValue == null || oldValue.getClass() != newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(key,newValue);
        return oldValue;
    }
    
}

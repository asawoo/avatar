/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.capability.remote;

import java.util.Collection;

import asawoo.core.annotation.Capability;
import asawoo.core.capability.CapabilityBean;
import asawoo.core.capability.CapabilityManager;
import asawoo.core.util.CollectionListener;

/**
 * 
 * @author lesommer
 * @version 0.1.0
 */
public class RemoteCapabilityManager implements CapabilityManager{

    @Override
    public void addListener(CollectionListener<CapabilityBean> listener) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public void removeListener(CollectionListener<CapabilityBean> listener) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public Collection<CollectionListener<CapabilityBean>> getListeners() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Collection<CapabilityBean> getCapabilities() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Collection<CapabilityBean> getCapabilities(Object p) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Collection<Object> getCapabilityProviders(CapabilityBean c) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Collection<Object> getCapabilityProviders() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void addCapabilityProvider(Object p) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public void removeCapabilityProvider(Object p) {
	// TODO Auto-generated method stub
	
    }

	@Override
	public String getOwlCapabilities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object executeCapability(String capability) {
		// TODO Auto-generated method stub
		return null;
	}

}

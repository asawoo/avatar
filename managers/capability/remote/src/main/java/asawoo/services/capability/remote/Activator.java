/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package asawoo.services.capability.remote;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;

import asawoo.core.capability.CapabilityManager;

public class Activator implements BundleActivator{
	
    private ServiceRegistration serviceRegistration;
    private ServiceTracker tracker;
    
    @Override
    public void start(final BundleContext context) throws Exception {
//	LocalCapabilityManager capabilityManager = new LocalCapabilityManager();
//	Dictionary<String, String> props = new Hashtable<>();
// 
//        props.put("service.exported.interfaces", "*");
//        props.put("service.exported.configs", "org.apache.cxf.ws");
//        props.put("org.apache.cxf.ws.address", "http://localhost:9090/capability-manager");
//
//	this.serviceRegistration = context.registerService(asawoo.core.capability.CapabilityManager.class.getName(), capabilityManager, props);
//	ServiceReference refServ = context.getServiceReference(CapabilityManager.class.getName());
//	if(refServ == null) {
//	    System.out.println("The CapabilityManager cannot be found!!!");
//	}
	
	tracker = new ServiceTracker(context, asawoo.core.capability.CapabilityManager.class.getName(), null) {
	    @Override
	    public Object addingService(ServiceReference reference) {
		Object service = super.addingService(reference);
		if (service instanceof CapabilityManager) {
		    System.out.println("The CapabilityManager has been found!!!");
		}
		return service;
	    }
	};
	tracker.open();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
//	this.serviceRegistration.unregister();
	tracker.close();
    }

}
package asawoo.services.net.rest.impl;

/**
 * Created by alban on 27/11/17.
 */
public class Neighbor {
    public String platformUrl;
    public String label;
    public Neighbor() {}
    public Neighbor(String platformUrl, String label) {
        this.platformUrl = platformUrl;
        this.label = label;
    }
}
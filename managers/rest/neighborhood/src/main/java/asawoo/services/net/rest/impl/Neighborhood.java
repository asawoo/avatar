package asawoo.services.net.rest.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alban on 27/11/17.
 */
public class Neighborhood {
    private Collection<Neighbor> neighbors;
    private Neighbor me;

    public Neighborhood() {};
    

    public Neighborhood(Neighbor me, Collection<Neighbor> neighbors) {
        this.me = me;
        this.neighbors = neighbors;
    }

    public Neighbor getMyself() {
      return this.me;
    }

    public Collection<Neighbor> getNeighbors() {
      return this.neighbors;
    }

    public void setMyself(Neighbor myself) {
      this.me = myself;
    }

    public void setNeighbors(Collection<Neighbor> neighbors) {
      this.neighbors = neighbors;
    }

}

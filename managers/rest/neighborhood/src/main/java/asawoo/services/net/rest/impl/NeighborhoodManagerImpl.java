package asawoo.services.net.rest.impl;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.util.AsawooUtils;
import asawoo.core.wotapp.WoTApp;
import asawoo.services.net.dtn.service.DTNNeighborDiscoveryListener;
import asawoo.vertx.VertxConfiguration;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.net.rest.RestRegistryListener;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.util.DTNConstants;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.SchemeBean;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.*;
import io.vertx.core.http.impl.HttpServerResponseImpl;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.felix.ipojo.Nullable;
import org.apache.felix.ipojo.annotations.*;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @author Alban Mouton <alban.mouton@koumoul.com>
 */
@Component
@Provides
@Instantiate
public class NeighborhoodManagerImpl implements DTNNeighborDiscoveryListener {

    @Requires
    private Vertx vertx;

    @Requires
    private Router router;

    @Requires
    private DTNAdapter dtnAdapter;

    @Requires
    private VertxConfiguration vertxConfig;

    @Requires
    private VertxHttpClient httpClient;

    @Requires
    private RestServiceRegistry restRegistry;

    private Logger logger = LoggerFactory.getLogger(NeighborhoodManagerImpl.class);

    private Neighbor myself;
    private Map<String, Neighbor> neighbors = new HashMap<String, Neighbor>();

    private final static String NEIGHBORHOOD_PATH_MAPPING = "/neighborhood";
    private final static String NEIGHBORHOOD_SYSTEM_FUNCTIONALITY = "asawoo.neighborhood";


    @Validate
    private void start() {
        logger.info("Init neighborhood manager");
        myself = new Neighbor("http://[" + dtnAdapter.getHost().split("%")[0] + "]:" + AsawooConfiguration.getHttpPort(), dtnAdapter.getNodeId());

        this.dtnAdapter.addNeighborDiscoveryListener(this);


        restRegistry.registerSystemRoute(NEIGHBORHOOD_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(NEIGHBORHOOD_PATH_MAPPING, HttpMethod.GET.name(), null));
        router.get(NEIGHBORHOOD_PATH_MAPPING).handler(routingContext -> {
            String neighborhoodJson = Json.encodePrettily(new Neighborhood(myself, neighbors.values()));
            routingContext.response()
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(neighborhoodJson.length()))
                    .write(neighborhoodJson)
                    .end();
        });
    }

    @Invalidate
    private void stop() {
        this.dtnAdapter.removeNeighborDiscoveryListener(this);
    }

    @Override
    public void onNeighborContact(String nodeId) {
        logger.info("New neighbor detected nodeId: " + nodeId);

        HttpClient client = vertx.createHttpClient(new HttpClientOptions());
        String url = "http://localhost:" + AsawooConfiguration.getHttpPort()
                                        + DTNConstants.DTN_PROXY_URL_MAPPING + "?" + DTNConstants.DTN_URI
                                        + "=" + URLEncoder.encode("http://" + nodeId + NEIGHBORHOOD_PATH_MAPPING);
        logger.info("Fetch info from new neighbor: " + url);
        client.getAbs(url, res -> {
            res.exceptionHandler(error -> {
                logger.error(error);
            });
            res.bodyHandler(buffer -> {
                String body = buffer.getString(0, buffer.length());
                logger.info("Neighbor info response: " + body);
                Neighborhood neighborhood = Json.decodeValue(body, Neighborhood.class);
                neighbors.put(nodeId, neighborhood.getMyself());
            });
        })
        .end();
    }

    @Override
    public void onNeighborLoss(String nodeId) {
        if (neighbors.containsKey(nodeId)) {
            // TODO: uncomment when C3PO bug is fixed. For now neighbors are always lost.
            neighbors.remove(nodeId);
        }
    }
}

package asawoo.manager.net.rest.registrar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.util.AsawooUtils;
import asawoo.vertx.VertxConfiguration;
import asawoo.core.annotation.Parameter;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.*;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.core.util.PlatformUtils;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.felix.ipojo.Factory;
import org.apache.felix.ipojo.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.*;

/**
 * Listens to Functionalities and add them to the functionality Manager
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 10/05/16.
 */
@Component
@Instantiate
public class FunctionalityListener {

    @Requires
    private FunctionalityManager functionalityManager;

    @Requires
    private RestServiceRegistry restServiceRegistry;

    @Requires
    private Router router;

    @Requires
    private VertxConfiguration vertxConfiguration;

    private RestRouteMapper routeMapper;

    private List<String> instanceNames = Collections.synchronizedList(new ArrayList<String>());

    @Validate
    private void start() {
        if (routeMapper == null) {
            routeMapper = new RestRouteMapper(router, restServiceRegistry);
        }
    }

    @Bind(optional = true, aggregate = true)
    public void bindFunctionality(FunctionalityInterface funcInterface, Map<String, Object> serviceProperties) {
        Class providerClazz = funcInterface.getClass();
        Class[] implementedFunctionalities = providerClazz.getInterfaces();

        String avatarId = (serviceProperties != null && serviceProperties.containsKey(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID)) ?
                (String) serviceProperties.get(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID) // TODO iPOJO component MUST set propagation = true or else Avatar publishes in the SvcRegistry
                : AsawooConfiguration.getPlatformAvatarId()/* TODO get actual AvatarID instead*/;

//        boolean alreadyRegistered = providerRegistered(providerClazz);

        // TODO this way, the first instance registered does not add up in the URI. Good if we have only one instance..
//        String instanceName = alreadyRegistered ? getInstanceName(providerClazz, serviceProperties) : null;
        String instanceName = getInstanceName(providerClazz, serviceProperties, avatarId, true);

        for (Class funcClazz : implementedFunctionalities) {
            if (FunctionalityInterface.class.isAssignableFrom(funcClazz)) {

                // System.out.println("FUNCTIO : " + funcClazz.getName());

                FunctionalityProviderBean fpb = functionalityManager.getOrAddFunctionality(avatarId, funcClazz, funcInterface, instanceName, serviceProperties);

                // System.out.println("NEW FUNCTIONALITY : " + providerClazz.getName() + " // " + fpb.getName());

                if (fpb.isExposable()) {
                    // TODO filter by context before publishing ?
                    routeMapper.exposeFunctionality(fpb);
                }

            }
        }

    }

    @Unbind(optional = true, aggregate = true)
    public void unbindFunctionality(FunctionalityInterface funcInterface, Map<String, Object> serviceProperties) {
        System.out.println("FUNCTIONALITY : "+funcInterface.getClass().getName()+" IS GONE");
        Class providerClazz = funcInterface.getClass();
        Class[] implementedFunctionalities = providerClazz.getInterfaces();

        String avatarId = (serviceProperties != null && serviceProperties.containsKey(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID)) ?
                (String) serviceProperties.get(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID) // TODO that does not work, figure a way to publish avatarId as service property - or else Avatar publishes in the SvcRegistry
                : AsawooConfiguration.getPlatformAvatarId()/* TODO get actual AvatarID instead*/;

        String instanceName = getInstanceName(providerClazz, serviceProperties, avatarId, false);

        for (Class funcClazz : implementedFunctionalities) {
            if (FunctionalityInterface.class.isAssignableFrom(funcClazz)) {
                // retrieve existing bean
                FunctionalityProviderBean fb = functionalityManager.getOrAddFunctionality(avatarId, funcClazz, funcInterface, instanceName, serviceProperties);
                // remove it from functionality manager
                functionalityManager.removeFunctionality(fb);
                if (fb.isExposable()) {
                    // remove route
                    routeMapper.removeRoute(fb);
                }
            }
        }

    }

    @Bind(optional = true, aggregate = true)
    public void bindFunctionalityListener(FunctionalityManagerListener listener) {
        // System.out.println("NEW LISTENER : "+listener.getClass().getName());
        functionalityManager.addListener(listener);
    }

    @Unbind(optional = true, aggregate = true)
    public void unbindFunctionalityListener(FunctionalityManagerListener listener) {
        // System.out.println("REMOVE LISTENER : "+listener.getClass().getName());
        functionalityManager.removeListener(listener);
    }

    private synchronized String getInstanceName(Class providerClazz, Map<String, Object> serviceProperties, String avatarId, boolean allowCreation) {
        String instanceName;
        if (serviceProperties.containsKey(Factory.INSTANCE_NAME_PROPERTY)) {
            String iPojoInstanceName = (String) serviceProperties.get(Factory.INSTANCE_NAME_PROPERTY);
            instanceName = AsawooUtils.getInstanceNameFromiPojo(avatarId, iPojoInstanceName);
            if (!allowCreation) {
                return instanceName;
            }
        } else {
            instanceName = providerClazz.getSimpleName();
            int counter = 0;
            while (counter < 1000) { // max 1000 instances
                String generated = instanceName+counter;
                counter++;
                // Verify uniqueness
                if (!instanceNames.contains(generated)) {
                    instanceName = generated;
                    break;
                }
            }
        }

        instanceNames.add(instanceName);
        return instanceName;
    }


}

package asawoo.manager.net.rest.registrar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.annotation.Parameter;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.net.rest.RestServiceRegistry;
import com.google.common.collect.*;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

import javax.ws.rs.core.MediaType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 02/05/17.
 */
public class RestRouteMapper {

    private Router router;
    private RestServiceRegistry restServiceRegistry;

    /* <HTTP method + path, provider> */
    private ListMultimap<String, FunctionalityProviderBean> providersByRoute = ArrayListMultimap.create();

    /* <HTTP method + path, route> */
    private Map<String, Route> routes = Maps.newHashMap();

    public RestRouteMapper(Router router, RestServiceRegistry restServiceRegistry) {
        this.router = router;
        // Allow cross-domain requests for all future routes
        router.route().handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.PUT)
                .allowedMethod(HttpMethod.DELETE)
                .allowedMethod(HttpMethod.GET)
                .allowedHeader(HttpHeaders.CONTENT_TYPE.toString())
        );
        this.restServiceRegistry = restServiceRegistry;
    }

    /**
     * Exposes functionality as a REST service
     * @param functionality
     */
    public void exposeFunctionality(FunctionalityProviderBean functionality) {
        Map<String, FunctionalityRequest> fReq = functionality.getMethods();

        Class<? extends FunctionalityInterface> funcClazz = functionality.getImplementedFunctionality();

        for (String m : fReq.keySet()) {

            // create vertx route
            FunctionalityRequest fr = fReq.get(m);

            String path = getMethodPath(functionality.getBaseUri(), functionality.getInstanceName(), fr.url);

            boolean alreadyRegistered = routeRegistered(path, fr.verb);

            // store this URI mapping anyway
            providersByRoute.put(concatVerbPath(fr.verb, path), functionality);

            // only registers new routes
            if (!alreadyRegistered) {

                // System.out.println("---- NEW ROUTE REGISTERED : "+path+ " ------ "+funcClazz);

                routes.put(concatVerbPath(fr.verb, path),
                            router.route(HttpMethod.valueOf(fr.verb), path)/*.handler(BodyHandler.create().)*/
                                .handler(routingContext -> {
                                            HttpServerResponse response = routingContext.response();

                                            MultiMap queryParams = routingContext.request().params();
                                            // any Body ?
                                            String body = routingContext.getBodyAsString();

                                            JsonObject methodParams = null;

                                            if (HttpMethod.valueOf(fr.verb) != HttpMethod.GET
                                                && body != null && !body.isEmpty()) {
                                                methodParams = new JsonObject(body);
                                            } else if (HttpMethod.valueOf(fr.verb) == HttpMethod.GET
                                                    && queryParams != null && !queryParams.isEmpty()) {

                                                methodParams = new JsonObject();
                                                for (Map.Entry<String, String> entry : queryParams.entries()) {
                                                    methodParams.put(entry.getKey(), entry.getValue());
                                                }
                                            }

                                            //System.out.println("++++Body params : " + methodParams.toString());


                                            try {
                                                // get the right provider
                                                FunctionalityProviderBean matchingProvider = findProviderByPath(concatVerbPath(routingContext.request().method().name(),
                                                                routingContext.request().path()),
                                                                queryParams);

                                                Object res = invoke(m, matchingProvider, methodParams);

                                                if (res != null) {
                                                    String json = Json.encodePrettily(res);

                                                    response.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                                                            .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                                                            .write(json);
                                                }
                                                response.end();
                                            } catch (UnsupportedOperationException unsoe) {
                                                String errorMessage = "Method " + m + " is not implemented by " + functionality.getFunctionalityProvider().getClass() + "\n"
                                                        + unsoe.getMessage();
                                                response.setStatusCode(HttpResponseStatus.NOT_IMPLEMENTED.code())
                                                        .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(errorMessage.length()))
                                                        .end(errorMessage);
                                            } catch (ReflectiveOperationException e) {
                                                e.printStackTrace();
                                                String errorMessage = "Could not invoke method " + m + " on " + functionality.getFunctionalityProvider().getClass();
                                                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code())
                                                        .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(errorMessage.length()))
                                                        .end(errorMessage);
                                            }

                                            // TODO redirect to capability provider + invoke method
                                            // TODO almost same job as what apache cxf does.... useful ?
                                        }
                                )
                );
            }

            // TODO fix this: add service properties and register all methods at once -- OK ?
//                    restServiceRegistry.publishUri(true, AsawooConfiguration.getApplianceName()/* TODO get AvatarID instead*/, fb.getInterfaceName(), m, fb.getFunctionalityUrisByMethod(m));

            // System.out.println("---- Method : " + m + " // " + functionality.getFunctionalityUrisByMethod(m).url);
        }

        // TODO use functionalityBean instead of REB
        // publish in service registry
        RegistryEntryBean reb = new RegistryEntryBean(true, functionality);
        restServiceRegistry.publishUri(reb);

    }

    /**
     *
     * @param functionality
     */
    public void removeRoute(FunctionalityProviderBean functionality) {

        Map<String, FunctionalityRequest> fReq = functionality.getMethods();

        for (String m : fReq.keySet()) {

            // create vertx route
            FunctionalityRequest fr = fReq.get(m);

            String path = getMethodPath(functionality.getBaseUri(), functionality.getInstanceName(), fr.url);

            if (routeRegistered(path, fr.verb)) {
                String routeId = concatVerbPath(fr.verb, path);
                providersByRoute.remove(routeId, functionality);
                Route route = routes.remove(routeId);
                route.remove(); // remove from Vert.x router
            }
        }


        // unpublish from REST registry
        restServiceRegistry.removeService(functionality);

    }

    private String getMethodPath(String baseUri, String instanceName, String url) {
        String fullUri = (url.startsWith("/")) ?
                baseUri+"/"+instanceName+url
                : url;
        return URI.create(fullUri).getPath();
    }

    private FunctionalityProviderBean findProviderByPath(String verbPath, MultiMap queryParams) {

        List<FunctionalityProviderBean> funcProviders = providersByRoute.get(verbPath);

        if (funcProviders == null || funcProviders.isEmpty()) {
            return null;
        }

        if (queryParams == null || queryParams.isEmpty()) {
            // return the first and only (? check ?) one
            return funcProviders.get(0);
        }

        for (FunctionalityProviderBean provider : funcProviders) {
            boolean matches = true;
            Map<String, Object> svcProps = provider.getServiceProperties();
            if (svcProps != null && !svcProps.isEmpty()) {
                for (Map.Entry<String, String> entry : queryParams.entries()) {
                    if (svcProps.containsKey(entry.getKey()) && !svcProps.get(entry.getKey()).equals(entry.getValue())) {
                        matches = false;
                        break;
                    }
                }
            }
            if (matches) {
                return provider;
            }
        }

        // no provider matching against query parameters has been found
        // return the first and only (? check ?) one
        return funcProviders.get(0);

    }

    private Object invoke(String methodName, FunctionalityProviderBean functionalityProvider, JsonObject params) throws InvocationTargetException, IllegalAccessException {
        Method method = null;
        for (Method m : functionalityProvider.getImplementedFunctionality().getMethods()) {
            if (m.getName().equals(methodName)) {
                method = m;
                // TODO also match on parameters args length
                break;
            }
        }

        java.lang.reflect.Parameter[] mParams = method.getParameters();
        ArrayList<Object> orderedParams = null;

        /*
        // parameter names from annotation
        Class<?> ptypes[]=method.getParameterTypes();
        int nargs=method.getParameterTypes().length;
        System.out.println("------ NARGS : "+nargs);

        Annotation[][] parameterAnnotations=(Annotation[][])method.getParameterAnnotations();
        System.out.println("------- ANNOT SIZE "+parameterAnnotations.length);

        System.out.println("-------- CLAZZ : "+functionalityProvider.getClass().getName());

        ArrayList<String> parameterNames=new ArrayList<String>();
        for (int i=0;i<nargs;i++) {
            String pname=((Parameter)parameterAnnotations[i][0]).name();
            parameterNames.add(pname);
        }

        for (String p:parameterNames) {
            System.out.println("\t LALALALALALALALA "+p);
        }

        System.out.println(" ******* NB annot PARAM"+parameterAnnotations.length);
        */

        if (mParams != null && mParams.length > 0) {
            orderedParams = new ArrayList();

            for (int i=0; i<mParams.length; i++) {//java.lang.reflect.Parameter p : mParams) {
                java.lang.reflect.Parameter p = mParams[i];

                // TODO CAUTION : if class is not compiled with javac -parameters, classes do not contain parameter names, they are generically named 'arg0', ...
                String parameterName;
                // first check if an annotation is present and use annotation's name instead
                Parameter pAnnot = p.getAnnotation(Parameter.class);//parameterAnnotations[i][0];
                if (pAnnot != null) {
                    parameterName = pAnnot.name();
                } else {
                    // use parameter name as in .class file
                    parameterName = p.getName();
                }

//                String paramValue = params.getString(parameterName);
                Object paramValue = null;
                if (params != null && !params.isEmpty()) {
                    paramValue = params.getValue(parameterName);
                }


                Class paramType = p.getType();
                if (paramValue != null) {
                    orderedParams.add(castParamValue(paramValue, paramType));
                } else {
                    orderedParams.add(null);
                }

/*
                if (paramValue != null) {
//                    orderedParams.add(paramType.cast(paramValue));
                    orderedParams.add(Json.decodeValue(paramValue, paramType));
                }
                */
// TODO deal with complex object parameters
//                JsonObject jo = new JsonObject(paramValue);

                System.out.println("-*-*-*-*- INVOKATION ** "+methodName+" --- parameters :: "+parameterName + " ("+p.getType()+") " +" -- value: "+paramValue);
            }

        }



        Object[] paramValues = orderedParams == null ? null : orderedParams.toArray();


        return method.invoke(functionalityProvider.getFunctionalityProvider(), paramValues);

    }

    private Object castParamValue(Object paramValue, Class paramType) {

        if (paramValue instanceof String) {
            System.out.println("------- STRING");
            if (paramType.equals(String.class)) {
                return paramValue;
            } else {
                return Json.decodeValue((String) paramValue, paramType);
            }
        } else if (paramValue instanceof Boolean) {
            System.out.println("------- BOOL");
            return Boolean.valueOf((boolean)paramValue);
        } else if (paramValue instanceof Number) {
            return paramValue;
        }

        // complex types
        else if (paramValue instanceof JsonObject) {
            JsonObject paramValueJS = (JsonObject) paramValue;
            return Json.decodeValue(paramValueJS.encode(), paramType);
        } else if (paramValue instanceof JsonArray) {
            // TODO
            JsonArray paramValueJS = (JsonArray) paramValue;
            List values = paramValueJS.getList(); //Lists.newArrayList();
            return values;

        } else {
            // default
            System.out.println("------- DEFAULT OBJ");
            return paramValue;
        }
    }

    private String concatVerbPath(String httpMethod, String path) {
        return httpMethod+path;
    }

    private boolean providerRegistered(Class providerClazz) {
        Collection<Class> providerClazzes = Collections2.transform(providersByRoute.values(), input -> input.getFunctionalityProvider().getClass());
        return providerClazzes.contains(providerClazz);
    }

    private boolean routeRegistered(String path, String verb) {
        return providersByRoute.containsKey(concatVerbPath(verb, path));
    }

}

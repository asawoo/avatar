package asawoo.services.net.rest.impl;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.functionality.FunctionalityProviderBean;
import asawoo.core.util.AsawooUtils;
import asawoo.vertx.VertxConfiguration;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.FunctionalityInterface;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RegistryEntryBean;
import asawoo.core.net.rest.RestRegistryListener;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.SchemeBean;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.felix.ipojo.Nullable;
import org.apache.felix.ipojo.annotations.*;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 25/05/16.
 */
@Component
@Provides
@Instantiate
public class RestServiceRegistryImpl implements RestServiceRegistry {

    private static final String SOURCE_REGISTRY_HEADER = "SOURCE_REGISTRY_HEADER";

    @Requires
    private Vertx vertx;

    @Requires
    private Router router;

    @Requires(optional = true, proxy = false, nullable = false)
    private DTNAdapter dtnAdapter;

    @Requires
    private VertxConfiguration vertxConfig;

    @Requires
    private VertxHttpClient httpClient;

    private Logger logger = LoggerFactory.getLogger(RestServiceRegistryImpl.class);

    private final String MAPPING_LOCAL = "/local";
    private final String MAPPING_REMOTE = "/remote";
    private final String MAPPING_DTN = "/dtn";
    private final String MAPPING_SYSTEM = "/system";


    private String functionalityParameter = "func";
    private String methodParameter = "method";
    private String communicationModeParameter = "commode";

    private String localRegistryUriServiceId;

    private final Set<Route> routes = new HashSet<>();

    private Long advertisingTimerId;

    private String lastRegistryAdvertisementMessageId;

    /**
     * Map<AvatarId,
     *      MultiMap<functionality, registryEntry>>
     */
    private Map<String, Multimap<String, RegistryEntryBean>> localRegistry = new HashMap<>();

    /**
     * Map<AvatarId,
     *      MultiMap<functionality, registryEntry>>
     */
    Map<String, Multimap<String, RegistryEntryBean>> remoteRegistry = new HashMap<>();

    private List<RestRegistryListener> listeners = new ArrayList<>();

    private boolean registryModified;

    private Multimap<String, FunctionalityRequest> systemRoutes = ArrayListMultimap.create();

    @Validate
    private void start() {

        // declare routes API
        String parameterizedPath = REST_REGISTRY_MAPPING+"/:"+functionalityParameter;// +"/:"+methodParameter;

        // get all
        router.get(REST_REGISTRY_MAPPING)
                .handler(routingContext -> {
//                    String json = Json.encodePrettily(getAllFunctionalities());
                    String json = getJsonSerializedRegistry(getAllFunctionalities());
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                            .write(json)
                            .end();
                    });

        // get local
        router.get(REST_REGISTRY_MAPPING+MAPPING_LOCAL)
                .handler(routingContext -> {
                    String json = getJsonSerializedRegistry(getFunctionalities(true));
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                            .write(json)
                            .end();
                });

        // get remote
        router.get(REST_REGISTRY_MAPPING+MAPPING_REMOTE)
                .handler(routingContext -> {
                    String json = getJsonSerializedRegistry(getFunctionalities(false));
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                            .write(json)
                            .end();
                });


        // get system functionalities
        router.get(REST_REGISTRY_MAPPING+MAPPING_SYSTEM)
                .handler(routingContext -> {
                    String json = getJsonSerializedSystemRoutes();
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                            .write(json)
                            .end();
                });

        // get dtn version
        router.get(REST_REGISTRY_MAPPING+MAPPING_DTN)
                .handler(routingContext -> {
                    // no ComMode specified => unicast
                    String json = getJsonSerializedRegistry(translateToDtnUris(getFunctionalities(false), CommunicationMode.UNICAST));
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                            .write(json)
                            .end();
                });

        router.get(REST_REGISTRY_MAPPING+MAPPING_DTN+"/:"+communicationModeParameter)
                .handler(routingContext -> {
                    String comModeCode = routingContext.request().getParam(communicationModeParameter);
                    CommunicationMode communicationMode = comModeCode == null ? null : CommunicationMode.findByCode(comModeCode);
                    if (communicationMode == null) {
                        String errorMessage = comModeCode+" is not a valid communication mode. Use "+dtnAdapter.getSupportedCommunicationModes().toString()+ " instead.";
                        logger.error(errorMessage);
                        routingContext.response()
                                .setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/text")
                                .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(errorMessage.length()))
                                .write(errorMessage)
                                .end();
                    } else {
                        String json = getJsonSerializedRegistry(translateToDtnUris(getFunctionalities(false), communicationMode));
                        routingContext.response()
                                .setStatusCode(HttpResponseStatus.OK.code())
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                                .write(json)
                                .end();
                    }

                });
        // TODO how to register system URI for parameterized path ?

        // upon POST request reception on /registry we get the functionalities from other avatars
        // TODO or PUT ?
        router.post(REST_REGISTRY_MAPPING)
                .handler(this::onSharedRegistryReception);

        // get
        router.get(parameterizedPath)
                .handler(routingContext -> {
                    String functionalityName = routingContext.request().getParam(functionalityParameter);
                    Set<RegistryEntryBean> rebs = getRegistryEntriesByFunctionality(functionalityName);

                    if (rebs != null && !rebs.isEmpty()) {
                        String json = Json.encodePrettily(rebs);
                        routingContext.response()
                                .setStatusCode(HttpResponseStatus.OK.code())
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                                .write(json)
                                .end();
                    } else {
                        routingContext.response()
                                .setStatusCode(HttpResponseStatus.NOT_FOUND.code())
                                .end();
                    }

                });



        // register itself
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING, HttpMethod.GET.name(), null));
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING, HttpMethod.POST.name(), Arrays.asList("sharedRestRegistry")));
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING+MAPPING_LOCAL, HttpMethod.GET.name(), null));
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING+MAPPING_REMOTE, HttpMethod.GET.name(), null));
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING+MAPPING_DTN, HttpMethod.GET.name(), null));
        registerSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(REST_REGISTRY_MAPPING+MAPPING_SYSTEM, HttpMethod.GET.name(), null));

        // TODO how to register system URI for parameterized path ?

    }



    @Invalidate
    private void stop() {

        // TODO unregister routes ? (delete routes from router)
        unregisterSystemRoute(REGISTRY_SYSTEM_FUNCTIONALITY, null);

    }

    @Bind(optional = true)
    private void bindDtnAdapter(DTNAdapter dtnAdapter) {
        this.dtnAdapter = dtnAdapter;

        // listen to system functionalities topics
        systemRoutes.keySet().forEach(dtnAdapter::registerMulticastSubscriber);

        // if a DTN communication module is available, use it to advertise local registry
        startAdvertisingRegistry();
        // TODO add a DTNNeighborListener ? and only advertise registry to neighbors ?
    }

    @Unbind(optional = true)
    private void unbindDtnAdapter(DTNAdapter dtnAdapter) {
        this.dtnAdapter = null;
        stopAdvertisingRegistry();
    }


    @Override
    public Map<String, Multimap<String, RegistryEntryBean>> getAllFunctionalities() {
        return getRegistry();
    }

    private Map<String, Multimap<String, RegistryEntryBean>> getRegistry() {
//        Map<String, Map<String, List<RegistryEntryBean>>> allFunctionalities = new HashMap<>();
        Map<String, Multimap<String, RegistryEntryBean>> allFunctionalities = new HashMap<>();

//        allFunctionalities.put(getLocalAvatarId(), localRegistry);
        allFunctionalities.putAll(localRegistry);
        allFunctionalities.putAll(remoteRegistry);
        return allFunctionalities;
    }

    private Map<String, Multimap<String, RegistryEntryBean>> translateToDtnUris(Map<String, Multimap<String, RegistryEntryBean>> registry, CommunicationMode communicationMode) {
        Map<String, Multimap<String, RegistryEntryBean>> dtnRegistry = new HashMap<>();
        // TODO make a whole copy of maps and their elements
        registry.forEach((avatarId, entries) -> {
            Multimap<String, RegistryEntryBean> transformedMap = Multimaps.transformValues(entries, new Function<RegistryEntryBean, RegistryEntryBean>() {
                @Override
                public RegistryEntryBean apply(RegistryEntryBean input) {
                    // methods URIs should be relative so we just need to update base URI
                    String baseDtnUri = DTNUtils.getDtnUriFromRegistryEntry(input, communicationMode, getLocalAvatarId());

                    RegistryEntryBean dtnReb = new RegistryEntryBean(input);
                    dtnReb.setBaseUri(baseDtnUri);

                    /* OLD piece of code
                    dtnReb.getMethods().forEach((method, fr) -> {
                        URI dtnUri = DTNUtils.getDtnUriFromRegistryEntry(dtnReb, method, communicationMode, getLocalAvatarId());

                        // modify functionality request uri

                        fr.url = dtnUri.toString();
                    });
                    */

                    return dtnReb;
                }
            });

            dtnRegistry.put(avatarId, transformedMap);

/*

                Multimap<String, List<RegistryEntryBean>> dtnRebs = new HashMap<String, List<RegistryEntryBean>>();
                entries.forEach((funcName, rebs) -> {
                    List<RegistryEntryBean> cloneRebs = new ArrayList<RegistryEntryBean>();
                    rebs.forEach(reb -> {
                        // clone REB
                        RegistryEntryBean dtnReb = new RegistryEntryBean(reb);
                        dtnReb.getFunctionalityRequests().forEach((method, fr) -> {
                            URI dtnUri = DTNUtils.getDtnUriFromRegistryEntry(dtnReb, method, communicationMode);
                            // modify functionality request uri
                            fr.url = dtnUri.toString();
                        });
                        cloneRebs.add(dtnReb);
                    });
                    dtnRebs.put(funcName, cloneRebs);
                });
            dtnRegistry.put(avatarId, dtnRebs);

            */
        });

        return dtnRegistry;
    }

    @Override
    public Map<String, Multimap<String, RegistryEntryBean>> getFunctionalities(boolean isLocal) {
        return isLocal ? localRegistry : remoteRegistry;

        // else
//        Map<String, List<RegistryEntryBean>> remoteFunctionalities = new HashMap<>();
//        remoteRegistry.values().forEach(functionalities -> { // for each avatar
//            functionalities.forEach((functionality, rebs) -> {
//                if (remoteFunctionalities.containsKey(functionality)) {
//                    remoteFunctionalities.get(functionality).addAll(rebs);
//                } else {
//                    remoteFunctionalities.put(functionality, rebs);
//                }
//            });
//        });
//        return remoteFunctionalities;
    }

    @Override
    public Set<RegistryEntryBean> getRegistryEntriesByFunctionality(String functionalityName) {

        Set<RegistryEntryBean> rebs = new HashSet<>();

//        localRegistry.forEach((funcName, entries) -> {
//            if (funcName.equals(functionalityName)) {
//                rebs.addAll(entries);
//            }
//        });

        localRegistry.values().forEach(registry -> {
            if (registry.containsKey(functionalityName)) {
                rebs.addAll(registry.get(functionalityName));
            }

//            rebs.addAll(Multimaps.filterKeys(registry, new Predicate<String>() {
//                @Override
//                public boolean apply(String input) {
//                    return input.equals(functionalityName);
//                }
//            }).values());


//            registry
//            registry.forEach((funcName, entries) -> {
//                if (funcName.equals(functionalityName)) {
//                    rebs.addAll(entries);
//                }
//            });
        });

        remoteRegistry.values().forEach(registry -> {
            rebs.addAll(Multimaps.filterKeys(registry, new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.equals(functionalityName);
                }
            }).values());
//            registry.forEach((funcName, entries) -> {
//                if (funcName.equals(functionalityName)) {
//                    rebs.addAll(entries);
//                }
//            });
        });

        return rebs;
    }

    @Override
    public Set<RegistryEntryBean> getFilteredRegistryEntriesByFunctionality(String functionalityName, Map<String, Object> propertiesFilter) {

        if (propertiesFilter == null)
            return getRegistryEntriesByFunctionality(functionalityName);

        Set<RegistryEntryBean> rebs = new HashSet<>();

        getRegistry().values().forEach(registry -> {

            rebs.addAll(Multimaps.filterValues(registry, new Predicate<RegistryEntryBean>() {
                        @Override
                        public boolean apply(RegistryEntryBean input) {
                            Map<String, Object> props = input.getServiceProperties();
                            boolean matches = false;
                            if (props != null) {
                                for (Map.Entry<String, Object> property : propertiesFilter.entrySet()) {
                                    if (props.containsKey(property.getKey())
                                            && props.get(property.getKey()).equals(property.getValue())) {
                                        matches = true;
                                        break;
                                    }
                                }
//                                propertiesFilter.forEach((filterKey, filterValue) -> {
//                                    if (props.containsKey(filterKey)
//                                            && props.get(filterKey).equals(filterValue)) {
//                                        matches = true;
//                                    }
//                                });
                            }
                            return matches;
                        }
                    }).values());


//                    registry.forEach((funcName, entries) -> {
//                        if (funcName.equals(functionalityName)) {
//                            entries.forEach(reb -> {
//                                Map<String, Object> props = reb.getProperties();
//                                if (props != null) {
//                                    propertiesFilter.forEach((filterKey, filterValue) -> {
//                                        if (props.containsKey(filterKey)
//                                                && props.get(filterKey).equals(filterValue)) {
//                                            rebs.add(reb);
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                    });
        });

        return rebs;

    }

    @Override
    public Set<FunctionalityRequest> getUriByFunctionalityMethod(Class<? extends FunctionalityInterface> functionalityClass, Method method) {
        return getUriByFunctionalityMethod(functionalityClass.getName(), method.getName());
    }

    @Override
    public Set<FunctionalityRequest> getUriByFunctionalityMethod(String functionalityName, String methodName) {

        Set<FunctionalityRequest> frs = new HashSet<>();

        // // TODO perhaps we should not answer with remote URIs ? localRegistry only ?
        getRegistry().values().forEach(funcRebs -> {
            Collection<RegistryEntryBean> rebs = funcRebs.get(functionalityName);
            rebs.forEach(reb -> {
                reb.getMethods().forEach((method, funcReq) -> {
                    if (methodName.equals(method)) {
                        frs.add(funcReq);
                    }
                });
            });

//            funcRebs.forEach((funcName, rebs) -> {
//                if (funcName.equals(functionalityName)) {
//                    rebs.forEach(reb -> {
//                        reb.getFunctionalityRequests().forEach((method, funcReq) -> {
//                            if (methodName.equals(method)) {
//                                frs.add(funcReq);
//                            }
//                        });
//                    });
//                }
//            });
        });

        return frs;

    }

    /*
    @Override
    public FunctionalityRequest getUriByFunctionalityMethod(String functionalityMethodName) {

        Set<RegistryEntryBean> registryEntries = new HashSet<>();
        FunctionalityRequest fr = null;
        for (String serviceId : localRegistry.keySet()) {
            if (serviceId.contains(functionalityMethodName)) {
                fr = localRegistry.get(serviceId).getFunctionalityRequest();
                break;
            }
        }

        for (String serviceId : remoteFunctionalitiesRegistry.keySet()) {
            if (serviceId.contains(functionalityMethodName)) {
                fr = remoteFunctionalitiesRegistry.get(serviceId).getFunctionalityRequest();
            }
        }

        return null;
    }*/

    @Override
    public void publishUri(RegistryEntryBean registryEntry) {

        String funcName = registryEntry.getName();
        String avatarId = registryEntry.getAvatarId();

        Map<String, Multimap<String, RegistryEntryBean>> reg;

        // TODO remove this temporary case ? should always be local ?
        if (!registryEntry.isLocal()) {
            reg = remoteRegistry;
        } else {
            reg = localRegistry;
        }

        if (!reg.containsKey(avatarId)) {
            reg.put(avatarId, ArrayListMultimap.create());
        }
        reg.get(avatarId).put(funcName, registryEntry);

        // notify
        notifyListenersAdded(registryEntry);

        if (registryEntry.isLocal()) {
            // updates modified status
            registryModified = true;
        }

    }

    @Override
    public void removeService(FunctionalityProviderBean functionality) {

        Multimap<String, RegistryEntryBean> localFunc = localRegistry.get(functionality.getAvatarId());

        List<RegistryEntryBean> rebsToRemove = Lists.newArrayList();
        for (RegistryEntryBean reb : localFunc.get(functionality.getName())) {
            if (reb.getBaseUri().equals(functionality.getBaseUri())
                    && reb.getInstanceName().equals(functionality.getInstanceName())) {
                rebsToRemove.add(reb);
            }
        }

        rebsToRemove.forEach(regEntry -> {
            localFunc.remove(functionality.getName(), regEntry);
            // notify
            notifyListenersRemoved(regEntry);
        });

        if (rebsToRemove.size() > 0)
            registryModified = true;
    }

    @Override
    public String findFunctionalityNameByUri(String uri) {

        List<RegistryEntryBean> r = getAllPlainEntries();

        for (RegistryEntryBean reb : r) {
            for (FunctionalityRequest funcReq : reb.getMethods().values()) {
                if (funcReq.url.contains(uri)) {
                    return reb.getName();
                }
            }
        }

        // not found
        return null;
    }

    @Override
    public Set<String> findFullUriByFunctionalityAndMethodPath(String functionalityInterface, String methodPath) {

        Set<String> matchingUris = Sets.newHashSet();

        List<RegistryEntryBean> r = getAllPlainEntries();

        for (RegistryEntryBean reb : r) {
            if (reb.getInterfaceName().equals(functionalityInterface) || reb.getName().equals(functionalityInterface)) {
                for (FunctionalityRequest funcReq : reb.getMethods().values()) {
                    String knownUri = funcReq.url;
                    if (knownUri.equals(methodPath)) { // TODO use endsWith() instead if using full URI
                        matchingUris.add(AsawooUtils.getFullURI(reb, methodPath));
                    }
                }
            }
        }

        return matchingUris;
    }

    @Override
    public void registerSystemRoute(String systemFunctionality, FunctionalityRequest functionalityRequest) {
        if (!systemRoutes.containsKey(systemFunctionality)) {
            // it is the first time that a route is registered for this system functionality
            // order the DTN middleware to listen to the right topic
            if (dtnAdapter != null
                    && !(dtnAdapter instanceof Nullable)) {
                dtnAdapter.registerMulticastSubscriber(systemFunctionality);
            }
        }
        systemRoutes.put(systemFunctionality, functionalityRequest);
    }

    @Override
    public void unregisterSystemRoute(String systemFunctionality, String url) {
        if (url != null) {
            for (Iterator<FunctionalityRequest> it = systemRoutes.get(systemFunctionality).iterator(); it.hasNext();) {
                if (url.equals(it.next().url)) {
                    it.remove();
                }
            }
        } else {
            systemRoutes.removeAll(systemFunctionality);
        }

        // and order the DTN middleware to stop listening to that topic
        if (dtnAdapter != null
                && !(systemRoutes.containsKey(systemFunctionality)
                    || systemRoutes.get(systemFunctionality).size() < 1)) {
            dtnAdapter.unregisterMulticastSubscriber(systemFunctionality);
        }
    }

    @Override
    public Multimap<String, FunctionalityRequest> getSystemRoutes() {
        return systemRoutes;
    }

    @Override
    public void addListener(RestRegistryListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void removeListener(RestRegistryListener listener) {
        this.listeners.remove(listener);
    }

    private void startAdvertisingRegistry() {

/* TODO : should we really publish this entry since it is not a functionality and it is common to all avatars ? same goes for /avatars and other "system" services
        // add the corresponding POST entry in the local registry
        String host = System.getenv().getOrDefault("HOSTNAME", "localhost");
        try {
            String localhostName = InetAddress.getLocalHost().getHostName();
            host = localhostName;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        int port = vertxConfig.getConfig().getInteger(VertxConfiguration.HTTP_PORT);


        FunctionalityRequest fr = new FunctionalityRequest("http://"
                +host+":"
                +port
                +REST_REGISTRY_MAPPING,
                "POST",
                Arrays.asList("registryEntriesMap"));

        Map<String, FunctionalityRequest> frs = new HashMap<>(1);
        frs.put("postRegistry", fr);
        RegistryEntryBean reb = new RegistryEntryBean(true, getLocalAvatarId(), REGISTRY_SYSTEM_FUNCTIONALITY, frs, null);// new RegistryEntryBean(true, dtnAdapter.getNodeId(), "registry", fr, null);
        publishUri(reb);
*/

//        // test decodeValue
//        Map<String, RegistryEntryBean> registry = getRegistry();
//        String registryJS = Json.encode(registry);
//        httpClient.executeRequest("POST", URI.create("http://localhost:9999/registry"),null, registryJS, r -> {});


        // Periodically send registry advertisement
        Integer DELAY = 5000; // 5s
        advertisingTimerId = vertx.setPeriodic(DELAY.longValue(), timerId -> {
            // TODO: just advertise once: the message will be kept in DTN middleware cache
            // Actually we need to republish the registry if functionalities set has changed.

            if (dtnAdapter != null
                    && !(dtnAdapter instanceof Nullable)
                    && !localRegistry.isEmpty()
                    && registryModified) { // only send non-null or modified registry

                // TODO ONLY advertise when in contact with a neighbor ? (avoids too many self-broadcasting when alone)
                // or when
                // TODO change local to remote for each registry entry before advertising ??

                URI dtnUri = URI.create("http+mcast://" + REGISTRY_SYSTEM_FUNCTIONALITY + REST_REGISTRY_MAPPING);
                SchemeBean scheme = new SchemeBean("http", CommunicationMode.MULTICAST, true);

                DTNRequestMetadata requestMetadata = new DTNRequestMetadata(dtnUri, scheme, "POST");
                requestMetadata.setCreationTime(System.currentTimeMillis());
//                requestMetadata.setExpirationTime(DELAY);

                // Map URIs to DTN uris so that they can be used remotely
                Map<String, Multimap<String, RegistryEntryBean>> dtnRemoteRegistry = translateToDtnUris(localRegistry, CommunicationMode.UNICAST);
                // make sure sent data is serializable
                String requestBody = getJsonSerializedRegistry(dtnRemoteRegistry);

                // TODO ? use HttpClient to proxy instead ?
                try {
                    // purge previous registry occurence
                    if (lastRegistryAdvertisementMessageId != null) {
                        dtnAdapter.purgeMessage(lastRegistryAdvertisementMessageId);
                    }

                    // response does not matter, so we do not need a callback
                    lastRegistryAdvertisementMessageId = dtnAdapter.forwardRequest(requestMetadata, null, requestBody, null);
                } catch (DTNUnsupportedComModeException e) {
                    e.printStackTrace();
                }

                synchronized (this) {
                    registryModified = false;
                }

            }
        });

    }

    private void stopAdvertisingRegistry() {
        // only valid if registry is sent periodically
        if (advertisingTimerId != null) {
            vertx.cancelTimer(advertisingTimerId);
        }
    }

    private void onSharedRegistryReception(RoutingContext routingContext) {


        JsonObject registryJS = routingContext.getBodyAsJson();

        // TODO optimize ?
        registryJS.forEach(regEntry -> {

            String avatarId = regEntry.getKey();

            // if local from itself, problem -> TODO : deal with this case
            if (isLocal(avatarId)) {
                logger.error("Received shared local registry, which should not happen");
                return;
            }

            JsonObject avatarRegJS = registryJS.getJsonObject(avatarId);
            Multimap<String, RegistryEntryBean> registry = ArrayListMultimap.create();

            avatarRegJS.forEach(strObjEntry -> {

                String funcName = strObjEntry.getKey();
                JsonArray rebsJS = avatarRegJS.getJsonArray(funcName);
                rebsJS.forEach(rebJS -> {
                    RegistryEntryBean reb = Json.decodeValue(((JsonObject) rebJS).encode(), RegistryEntryBean.class);
                    // set as remote
                    reb.setLocal(false);
                    registry.put(funcName, reb);
                });
                //            List<RegistryEntryBean> rebs = rebsJS.getList().;
                //            List<RegistryEntryBean> rebs = Json.decodeValue(registryJS.getJsonObject(funcName).encode(), List.class);

            });


            if (!remoteRegistry.containsKey(avatarId)) {
                // new avatar registry
                remoteRegistry.put(avatarId, registry);
                // notify listeners
                registry.values().forEach(newReb -> notifyListenersAdded(newReb));
            } else {
                Multimap<String, RegistryEntryBean> avatarRegistry = remoteRegistry.get(avatarId);

                // check if old entries are missing
                avatarRegistry.keySet().forEach(functionalityName -> {
                    if (!registry.containsKey(functionalityName)) {
                        avatarRegistry.get(functionalityName).forEach(reb -> notifyListenersRemoved(reb));

                        // TODO remove from avatarRegistry ? or keep old entries ?

                    } else {
                        // TODO updated ?
                        avatarRegistry.putAll(functionalityName, registry.get(functionalityName));
                        // notifyListenersUpdated();
                    }
                });

                // add remaining functionalities
                registry.keySet().forEach(functionalityName -> {
                    if (!avatarRegistry.containsKey(functionalityName)) {
                        Collection<RegistryEntryBean> newRebs = registry.get(functionalityName);
                        avatarRegistry.putAll(functionalityName, newRebs);
                        // notify listeners
                        newRebs.forEach(newReb -> notifyListenersAdded(newReb));

                    }
                });

//                avatarRegistry.putAll(registry);

//                // add one by one
//                registry.keySet().forEach((functionalityName) -> {
//
//                    if (!avatarRegistry.containsKey(functionalityName)) {
//                        avatarRegistry.put(functionalityName, avatarRegistry.);
//                        // notify listeners
//                        registryEntries.forEach(newReb -> notifyListenersAdded(newReb));
//                    } else {
//                        // TODO try to identify differences between RegistryEntryBean lists
//                    }
//
//                /* TODO delete old code
//                // if local from a remote source -> remote
//                if (registryEntry.isLocal() && !remoteFunctionalitiesRegistry.containsKey(id)) {
//                    registryEntry.setLocal(false);
//                    remoteFunctionalitiesRegistry.put(id, registryEntry);
//                    // notify listeners
//                    notifyListenersAdded(registryEntry);
//                } else if (!registryEntry.isLocal()
//                        && !localRegistry.containsKey(id)
//                        && !remoteFunctionalitiesRegistry.containsKey(id)) {
//
//                    // add remote registry entries
//                    remoteFunctionalitiesRegistry.put(id, registryEntry);
//
//                    // notify listeners
//                    notifyListenersAdded(registryEntry);
//                }
//                */
//
//                });



                // TODO : check for modified entries ?? check if functionalities have been removed ? Maintain 1 registry by HOST/Avatar ?

            }

        });

//        logger.info("Shared registry received "+registryJS.encodePrettily());



    }

    private String getAvatarIdFromRegistry(Map<String, List<RegistryEntryBean>> registry) {
        String firstFunctionality = registry.keySet().iterator().next();
        RegistryEntryBean firstReb = registry.get(firstFunctionality).get(0);
//        RegistryEntryBean firstReb = registry.entrySet().iterator().next().getValue().get(0);
        return firstReb.getAvatarId();
    }

    private List<RegistryEntryBean> getAllPlainEntries() {
        List<RegistryEntryBean> rebs = new ArrayList<>();
        localRegistry.values().forEach(funcRebs -> {
            rebs.addAll(funcRebs.values());
//            funcRebs.values().forEach(rebList -> {
//                rebs.addAll(rebList);
//            });
        });

        return rebs;

    }

    private String getJsonSerializedRegistry(Map<String, Multimap<String, RegistryEntryBean>> registry) {

        Map<String, Map<String, Collection<RegistryEntryBean>>> serializableRegistry = new HashMap<>();
        registry.forEach((avatarId, mmap) -> {
            serializableRegistry.put(avatarId, Multimaps.asMap(mmap));
        });

        return Json.encodePrettily(serializableRegistry);
    }

    private String getJsonSerializedSystemRoutes() {
        JsonObject jo = new JsonObject();
        systemRoutes.keySet().forEach(systemFunctionality -> {
            JsonArray ja = new JsonArray();
            systemRoutes.get(systemFunctionality).forEach(fr -> ja.add(new JsonObject(Json.encode(fr))));
            jo.put(systemFunctionality,ja);
        });
        return jo.encodePrettily();
    }

    private boolean isLocal(String avatarId) {
        return localRegistry.containsKey(avatarId);
        //getLocalAvatarId().equals(avatarId);
    }

    private String getLocalAvatarId() {

        String dtnNodeId = (dtnAdapter != null) ? dtnAdapter.getNodeId() : null;

        if (dtnNodeId == null) {
            try {
                return "platform@" + InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return AsawooConfiguration.getPlatformAvatarId();
            }
        } else {
            return dtnNodeId;
        }

//        String localAvatarId = AsawooConfiguration.getPlatformAvatarId();
//        if (localAvatarId != null) {
//            return localAvatarId;
//        } else {
//            // TODO return proper avatar ID
//            return dtnAdapter.getNodeId();
//        }
    }

    private void notifyListenersAdded(RegistryEntryBean reb) {
        for (RestRegistryListener l : listeners) {
            l.onServiceAdded(reb);
        }
    }

    private void notifyListenersUpdated(RegistryEntryBean reb) {
        for (RestRegistryListener l : listeners) {
            l.onServiceModified(reb);
        }
    }

    private void notifyListenersRemoved(RegistryEntryBean reb) {
        for (RestRegistryListener l : listeners) {
            l.onServiceRemoved(reb);
        }
    }

}

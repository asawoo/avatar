package asawoo.services.http.client;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import io.vertx.core.Handler;
import io.vertx.core.http.HttpClientResponse;

import java.net.URI;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 06/04/16.
 */
public interface VertxHttpClient {

    void executeRequest(String httpMethod, URI uri, Map<String, String[]> parameters, String requestBody, Handler<HttpClientResponse> responseHandler);

}

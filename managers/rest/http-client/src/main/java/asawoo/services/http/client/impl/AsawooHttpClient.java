package asawoo.services.http.client.impl;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.services.http.client.HttpClientService;
import asawoo.services.http.client.VertxHttpClient;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
//import org.apache.http.client.ResponseHandler;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;

import javax.ws.rs.HttpMethod;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 28/03/16.
 */
@Component(publicFactory = false)
@Provides
@Instantiate
public class AsawooHttpClient extends AbstractVerticle implements HttpClientService, VertxHttpClient {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    //private HttpClient vertxHttpClient;
//    CloseableHttpClient httpClient;

    private AsawooHttpClient() {
//        this.httpClient  = HttpClients.createDefault();
    }

    @Override
    public void start() throws Exception {
      // FIXED: reusing the same htpclient seems to be a bad practice
      //  this.vertxHttpClient = vertx.createHttpClient();
    }

    @Override
    public void executeRequest(String httpMethod, URI uri, Map<String, String[]> parameters, String requestBodyJson, Handler<HttpClientResponse> responseHandler) {

        // create HttpClient each time it is needed
        HttpClient vertxHttpClient = vertx.createHttpClient();

        String requestUri = uri.getPath();
        if (uri.getQuery() != null) {
            requestUri = requestUri + "?" + uri.getQuery();
        }
        if (uri.getFragment() != null) {
            requestUri = requestUri + uri.getFragment();
        }
        System.out.println("Request URI : "+requestUri);

        System.out.println("Request Body : "+requestBodyJson);

        switch (httpMethod) {
            case HttpMethod.GET:
                HttpClientRequest getReq = vertxHttpClient.get(uri.getPort(), "localhost", requestUri);
                logger.info("VERTX HTTP client: calling GET on "+getReq.absoluteURI());
                getReq.handler(responseHandler)
//                setTimeout(5000) // TODO added for test purposes
                      .end();
                break;
            case HttpMethod.POST:
                // TODO add body part
                HttpClientRequest postReq = vertxHttpClient.post(uri.getPort(), "localhost", requestUri);
                if (requestBodyJson != null && requestBodyJson.length() > 0) {
                    postReq.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(requestBodyJson.length()))
                        .handler(responseHandler)
                        .write(requestBodyJson);
                } else {
                    postReq.handler(responseHandler);
                }
//              .setTimeout(5000) // TODO added for test purposes
                postReq.end();
                break;
            case HttpMethod.PUT:
                // TODO add body part
                HttpClientRequest putReq = vertxHttpClient.put(uri.getPort(), "localhost", requestUri);
                if (requestBodyJson != null && requestBodyJson.length() > 0) {
                    putReq.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(requestBodyJson.length()))
                            .handler(responseHandler)
                            .write(requestBodyJson);
                } else {
                    putReq.handler(responseHandler);
                }

//                        .setTimeout(5000) // TODO added for test purposes
                putReq.end();
                break;
            case HttpMethod.DELETE:
                // TODO
                break;
            default:

        }

    }

}

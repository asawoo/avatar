package asawoo.services.http.client;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

//import org.apache.http.HttpResponse;
//import org.apache.http.client.ResponseHandler;

import java.net.URI;
import java.util.Map;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 24/03/16.
 */
public interface HttpClientService {

//    <T> T executeRequest(String httpMethod, URI uri, Map<String, String> parameters, ResponseHandler<T> responseHandler);

    /**
     * @param httpMethod
     * @param uri
     * @param parameters
     * @param responseHandler
     * return HTTP response content as a java.lang.String
     */
//    String executeRequest(String httpMethod, URI uri, Map<String, String[]> parameters, ResponseHandler<String> responseHandler);

//    /**
//     * No return value, ResponseHandler processes the response itself
//     *
//     * @param httpMethod
//     * @param uri
//     * @param parameters
//     * @param responseHandler
//     */
//    void executeRequest(String httpMethod, URI uri, Map<String, String> parameters, ResponseHandler responseHandler);

}

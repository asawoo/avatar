package asawoo.services.semanticrepo;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import org.apache.felix.ipojo.annotations.*;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.sparql.modify.request.QuadDataAcc;
import org.apache.jena.sparql.modify.request.UpdateDataDelete;
import org.apache.jena.sparql.modify.request.UpdateDataInsert;
import org.apache.jena.update.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

@Component
@Provides
@Instantiate
public class SemanticRepo {

	// TODO: externalize this ? Only one explicitly defined config key ?
	public final String[] POSSIBLE_URIS = new String[]{
		"http://localhost:3030",
		"http://semantic-repos:3030"
	};

	public final String LIST_GRAPHS_QUERY = "SELECT ?g WHERE {GRAPH ?g { } }";

	private String serviceUri;

	private Logger logger = LoggerFactory.getLogger(SemanticRepo.class);

	@Requires
	private Router router;

	@Requires
	private Vertx vertx;

	private void checkAvailability() throws RuntimeException {
		Arrays.stream(POSSIBLE_URIS).forEach(uri -> {
			QueryExecution q = QueryExecutionFactory.sparqlService(uri + "/asawoo", LIST_GRAPHS_QUERY, HttpClients.createDefault());

			try {
				ResultSet results = q.execSelect();
				int i = 0;
				while (results.hasNext()) {
					QuerySolution soln = results.nextSolution();
					i++;
				}

				if (i >= 3)	serviceUri = uri;
			} catch(Exception e) {
				// System.out.println("Caught exception " + e.getMessage());
			}
		});

		if (serviceUri == null) {
			throw new RuntimeException("Semantic repository is unavailable");
		}
	}

	private void reexpose() {
		// Re-exposing the full semantic repo under asawoo router.
		// This way it will be protected by the web-proxy manager without cross-domain problems.
		HttpClient client = vertx.createHttpClient(new HttpClientOptions());
		router.route("/semantic-repo/*").handler(ctx -> {
			HttpServerRequest req = ctx.request();
			URL dest;
			try {
				dest = new URL(serviceUri);
			} catch(MalformedURLException e) {
				return;
			}

			HttpClientRequest c_req = client.request(
					req.method(),
					dest.getPort(),
					dest.getHost(),
					req.uri().replaceAll("/semantic-repo", ""),
					c_res -> {

				req.response().setChunked(true);
				req.response().setStatusCode(c_res.statusCode());
				req.response().headers().setAll(c_res.headers());
				c_res.handler(data -> {
					req.response().write(data);
				});
				c_res.endHandler((v) -> req.response().end());
			});
			c_req.setChunked(true);
			c_req.headers().setAll(req.headers());
			c_req.write(ctx.getBody());
			c_req.end();
		});
	}

	public ResultSet select(String query) throws RuntimeException {
		if (serviceUri == null) throw new RuntimeException("The semantic repository is not available");
		QueryExecution q = QueryExecutionFactory.sparqlService(serviceUri + "/asawoo", query, HttpClients.createDefault());
		ResultSet results = q.execSelect();
		return results;
	}

	public Model construct(String query) throws RuntimeException {
		if (serviceUri == null) throw new RuntimeException("The semantic repository is not available");
		QueryExecution q = QueryExecutionFactory.sparqlService(serviceUri + "/asawoo", query, HttpClients.createDefault());
		Model model = q.execConstruct();
		return model;
	}

	public void insert(Model model, String graph)  throws RuntimeException {
		if (serviceUri == null) throw new RuntimeException("The semantic repository is not available");
		// String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";
        StmtIterator it = model.listStatements();
        QuadDataAcc data = new QuadDataAcc();
        if (graph != null) data.setGraph(NodeFactory.createURI(graph));
        while(it.hasNext()) {
			Statement statement = it.nextStatement();
            data.addTriple(statement.asTriple());
		}
        UpdateRequest updateRequest = UpdateFactory.create();
        updateRequest.add(new UpdateDataInsert(data));
        logger.debug("Run update query " + updateRequest.toString());
        UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, serviceUri + "/asawoo", HttpClients.createDefault());
		updateProcessor.execute();
	}

	public void delete(Model model, String graph)  throws RuntimeException {
		if (serviceUri == null) throw new RuntimeException("The semantic repository is not available");
		StmtIterator it = model.listStatements();
		QuadDataAcc data = new QuadDataAcc();
		if (graph != null) data.setGraph(NodeFactory.createURI(graph));
		while(it.hasNext()) {
			Statement statement = it.nextStatement();
			data.addTriple(statement.asTriple());
		}
		UpdateRequest updateRequest = UpdateFactory.create();
		updateRequest.add(new UpdateDataDelete(data));
		logger.debug("Run update query " + updateRequest.toString());
		UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, serviceUri + "/asawoo", HttpClients.createDefault());
		updateProcessor.execute();
	}

	@Validate
	private void start() throws RuntimeException, InterruptedException {
		logger.info("Start the Semantic repo connector");
		try {
			checkAvailability();
		} catch(RuntimeException e) {
			logger.info("First attempt at connecting to semantic repo, failed, try again after 5s");
			Thread.sleep(5000);
			checkAvailability();
		}
		reexpose();
	}
}

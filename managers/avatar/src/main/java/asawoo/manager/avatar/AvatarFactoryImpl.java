package asawoo.manager.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.avatar.AvatarFactory;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.config.AvatarConfiguration;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.ipojo.*;
import org.apache.felix.ipojo.annotations.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * This class is in charge of avatar creation and disposal. It manages avatar instances, based on iPOJO component factories and instances.
 *
 * It also publishes REST services to create / dispose
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 12/12/16.
 */
@Component
@Provides
@Instantiate
public class AvatarFactoryImpl implements AvatarFactory {

    @Requires(filter="(factory.name=asawoo.avatar.factory)")
    private Factory avatarFactory;

    /* Map <avatarID, avatarInstance> */
    private Map<String, ComponentInstance> instances = new HashMap<>();

    private Logger logger = LoggerFactory.getLogger(AvatarFactoryImpl.class);

    @Validate
    private void start() {
        // register platform Avatar
        String platformAvatarId = PREFIX_PLATFORM+"-"+AsawooConfiguration.getPlatformAvatarId();
        AvatarConfiguration platformAvatarConfig = new AvatarConfiguration(
                new JsonObject()
                        .put(AvatarConfiguration.PROPERTY_AVATAR_ID, platformAvatarId));
        createAvatar(platformAvatarConfig);
    }

    @Invalidate
    private void stop() {
        // unregister platform avatar
        // register platform Avatar
        String platformAvatarId = PREFIX_PLATFORM+"-"+AsawooConfiguration.getPlatformAvatarId();
        instances.remove(platformAvatarId).dispose();
    }

    public void createAvatar(AvatarConfiguration configuration) {
        logger.info("AvatarFactory - Create avatar: " + configuration.toString());
        logger.info("With functionalities: " + configuration.getFunctionalitiesConfigurations());

        String avatarId = configuration.getAvatarId();
        String avatarLabel = configuration.getAvatarLabel();

        if (instances.containsKey(avatarId)) {
            // throw exception duplicate avatar
            throw new RuntimeException("An avatar with the same ID is already deployed [ID="+avatarId+"]");
        } else if (Arrays.asList(AsawooConfiguration.RESERVED_WORDS).contains(avatarId)) {
            throw new RuntimeException("Avatar ID: "+avatarId+" is forbidden as it is a reserved word");
        }

        Properties componentConfiguration = new Properties();
        componentConfiguration.put(Factory.INSTANCE_NAME_PROPERTY, PREFIX_AVATAR+"-"+avatarId);
        componentConfiguration.put(AvatarConfiguration.PROPERTY_AVATAR_ID, avatarId);
        if (avatarLabel != null) {
            componentConfiguration.put(AvatarConfiguration.PROPERTY_AVATAR_LABEL, avatarLabel);
        }
        componentConfiguration.put(AvatarConfiguration.PROPERTY_CONFIGURATION, configuration);
        try {
            ComponentInstance inst = avatarFactory.createComponentInstance(componentConfiguration); //configuration.getFunctionalitiesConfigurations()
            instances.put(avatarId, inst);
            // avatar is then published in the "avatar registry" via OSGi Avatar service publication
        } catch (UnacceptableConfiguration unacceptableConfiguration) {
            unacceptableConfiguration.printStackTrace();
        } catch (MissingHandlerException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void disposeAvatar(String avatarId) {
        if (instances.containsKey(avatarId)) {
            instances.remove(avatarId).dispose();
        }
    }


}

package asawoo.manager.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.avatar.Avatar;
import asawoo.core.avatar.AvatarFactory;
import asawoo.core.avatar.AvatarRegistryService;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.vertx.VertxConfiguration;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.util.PlatformUtils;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.felix.ipojo.annotations.*;

import javax.ws.rs.core.MediaType;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;

/**
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 16/12/16.
 */
@Component(immediate = true)
@Provides
@Instantiate
public class AvatarRegistry implements AvatarRegistryService {

    private static final String AVATAR_SYSTEM_FUNCTIONALITY = "asawoo.avatar";

    @Requires
    private AvatarFactory avatarFactory;

    //@Requires(optional = true, specification = Avatar.class)
    private List<Avatar> avatars;

    @Requires
    private Router router;

    @Requires
    private VertxConfiguration vertxConfiguration;

    @Requires
    private RestServiceRegistry restRegistry;

    private String baseUri;

    private final String PROPERTY_URI = "uri";

    private final String PATH_AVATAR = "/avatars";

    public AvatarRegistry() {
        int port = AsawooConfiguration.getHttpPort();
        //vertxConfiguration.getConfig().getInteger(VertxConfiguration.HTTP_PORT);

        String hostname = null;
        try {
            hostname = PlatformUtils.getIpAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (hostname == null) {
            hostname = PlatformUtils.getHostname();
        }

        baseUri = "http://"+hostname+":"+port+PATH_AVATAR;

        avatars = Lists.newArrayList();
    }

    @Validate
    private void start() {

        // register Vert.x routes

        router.route(HttpMethod.GET, PATH_AVATAR).handler(this::getAvatars);

        router.route(HttpMethod.POST, PATH_AVATAR).handler(this::addAvatar);

        // register these routes in Registry
        registerRoute(PATH_AVATAR, HttpMethod.GET.name(), null);
        registerRoute(PATH_AVATAR, HttpMethod.POST.name(), Arrays.asList("avatarConfiguration"));

        // FIXME: also register a generic route ? or a specific one ?
        registerRoute(PATH_AVATAR+"/:avatarId", HttpMethod.GET.name(), null);
        registerRoute(PATH_AVATAR+"/:avatarId", HttpMethod.PUT.name(), Arrays.asList("avatarConfiguration"));
        registerRoute(PATH_AVATAR+"/:avatarId", HttpMethod.DELETE.name(), null);

    }

    @Invalidate
    private void stop() {
        // remove avatar system routes
        restRegistry.unregisterSystemRoute(AVATAR_SYSTEM_FUNCTIONALITY, null);
    }

    @Bind(optional = true, aggregate = true)
    private void bindAvatar(Avatar avatar) {
        avatars.add(avatar);

        // create new routes : GET, PUT (update) and DELETE
        String avatarId = avatar.getAvatarId();
        router.get(PATH_AVATAR+"/"+avatarId).handler(routingContext -> {
            String json = avatar.toJson()
                            .put(PROPERTY_URI, getAvatarURI(avatarId))
                            .encodePrettily();
            routingContext.response()
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                    .write(json)
                    .end();
        });

        router.put(PATH_AVATAR+"/"+avatarId).handler(this::updateAvatar);

        router.delete(PATH_AVATAR + "/" + avatarId).handler(routingContext -> deleteAvatar(routingContext, avatarId));

        // add corresponding system routes
        registerRoute(PATH_AVATAR+"/"+avatarId, HttpMethod.GET.name(), null);
        registerRoute(PATH_AVATAR+"/"+avatarId, HttpMethod.PUT.name(), Arrays.asList("avatarConfiguration"));
        registerRoute(PATH_AVATAR+"/"+avatarId, HttpMethod.DELETE.name(), null);

    }

    @Unbind(optional = true, aggregate = true)
    private void unbindAvatar(Avatar avatar) {
        // unregister system route
        restRegistry.unregisterSystemRoute(AVATAR_SYSTEM_FUNCTIONALITY, PATH_AVATAR+"/"+avatar.getAvatarId());
        avatars.remove(avatar);

    }

    private void registerRoute(String path, String httpMethod, List<String> parameters) {
        restRegistry.registerSystemRoute(AVATAR_SYSTEM_FUNCTIONALITY,
                new FunctionalityRequest(path, httpMethod, parameters));
    }

    @Override
    public List<String> getAvatars() {
        return Lists.transform(avatars, new Function<Avatar, String>() {
            @Override
            public String apply(Avatar avatar) {
                return avatar.getUri();
            }
        });
    }

    @Override
    public void addAvatar(AvatarConfiguration avatarConfiguration) {
        avatarFactory.createAvatar(avatarConfiguration);
    }

    @Override
    public void removeAvatar(String avatarId) {
        avatarFactory.disposeAvatar(avatarId);
    }

    @Override
    public void updateAvatar(AvatarConfiguration avatarConfiguration) {
        // TODO
    }

    //@Override
    public String getOwlDescription() {
        return null;
    }

    private void getAvatars(RoutingContext routingContext) {
        // get avatar list
        List<JsonObject> avatars = Lists.transform(this.avatars, new Function<Avatar, JsonObject>() {
                                        @Override
                                        public JsonObject apply(Avatar avatar) {
                                            return avatar.toJson().put(PROPERTY_URI, getAvatarURI(avatar.getAvatarId()));
                                        }
                                    });
        String json = new JsonArray(avatars).encodePrettily();
        routingContext.response()
                .setStatusCode(HttpResponseStatus.OK.code())
                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length()))
                .write(json)
                .end();
    }

    private void addAvatar(RoutingContext routingContext) {

        JsonObject avatarConfigurationJS = routingContext.getBodyAsJson();
        AvatarConfiguration avatarConfiguration = new AvatarConfiguration(avatarConfigurationJS);
        addAvatar(avatarConfiguration);

        String avatarUri = getAvatarURI(avatarConfiguration.getAvatarId());
        String response = new JsonObject()
                            .put(PROPERTY_URI, avatarUri)
                            .encodePrettily();

        routingContext.response()
                .setStatusCode(HttpResponseStatus.OK.code())
                .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(response.length()))
                .write(response)
                .end();

    }

    private void updateAvatar(RoutingContext routingContext) {

        JsonObject avatarConfigurationJS = routingContext.getBodyAsJson();
        AvatarConfiguration avatarConfiguration = new AvatarConfiguration(avatarConfigurationJS);

        updateAvatar(avatarConfiguration);

        routingContext.response()
                .setStatusCode(HttpResponseStatus.OK.code())
                .end();

    }

    private void deleteAvatar(RoutingContext routingContext, String avatarId) {

//        String avatarId = routingContext.getBodyAsString();

        if (avatarId.equals(AsawooConfiguration.getPlatformAvatarId())) {
            String errorMessage = "Cannot DELETE platform avatar";

            routingContext.response()
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                    .putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(errorMessage.length()))
                    .write(errorMessage)
                    .end();
        } else {
            removeAvatar(avatarId);

            routingContext.response()
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .end();
        }


    }

    private String getAvatarURI(String avatarId) {
        return baseUri+"/"+avatarId;
    }

}

package asawoo.manager.avatar;
/*
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */

import asawoo.core.avatar.Avatar;
import asawoo.core.config.AsawooConfiguration;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.functionality.CollaborativeFunctionalityManager;
import asawoo.core.functionality.FunctionalityBean;
import asawoo.core.functionality.FunctionalityManager;
import asawoo.core.util.AsawooUtils;
import asawoo.core.util.CollectionListener;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.felix.bundlerepository.*;
import org.apache.felix.ipojo.*;
import org.apache.felix.ipojo.annotations.*;
import org.apache.felix.ipojo.annotations.Property;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Representation of an Avatar.
 *
 * This class exposes avatar configuration
 *
 * @author lionel touseau <lionel.touseau at univ-ubs.fr> on 17/10/16.
 *
 */
@Component(name = "asawoo.avatar.factory", propagation = true)/*(factoryMethod = "createAvatar")*/
@Provides
public class AvatarImpl implements Avatar {

    /* TODO get it from configuration ? SystemProperty ? or DTN ID ? or network interface ?
    Avatar ID is useful for REST service publication, DTN node identification, ...
    */
    @ServiceProperty(name = AvatarConfiguration.PROPERTY_AVATAR_ID)
    private String avatarId;

    @ServiceProperty(name = AvatarConfiguration.PROPERTY_AVATAR_LABEL)
    private String avatarLabel;

    @Property(name = AvatarConfiguration.PROPERTY_CONFIGURATION)
    private AvatarConfiguration configuration;

    @Requires //(proxy = false)
    private transient FunctionalityManager functionalityManager;

    @Requires //(optional = true, proxy = false, nullable = false)
    private transient CollaborativeFunctionalityManager collaborativeFunctionalityManager;

    @Requires //(optional = true, proxy = false)
    private RepositoryAdmin repositoryAdmin;

//    @Property(name = "functionalityFilter")
//    private String ldapFactoryFilter;

    private Set<ComponentInstance> spawnedFunctionalities = new HashSet<>();
    private List<String> rootFunctionalitiesNames = new ArrayList<>();
    private List<String> compositeFunctionalitiesNames = new ArrayList<>();
    private ListMultimap<String, Properties> functionalitiesToInstantiate;
    private CollectionListener<Factory> capabilityFactoryListener;
    private CollectionListener<Factory> compositeFuncFactoryListener;

    @ServiceProperty(name = "status")
    private int status = STATUS_STARTING; // TODO make status a service property or service publication trigger ?

    private String uri;

    private Logger logger = LoggerFactory.getLogger(AvatarImpl.class);




    /*
    @Bind(aggregate = true, optional = true) // TODO how to set up a dynamic filter ?? instead of binding to each factory ?
    private void bindFunctionalityFactory(Factory factory) {

        String[] specs = factory.getComponentDescription().getprovidedServiceSpecification();
        boolean isFunctionalityFactory = false;
        for (String s : specs) {
            if (s.equals(FunctionalityInterface.class.getName())) {
                isFunctionalityFactory = true;
            }
            System.out.println("++ SPEC :: "+s);
        }

        if (!isFunctionalityFactory) {
            return;
        }


        if (configuration.getFunctionnalities() != null
            && !configuration.getFunctionnalities().isEmpty()
            && configuration.getFunctionnalities().contains(factory.getName())) {

            System.out.println("------- Matching Factory detected : "+factory.getName());



            try {
                Class componentClass = factory.getBundleContext().getBundle().loadClass(factory.getName());

                Class[] implementedFunctionalities = componentClass.getInterfaces();
                for (Class iface : implementedFunctionalities) {
                    if (FunctionalityInterface.class.isAssignableFrom(iface)) {
                        System.out.println("***** Implemented func class found : " + iface.getName());
                        if (iface.isAnnotationPresent(Functionality.class)) {
                            System.out.println(" !!! FUNCTIONALITY ANNOTATION DETECTED :: "
                                    + ((Functionality) iface.getAnnotation(Functionality.class)).name());
                        }
                    }
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }




            // instantiate this functionality
            configuration.getFunctionalityConfiguration(factory.getName()).forEach(properties -> {
                try {
                    properties.put(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID, avatarId); // TODO that does not work, can't add service properties on the fly
                    ComponentInstance inst = factory.createComponentInstance(properties);
                    spawnedFunctionalities.add(inst);

                } catch (UnacceptableConfiguration unacceptableConfiguration) {
                    setStatus(STATUS_ERROR);
                    unacceptableConfiguration.printStackTrace();
                } catch (MissingHandlerException e) {
                    setStatus(STATUS_ERROR);
                    e.printStackTrace();
                } catch (ConfigurationException e) {
                    setStatus(STATUS_ERROR);
                    e.printStackTrace();
                }
            });

            // update status if all functionality instances described in the configuration have been instanciated
            if (spawnedFunctionalities.size() == configuration.getFunctionalitiesConfigurations().keys().size()) {
                setStatus(STATUS_READY);

                // when avatar and its atomic functionalities are ready, instantiate composite functionalities
                functionalityManager.getAvailableFunctionalities();


                // publish uri TODO ??
                setAvatarUri();
            }

        }
    }
    */

    @Validate
    private void start() {

        // read all root functionalities from configuration and create capability component instances
        // add a listener to listen to new factories from functionalityManager
        // and load capability-providing bundle when necessary

        // when all functionalities from configuration are instantiated, check for available composite functionalities
        // instantiate them, rinse and repeat.



        // get available functionalities

        // instantiate if available

        // register a listener on available functionalities

        if (status < STATUS_STARTING) { // sometimes functionalities are not initialized yet
            setStatus(STATUS_STARTING);
        }

        logger.info("********************* STARTING *************************");

        if (configuration.getFunctionnalities() != null
                && !configuration.getFunctionnalities().isEmpty()) {

            // init terminal functionalities to instantiate
            functionalitiesToInstantiate = ArrayListMultimap.create(configuration.getFunctionalitiesConfigurations());
            logger.info("********************* FUNC TO INSTANTIATE :: " + functionalitiesToInstantiate.size()+" *************************");

            logger.info("********************* install factory listener *************************");

            // listen for factories bound later on
            capabilityFactoryListener = new CollectionListener<Factory>() {
                @Override
                public void onAdd(Factory factory) {
                    logger.info("********************* Factory added *************************");
                    logger.info("Factory was just bound, check if avatar can use it : " + factory.getName());
                    // A functionality factory was just bound.
                    // first let's see if it matches one of the atomic functionalities configured on this avatar
                    configuration.getFunctionalitiesConfigurations().keySet().forEach(factoryName -> {
                        //String functionalityName = entry.getValue().getProperty(AvatarConfiguration.PROPERTY_FUNCTIONALITY_NAME);
                        // TODO: better link between factory instance and factory name ?
                        if (factoryName != null && factory.getName().equals(factoryName)) {
                            // System.out.println("Instantiate functionality in avatar after Factory was bound " + factoryName);
                            logger.info("********************* Matching factory found " + factoryName + " *************************");
                            instantiateFunctionality(factoryName, factory);

                        }
                    });
                }

                @Override
                public void onUpdate(Factory factory) {
                    // TODO: re-instantiate functionality when its factory was updated ?
                }

                @Override
                public void onRemove(Factory factory) {
                    // TODO handle remove and avatar that go from READY to incomplete and that must reinstantiate capabilities
                }
            };
            functionalityManager.addFactoryListener(capabilityFactoryListener);

            // instantiate functionality providers using specified factory
            Set<String> factoryNames = configuration.getFunctionalitiesConfigurations().keySet();
            factoryNames.forEach(factoryName -> instantiateFunctionality(factoryName, functionalityManager.getFunctionalityFactory(factoryName)));

        }

    }

    @Invalidate
    private void stop() {
        spawnedFunctionalities.forEach(inst -> {
            if (inst.getState() > 0)
                inst.dispose();
        });
        spawnedFunctionalities.clear();
        rootFunctionalitiesNames.clear();
    }

    public String getAvatarId() {
        return avatarId;
    }

    public String getAvatarLabel() {
        return avatarLabel;
    }

    public AvatarConfiguration getConfiguration() {
        return configuration;
    }

    public int getStatus() {
        return status;
    }

    public String getUri() {
        return uri;
    }

    private void setAvatarUri() {
//        String baseUri = "http://"+hostname+":"+port;
//        PlatformUtils.baseUri = ;
    }

    public FunctionalityManager getFunctionalityManager() {
        return functionalityManager;
    }

    /**
     *
     * @return a JSON String view of this avatar
     */
    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        return jsonObject.put(AvatarConfiguration.PROPERTY_AVATAR_ID, avatarId)
//                    .put("uri", getUri())
                    .put(AvatarConfiguration.PROPERTY_AVATAR_LABEL, avatarLabel)
                    .put("status", status)
                    .put(AvatarConfiguration.PROPERTY_CONFIGURATION, configuration.getRawConfig());
    }

    /**
     * Loads a functionality factory from the Bundle Repository
     * @param factoryName
     */
    private void loadFunctionalityFactory(String factoryName, boolean retry) {
        if (repositoryAdmin == null) {
            System.out.println("Trying to use OBR repository admin to load a functionality bundle, but it was not available");
            return;
        }
        Resolver resolver = repositoryAdmin.resolver();
        String[] nameParts = factoryName.split("\\.");
        if (nameParts.length < 2) {
            logger.error("Factory name doesn't seem to contain a package name " + factoryName);
            return;
        }
        String packageName = String.join(".", Arrays.copyOf(nameParts, nameParts.length - 1));
        Requirement packageRequirement = repositoryAdmin.getHelper().requirement("package", "(package=" + packageName + ")");
        // TODO: add requirement asawoo.capability / asawoo.functionality
        Requirement capabilityRequirement = repositoryAdmin.getHelper()
                                        .requirement(AsawooUtils.ASAWOO_CAPABILITY_NAMESPACE,
                                                "("+AsawooUtils.ASAWOO_CAPABILITY_FACTORY_FILTER_PROPERTY+"="+factoryName+")");


        Resource[] resources;
        try {
            resources = repositoryAdmin.discoverResources(new Requirement[] { packageRequirement, capabilityRequirement });
        } catch (Exception e) {
            logger.error("Failed to contact market place while loading functionality " + e.getMessage());
            return;
        }

        if (resources.length == 0) {
            logger.error("Functionality factory bundle not found in OBR repository: " + packageName);
            return;
        }
        if (resources.length > 1) {
            logger.warn("Found more than 1 bundle matching functionality factory in OBR repository: " + factoryName);
        }
        resolver.add(resources[0]);

        if (resolver.resolve()) {
            logger.info("Deploy a bundle to load functionality factory: " + factoryName);
            try {
                resolver.deploy(Resolver.START);
            } catch(IllegalStateException e) {
                logger.info("Framework state has changed while working with OBR.. try again to load functionality.");
                // FIXME do not retry

                // FIXME silence Exception
                // e.printStackTrace();
                // FIXME retry only once
                if (retry) {
                    logger.info("****************** OBR could not deploy "+factoryName+". RETRYING **************");
                    this.loadFunctionalityFactory(factoryName, false);}
                else {
                    logger.info("****************** OBR could not deploy "+factoryName+". NOT RETRYING **************");
                }
            }
        } else {
            Reason[] reasons = resolver.getUnsatisfiedRequirements();
            for (int i = 0; i < reasons.length; i++)
            {
                logger.error("Unable to resolve: " + reasons[i]);
            }
        }
    }

    private void setStatus(int status) {
        this.status = status;
        // TODO adds an AvatarStateListener ?
    }

    private void instantiateFunctionality(String factoryName, Factory funcFactory) {
        // if factory was not provided, load providing bundle from repository
        if (funcFactory == null) {
            logger.info("********************* INSTANTIATE :: Factory NULL => OBR *************************");
            logger.debug("Avatar failed to fetch functionality factory " + factoryName + ", let's try to load it from the repository");
            loadFunctionalityFactory(factoryName, true);
        }
        // only instantiate if not already done
        if (funcFactory != null && functionalitiesToInstantiate.containsKey(factoryName)) {
            logger.info("********************* INSTANTIATE :: Factory NOT NULL " +factoryName+ " *************************");

            // remove from list of root functionalities
            List<Properties> _properties = functionalitiesToInstantiate.removeAll(factoryName);

            logger.info("********************* FUNC TO INSTANTIATE :: " + functionalitiesToInstantiate.size()+" *************************");
            logger.info("********************* VS AvatarConfig :: " + configuration.getFunctionalitiesConfigurations().size()+" *************************");

            if (_properties != null && !_properties.isEmpty()) {
                // instantiate as many instances (i.e. properties.size) as necessary
                for (int i = 0; i < _properties.size(); i++) {
                    Properties properties = new Properties();
                    properties.putAll(_properties.get(i));

                    // remove unnecessary properties
                    properties.remove(AvatarConfiguration.PROPERTY_FUNCTIONALITY_FACTORY);
                    //                            properties.remove(AvatarConfiguration.PROPERTY_CONFIGURATION);
                    //                            properties.remove(AvatarConfiguration.PROPERTY_AVATAR_ID);
                    // injects the avatar ID as a service property
                    properties.put(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID, avatarId);
                    // set iPOJO instance name
                    if (properties.containsKey(Factory.INSTANCE_NAME_PROPERTY)) {
                        String newInstanceName = AsawooUtils.buildIpojoInstanceName(avatarId, properties.getProperty(Factory.INSTANCE_NAME_PROPERTY));
                        properties.put(Factory.INSTANCE_NAME_PROPERTY, newInstanceName);
                    }

                    try {
                        logger.info("Instantiate functionality " + funcFactory.getName() + " : " + properties);
                        ComponentInstance inst = funcFactory.createComponentInstance(properties);
                        logger.info("********************* INSTANTIATE :: new instance created " + inst.getInstanceName() + " *************************");
                        spawnedFunctionalities.add(inst);
                        // a factory can implement multiple functionalities
                        Collection<FunctionalityBean> implementedFunctionalities = functionalityManager.getFunctionalityByFactory(funcFactory);
                        rootFunctionalitiesNames.addAll(implementedFunctionalities.stream().map(bean -> bean.getName()).collect(Collectors.toList()));

                    } catch (UnacceptableConfiguration unacceptableConfiguration) {
                        setStatus(STATUS_ERROR);
                        unacceptableConfiguration.printStackTrace();
                    } catch (MissingHandlerException e) {
                        setStatus(STATUS_ERROR);
                        e.printStackTrace();
                    } catch (ConfigurationException e) {
                        setStatus(STATUS_ERROR);
                        e.printStackTrace();
                    }

                }
            }

            // if all capabilities have been instantiated
            if (functionalitiesToInstantiate.isEmpty()) {

                // stop listening to factory capabilities
                functionalityManager.removeFactoryListener(capabilityFactoryListener);

                // start listening to composite functionalities factories
                compositeFuncFactoryListener = new CollectionListener<Factory>() {
                    @Override
                    public void onAdd(Factory factory) {
                        logger.info("********************* COMPOSITE :: Factory added *************************");
                        logger.info("Factory was just bound, check if avatar can use it : " + factory.getName());
                        // A functionality factory was just bound.

                        // check that the factory is not a capability
                        Collection<FunctionalityBean> implementedFunctionalities = functionalityManager.getFunctionalityByFactory(factory);

                        implementedFunctionalities.stream().forEach(funcBean -> {
                            String functionalityName = funcBean.getName();
                            if (rootFunctionalitiesNames.contains(functionalityName) || compositeFunctionalitiesNames.contains(functionalityName)) {
                                // capability or composite functionality already instantiated => exit
                                return;
                            } else {
                                // instantiate this functionality
                                instantiateCompositefunctionality(factory, implementedFunctionalities);
                            }
                        });

                    }

                    @Override
                    public void onRemove(Factory factory) {

                    }

                    @Override
                    public void onUpdate(Factory factory) {

                    }
                };

                functionalityManager.addFactoryListener(compositeFuncFactoryListener);

                // check for composite functionalities
                finalizeAvatar();
            }


        }
    }

    private void instantiateCompositefunctionality(Factory factory, Collection<FunctionalityBean> functionalities) {

        Properties properties = new Properties();
        properties.put(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID, avatarId);
        try {
            ComponentInstance inst = factory.createComponentInstance(properties);
            spawnedFunctionalities.add(inst);
            compositeFunctionalitiesNames.addAll(functionalities.stream().map(bean -> bean.getName()).collect(Collectors.toList()));
            logger.info("Spawned a new composite functionality " + inst.getInstanceName());

            // then re-finalize the avatar in case a new composite functionality is available
            finalizeAvatar();

        } catch (UnacceptableConfiguration unacceptableConfiguration) {
            setStatus(STATUS_ERROR);
            unacceptableConfiguration.printStackTrace();
        } catch (MissingHandlerException e) {
            setStatus(STATUS_ERROR);
            e.printStackTrace();
        } catch (ConfigurationException e) {
            setStatus(STATUS_ERROR);
            e.printStackTrace();
        }

    }

    private void finalizeAvatar() {
        // update status if all functionality instances described in the configuration have been instantiated
        // TODO FIXME double check is useless
        logger.info("********************* FINALIZE  *************************");
        if (spawnedFunctionalities.size() == configuration.getFunctionalitiesConfigurations().keys().size()) {
            logger.info("********************* FINALIZE :: Status READY " +avatarId+ " *************************");
            setStatus(STATUS_READY);
        }

        // FIXME: do not even need to have all atomic functionalities available before trying to get composite functionalities
        // FIXME: too soon ? are avatar's functionalities already instantiated in the reasoner ?
        // when avatar and its atomic functionalities are ready, instantiate composite functionalities

        Collection<FunctionalityBean> availableFuncs = functionalityManager.getAvailableFunctionalities(avatarId);
        if (availableFuncs == null) {
            logger.error("getAvailableFunctionalities returned null : it should not be possible !");
            return;
        }
        logger.info("********** Available funcs BEFORE collabo " + availableFuncs);

        // Add collaborative functionalities attributed to this avatar
        if (collaborativeFunctionalityManager != null) {
            Collection<FunctionalityBean> collaborativeFuncs = collaborativeFunctionalityManager.getAvailableCollaborativeFunctionalities(avatarId);
            availableFuncs.addAll(collaborativeFuncs);
        }

        logger.info("********** Available funcs AFTER collabo " + availableFuncs);

        logger.info("Finalize avatar with available functionalities " + availableFuncs);
        int functionalityInstancesCount = spawnedFunctionalities.size();
        for (FunctionalityBean f : availableFuncs) {
            String functionalityName = f.getName();
            if (!rootFunctionalitiesNames.contains(functionalityName)
                    && !compositeFunctionalitiesNames.contains(functionalityName)) {
                logger.info("Available functionality " + f.getName());
                Collection<Factory> compositeFuncFactories = functionalityManager.getImplementingFactories(functionalityName);
                // FIXME just need one... how to select it ?
                // Note Alban : Is there any sense in having multiple implementations of a composite functionality ?
                // I don't think so.
                if (! compositeFuncFactories.isEmpty()) {
                    Factory factory = compositeFuncFactories.iterator().next();
                    Properties properties = new Properties();
                    properties.put(AvatarConfiguration.SERVICE_PROPERTY_AVATAR_ID, avatarId);
                    try {
                        ComponentInstance inst = factory.createComponentInstance(properties);
                        spawnedFunctionalities.add(inst);
                        compositeFunctionalitiesNames.addAll(functionalityManager.getFunctionalityByFactory(factory).stream().map(bean -> bean.getName()).collect(Collectors.toList()));
                        logger.info("Spawned a new composite functionality " + inst.getInstanceName());
                    } catch (UnacceptableConfiguration unacceptableConfiguration) {
                        setStatus(STATUS_ERROR);
                        unacceptableConfiguration.printStackTrace();
                    } catch (MissingHandlerException e) {
                        setStatus(STATUS_ERROR);
                        e.printStackTrace();
                    } catch (ConfigurationException e) {
                        setStatus(STATUS_ERROR);
                        e.printStackTrace();
                    }
                }
                /*
                compositeFuncFactories.forEach(factory -> {
                    // logger.info("Available functionality factory " + factory.getName());

                });*/
            }
        }

        // If a functionality was spawned we have to try again. A next level composite functionality might be available.
        if (spawnedFunctionalities.size() > functionalityInstancesCount) {
            logger.info("************* "+spawnedFunctionalities.size()+" > "+functionalityInstancesCount+" RE-FINALIZE **********");
            // System.out.println("Recursive finalizeAvatar when a functionality was spawned");
            this.finalizeAvatar();
        }

//        FIXME: remove ? setAvatarUri();
    }

}

package asawoo.services.ibrdtnadapter.server;

import asawoo.services.http.client.VertxHttpClient;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.HttpRequestWrapper;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpServerResponse;
import org.apache.felix.ipojo.annotations.Requires;
import org.ibrdtnapi.BundleHandler;
import org.ibrdtnapi.entities.Bundle;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * All received bundles (request **and** responses) are processed
 * by this handler.
 * 
 * Each node keeps track of the request it sends by storing in a Map
 * the HttpServletResponse responses objects (as a value) matched with
 * the MD5 digest of the serialized request (see {@link BpAppSingleton}
 * for more details.
 * 
 * When a message is received the MD5 digest is retrieved from the content.
 * If the MD5 digest does not match with a key of the Map:
 * 		-	The message is a request. The request is deserialized
 * 			and executed, the response gathered, serialized and
 * 			sent back to the source with the same MD5 digest.
 * If the MD5 digest matches with a key of the Map:
 * 		- 	The message is a response. The response is deserialized
 * 			and written into the object (value of the Map)
 * 
 * @author auzias
 */
public class ServerHandler implements BundleHandler {
   @Requires
    private VertxHttpClient vertxHttpClient;

	@Override
	public void onReceive(Bundle bundle) {
		//## First the message needs to be identified as a request or a response:
		//### Extract MD5 digest
		final int digestLength;
		try {
			digestLength = MessageDigest.getInstance("MD5").getDigestLength();

			byte[] md5digest = this.getDigest(bundle.getDecoded(0), digestLength);//new byte[digestLength];
			if (md5digest == null) System.err.println("MD5 digest could not be retrieved! An error occurred ");
			//### Extract content
			byte[] content = this.getContent(bundle.getDecoded(0), digestLength);//new byte[digestLength];
			if (content == null) System.err.println("Content could not be retrieved! An error occurred ");

			//### Check if the message is a request or a response
			HttpServerResponse response = BpAppSingleton.getResponse(md5digest);
			if (response == null) {
		//## Then, if the message is a request; process it:
				this.requestsAndReplies(bundle.getDecoded(0), bundle.getSource(), bundle.getFlags());
			} else {
		//## Else, if the message is a response; process it:
				this.processResponse(md5digest, content, response);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	private void processResponse(final byte[] md5digest, final byte[] content, HttpServerResponse response) {
        try {
            String httpResponse = DTNUtils.<String>deserialize(content);
            response.write(httpResponse);
            // TODO or call local callback URI
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
	}

	private void requestsAndReplies(final byte[] decoded, final String destination, final int flags) {
		try {
			HttpRequestWrapper requestWrapper = DTNUtils.<HttpRequestWrapper>deserialize(decoded);
            vertxHttpClient.executeRequest(requestWrapper.getMetadata().getHttpMethod(),
											requestWrapper.getMetadata().getDtnUri(),
											requestWrapper.getRequestParameters(),
											destination,
											new ResponseHandler(destination, flags));
		} catch (ClassNotFoundException e) {
			System.err.println("The class <HttpRequestWrapper> cannot be found: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("The deserialization rose an error: " + e.getMessage());
		}
	}

	private byte[] getContent(byte[] decoded, int digestLength) {
		byte[] content = null;
		final int size = decoded.length - digestLength;
		content = new byte[size];

		for(int i = 0; i < size; i++) {
			content[i] = decoded[digestLength+i];
		}

		return content;
	}

	private byte[] getDigest(final byte[] decoded, final int digestLength) {
		byte[] digest = new byte[digestLength];

		for(int i = 0; i < digestLength; i++) {
			digest[i] = decoded[i];
		}

		return digest;
	}

    private class ResponseHandler implements Handler<HttpClientResponse> {
        private final String destination;
        private final int flags;

        public ResponseHandler(String destination, int flags) {
			this.destination = destination;
			this.flags = flags;
		}

		@Override
        public void handle(HttpClientResponse response) {
            response.bodyHandler(buffer -> {
                byte[] msgContent = buffer.getBytes();

				Bundle bundle = new Bundle(this.destination, msgContent);
				bundle.setFlags(this.flags);
				BpAppSingleton.getInstance().send(bundle);
            });
        }
    }
}

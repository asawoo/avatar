package asawoo.services.ibrdtnadapter.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import io.vertx.core.http.HttpServerResponse;
import org.ibrdtnapi.BpApplication;

/**
 * This class enables the communication with the IBR-DTN daemon
 * though its local textual TCP API.
 * 
 * Each node runs a single instance (hence the singleton pattern).
 * The EID being: "dtn://$hostname/.asawoo" see the configuration
 * file for more details (not included nor documented here. Note also
 * that the daemon must be started beforehand on the host).
 * 
 * This class also offer a mean to store HttpServletResponse responses objects.
 * Theses objects are passed when a request need to be sent. The request, once
 * serialized, is identified by the MD5 digest of this serialization. The digest
 * (fixed-length hash) is added at the beginning of the payload of the message.
 * The server also prepends this same hash in the reply, so the client can use
 * this hash as a key of a Map to get the value stored beforehand. 
 * 
 * @author auzias
 *
 */
public class BpAppSingleton {
	@SuppressWarnings("unused")
	private static BpAppSingleton instance = null;
	public static final String EID = ".asawoo";
	private static BpApplication bpApp = null;
	private static Map<byte[], HttpServerResponse> responses = null;

	private BpAppSingleton() {
		//One sole unique EID is set per ASAWoO node.
		//This EID process then the request made to the different endpoint.
		BpAppSingleton.bpApp = new BpApplication(BpAppSingleton.EID);
		ServerHandler sh = new ServerHandler();
		BpAppSingleton.bpApp.setHandler(sh);
		BpAppSingleton.responses = new HashMap<byte[], HttpServerResponse>();
	}

	public static BpApplication getInstance() {
		if(BpAppSingleton.bpApp == null) {
			BpAppSingleton.instance = new BpAppSingleton();
		}
		return BpAppSingleton.bpApp;
	}

	/**
	 * 
	 * @param key md5digest of the HttpRequestWrapper serialized Object
	 * (through DTNUtils.<>serialize(Object))
	 * @param value the HttpServletResponse object that needs to be stored
	 * for further usage (further being when the response will be received)
	 */
	public static void add(byte[] key, HttpServerResponse value) {
		BpAppSingleton.responses.put(key, value);
	}

	/**
	 * Method used to get the object response matching with the
	 * just-received-message that embeds the waited response.
	 * 
	 * @param key md5digest of the HttpRequestWrapper serialized Object
	 * (through DTNUtils.<>serialize(Object))
	 * @return the HttpServletResponse previously stored matching the key
	 */
	public static HttpServerResponse getResponse(byte[] key) {
		if (key == null) return null;

		HttpServerResponse ret = BpAppSingleton.responses.get(key);
		if (ret != null) {
			BpAppSingleton.responses.remove(key);
		}
		return ret;
	}
}

package asawoo.services.ibrdtnadapter.client;

import asawoo.services.ibrdtnadapter.server.BpAppSingleton;
import asawoo.services.net.dtn.exception.DTNUnsupportedComModeException;
import asawoo.services.net.dtn.service.DTNAdapter;
import asawoo.services.net.dtn.service.DTNNeighborDiscoveryListener;
import asawoo.services.net.dtn.util.CommunicationMode;
import asawoo.services.net.dtn.util.DTNUtils;
import asawoo.services.net.dtn.util.http.DTNRequestMetadata;
import asawoo.services.net.dtn.util.http.HttpRequestWrapper;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Validate;
import org.ibrdtnapi.entities.Bundle;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.EnumSet;
import java.util.Map;

@Component
@Instantiate
@Provides
public class ClientAdapter implements DTNAdapter {
	private static final String MD5 = "MD5";
	private final String frameworkName = "IBR-DTN";
    private String nodeId = null;

	@Validate
    public void start() {
		// Ensure that IBR-DTN is started and a Java client is running
		BpAppSingleton.getInstance(); 
	}

	@Override
	public String forwardRequest(DTNRequestMetadata metadata, MultiMap queryParams, String requestBody, HttpServerResponse response) throws DTNUnsupportedComModeException {
		CommunicationMode comMode = DTNUtils.getCommunicationModeFromMetadata(metadata);
		//DTN URI in the form of: dtn://hostname/.asawoo
		//						.asawoo being BpAppSingleton.EID
		final String destination = "dtn://" +  metadata.getDtnUri().getHost() + "/" + BpAppSingleton.EID;
		// encapsulate metadata and parameters in a C3PO message object
		HttpRequestWrapper requestObject = new HttpRequestWrapper(metadata, DTNUtils.multiMapToSimpleMap(queryParams), requestBody);
		byte[] payload = null;
		byte[] contentMD5Digest = null;
		try {
			byte[] content = DTNUtils.<HttpRequestWrapper>serialize(requestObject);
			contentMD5Digest = MessageDigest.getInstance(ClientAdapter.MD5).digest(content);
			payload = this.concat(contentMD5Digest, content);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			//Should never append (unless the MD5 is took off the jre or someone change the static final String)
			System.err.println("The algorithm " + ClientAdapter.MD5 + " does not exist!" + e.getMessage());
		}
		Bundle bundle = new Bundle(destination, payload);
		switch(comMode) {
			case UNICAST:
				this.sendRequest(bundle, contentMD5Digest, response);
				break;
			case BROADCAST:
				new DTNUnsupportedComModeException(comMode.name()+" is not supported **yet** by "+getDTNFrameworkName()+" DTN framework");
				//TODO create a singleton listening to dtn://bcast/.asawoo
				break;
			case ANYCAST:
			case MULTICAST:
			default:
				throw new DTNUnsupportedComModeException(comMode.name()+" is not supported by "+getDTNFrameworkName()+" DTN framework");
		}

		// TODO store handler for callback too ?

		return bundle.getSource()+":"+bundle.getDestination()+"@"+bundle.getTimestamp();
	}

	private void sendRequest(final Bundle bundle, final byte[] contentMD5Digest, HttpServerResponse response) {
		BpAppSingleton.getInstance().send(bundle);
		BpAppSingleton.add(contentMD5Digest, response);
	}

	@Override
	public void forwardRequest(DTNRequestMetadata metadata, Map<String, String[]> requestParams, HttpServletResponse response) throws DTNUnsupportedComModeException {
		//DTN URI in the form of: dtn://hostname/.asawoo
		//						.asawoo being BpAppSingleton.EID
//		final String destination = "dtn://" +  metadata.getDtnUri().getHost() + "/" + BpAppSingleton.EID;
//		HttpRequestWrapper requestObject = new HttpRequestWrapper(metadata, requestParams, null /* TODO : HTTP request body */);
//		System.out.println(":: " + destination);
//		try {
//			byte[] content = DTNUtils.<HttpRequestWrapper>serialize(requestObject);
//			byte[] contentMD5Digest = MessageDigest.getInstance("MD5").digest(content);
//			byte[] payload = this.concat(contentMD5Digest, content);
//			Bundle bundle = new Bundle(destination, payload);
//			BpAppSingleton.getInstance().send(bundle);
//			BpAppSingleton.add(contentMD5Digest, response);
//		} catch (IOException e) {
//			System.err.println("IO exception while serialization: " + e.getMessage());
//		} catch (NoSuchAlgorithmException e) {
//			System.err.println("MD5 digest algorithm does not exist: " + e.getMessage());
//		}
	}

	@Override
	public void registerMulticastSubscriber(String topic) {
		// TODO implement
	}

	@Override
	public void unregisterMulticastSubscriber(String topic) {
		// TODO implement
	}

	@Override
	public void addNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener) {
		// TODO
	}

	@Override
	public void removeNeighborDiscoveryListener(DTNNeighborDiscoveryListener listener) {
		// TODO
	}

	private byte[] concat(byte[] contentMD5Digest, byte[] content) {
		final int length = contentMD5Digest.length + content.length;

		byte[] result = new byte[length];
		int pos = 0;
		for (byte element : contentMD5Digest) {
			result[pos] = element;
			pos++;
		}
		for (byte element : content) {
			result[pos] = element;
			pos++;
		}
		return result;
	}

	@Override
	public String getDTNFrameworkName() {
		return this.frameworkName;
	}

	@Override
	public EnumSet<CommunicationMode> getSupportedCommunicationModes() {
		return EnumSet.of ( CommunicationMode.UNICAST,
							CommunicationMode.MULTICAST,
							CommunicationMode.ANYCAST);
	}

	@Override
	public String getNodeId() {
		return this.nodeId;
	}

	@Override
	public void setNodeId(String nodeId) {
		this.nodeId = new String(nodeId);
	}

	@Override
	public void purgeMessage(String messageId) {
		// TODO
	}

	@Override
	public String getHost() {
		return nodeId;
	}
}

/**
* Utility functions that provide basic ASAWoO features
*/

window.asawooFunctionalities = {};

/**
 * Call this function at startup to retrieve functionalities URIs
 * @param functionalityName
 */
function initFunctionalities(functionalityName) {
    var url = "/registry/"+functionalityName;


     var xmlhttp = new XMLHttpRequest();
     xmlhttp.onreadystatechange = function(event) {
        if(this.readyState === 4 && this.status === 200){
            var functionalities = JSON.parse(this.responseText);
            window.asawooFunctionalities[functionalityName] = functionalities;
        }
     }

     xmlhttp.open("GET", url);
     xmlhttp.send(null);

}

/**
 * Browses through initialized functionalities to get the method full URI
 */
function getMethodUri(functionalityName, method, instanceName) {
    var funcs = window.asawooFunctionalities[functionalityName];
    var uri = '';
    for (var i=0; i < funcs.length; i++) {
        if (funcs[i].instanceName == instanceName) {
            uri = getUrl(funcs[i].baseUri, funcs[i].instanceName, funcs[i].methods[method].url);
            break;
        }

        // TODO should we stop at the first encountered ?
        // TODO add a predicate/filter (e.g., for the led id {led.id='1'} ou sur instanceName);
    }
    if (uri == '') {
        uri = getUrl(funcs[0].baseUri, funcs[0].instanceName, funcs[0].methods[method].url);
    }
    return uri;


}



function getUrl(baseUri, instanceName, methodPath) {
    return baseUri+"/"+instanceName+methodPath;
}

/**
 * Sends a HTTP request to the URL with a json message
 * @param url the address
 * @param verb the HTTP type
 * @param body the json message
 */
function callUrl(url, verb, body){
     var xmlhttp = new XMLHttpRequest();
     xmlhttp.open(verb, url);
     if (body != null) {
          xmlhttp.setRequestHeader("Content-Type", "application/json");
     }
     xmlhttp.send(body);
}
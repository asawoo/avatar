package asawoo.services.wotapp;

import asawoo.core.config.AsawooConfiguration;
import asawoo.core.functionality.FunctionalityRequest;
import asawoo.core.net.rest.RestServiceRegistry;
import asawoo.core.util.AsawooUtils;
import asawoo.core.wotapp.WoTApp;
import asawoo.core.wotapp.WoTAppBean;
import asawoo.core.wotapp.WoTAppManager;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import org.apache.felix.bundlerepository.*;
import org.apache.felix.ipojo.annotations.*;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * The WoTAppManager listens to wotapps being bound/unbound, copies their files,
 * exposes them using vert.x routes, maintains a list of active wotapps and also expose this list in a HTTP route
 *
 * Created by alban on 18/05/17.
 */
@Component
@Instantiate
@Provides
public class WoTAppManagerImpl implements WoTAppManager {

    private static final String WEBROOT_BUNDLE_LOCATION = "WEB-INF";
    private static final String INDEX_FILENAME = "index.html";
    private static final String[] SRC_DIRS = new String[]{"css", "js", "assets", "dist"};
    private static final String PATH_WOTAPPS = "/wotapps/";
    private static final String STATIC_DIR = "static";


    private HashMap<String, WoTApp> wotapps;
    private HashMap<String, Route> webRoutes;
    private HashMap<String, File> wotappDirs;

    @Requires
    private Router router;

    @Requires
    private Vertx vertx;

    @Requires(optional = true, proxy = false)
    private RepositoryAdmin repositoryAdmin;

    @Requires
    private RestServiceRegistry restRegistry;

    @Context
    private BundleContext bundleContext;

    private Logger logger = LoggerFactory.getLogger(WoTAppManagerImpl.class);

    public WoTAppManagerImpl() {
        this.wotapps = new HashMap<String, WoTApp>();
        this.webRoutes = new HashMap<String, Route>();
        this.wotappDirs = new HashMap<String, File>();
    }

    @Override
    public WoTAppBean getWotapp(String wotappId) {
        if (wotapps.containsKey(wotappId)) {
            WoTApp wa = wotapps.get(wotappId);
            return new WoTAppBean(wa.getId(), wa.getDescription(), getWotappUrl(wa.getId()), true);
        } else {
            return null;
        }

    }

    @Override
    public File getWotappDir(String wotappId) {
        return wotappDirs.get(wotappId);
    }

    @Override
    public Collection<WoTAppBean> geWotapps() {
        return Collections2.transform(wotapps.values(), new Function<WoTApp, WoTAppBean>() {
            @Override
            public WoTAppBean apply(WoTApp input) {
                return new WoTAppBean(input.getId(), input.getDescription(), getWotappUrl(input.getId()), true);
            }
        });
    }

    @Override
    public Collection<WoTAppBean> getAvailableWotapps() {

        Requirement requirement = repositoryAdmin.getHelper().requirement(WoTApp.ASAWOO_WOTAPP_NAMESPACE,
                "("+ WoTApp.ASAWOO_WOTAPP_ID_PROPERTY +"=*)");

        Resource[] resources = new Resource[0];
        try {
            resources = repositoryAdmin.discoverResources(new Requirement[] { (Requirement)requirement });
        } catch(Exception e) {
            logger.error("Failed to contact market place while listing wotapps " + e.getMessage());
        }

        List<WoTAppBean> availableWotapps = new ArrayList<WoTAppBean>();

        wotapps.keySet().forEach(key -> {
            WoTApp wotapp = wotapps.get(key);
            availableWotapps.add(new WoTAppBean(key, wotapp.getDescription(), getWotappUrl(key), true, null));
        });

        Arrays.stream(resources).forEach(r -> {

            // REQUIREMENTS

            // FIXME: Kinda hackish way of getting required functionalities of a wotapp. A better way ?
            // FIXME (LT): this would not work for a complex LDAP filter. If this is for display purpose, can't we just display the raw filter ?
            Set<String> requiredFunctionalities = Sets.newHashSet();
            Arrays.stream(r.getRequirements()).forEach(req -> {
                if (req.getName().equals(AsawooUtils.ASAWOO_FUNCTIONALITY_NAMESPACE)) {
                    requiredFunctionalities.add(req.getFilter()
                            .replace("("+AsawooUtils.ASAWOO_FUNCTIONALITY_NAME_FILTER_PROPERTY+"=", "")
                            .replace(")", ""));
                }
            });

            // CAPABILITIES
            Capability capability = Arrays.stream(r.getCapabilities())
                    .filter(c -> c.getName().equals(WoTApp.ASAWOO_WOTAPP_NAMESPACE))
                    .findFirst().get();


            Map<String, Object> wotappProperties = capability.getPropertiesAsMap();
            String id = (String) wotappProperties.get(WoTApp.ASAWOO_WOTAPP_ID_PROPERTY);
            if (wotapps.containsKey(id)) return;
            String description = (String) wotappProperties.get(WoTApp.ASAWOO_WOTAPP_DESCRIPTION_PROPERTY);
            WoTAppBean wotapp = new WoTAppBean(id, description, null, false, requiredFunctionalities);
            availableWotapps.add(wotapp);
        });

        return availableWotapps;

    }


    @Override
    public void loadWotappBundle(String id) throws NoSuchElementException, RuntimeException, InterruptedException {
        Requirement requirement = repositoryAdmin.getHelper().requirement(WoTApp.ASAWOO_WOTAPP_NAMESPACE,
                "("+ WoTApp.ASAWOO_WOTAPP_ID_PROPERTY +"=" + id + ")");
        Resource[] resources = repositoryAdmin.discoverResources(new Requirement[] { (Requirement)requirement });
        if (resources == null || resources.length == 0) {
            throw new NoSuchElementException("Wotapp unknown : " + id);
        }

        Resolver resolver = repositoryAdmin.resolver();
        resolver.add(resources[0]);
        if (resolver.resolve()) {
            logger.info("Deploy a bundle to load Wotapp code: " + id);
            resolver.deploy(Resolver.START);
            int waitedTime = 0;
            while(this.wotapps.get(id) == null && waitedTime < 120000) {
                waitedTime += 100;
                Thread.sleep(100);
            }
        } else {
            Reason[] reasons = resolver.getUnsatisfiedRequirements();
            String msg = "Failure to load wotapp bundle: " + Arrays.toString(Arrays.stream(reasons).map(r -> r.getRequirement().toString()).toArray());
            logger.error(msg);
            throw new RuntimeException(msg);
        }
    }

    @Override
    public void unloadWotappBundle(String id) throws BundleException {
        this.wotapps.get(id).getBundleContext().getBundle().uninstall();
    }


    @Validate
    private void start() throws IOException {
        logger.info("Starts the WoTApp manager");

        // Expose a /wotapps route returning a simple json list of application
        // TODO: Should this json respect a format ?
        router.get("/wotapps").handler(routingContext -> {


            // TODO FIXME : return deployed wotapps here (different from market, i.e., available ?)

            // Get the available wotapps list from code repo

            vertx.<Collection<WoTAppBean>>executeBlocking(future -> {
                Collection<WoTAppBean> availableWotapps = getAvailableWotapps();
                future.complete(availableWotapps);
            }, false, res -> {
                if (res.failed()) {
                    routingContext.response().setStatusCode(500).end(res.cause().getMessage());
                } else {
                    String wotappsJSON = Json.encodePrettily(res.result());
                    routingContext.response()
                            .setStatusCode(HttpResponseStatus.OK.code())
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .end(wotappsJSON);
                }
            });

        });

        // Used by the admin wotapp status page to check availability of the wotapps from the marketplace
        router.get("/wotappsstatus").handler(routingContext -> {
            vertx.executeBlocking(future -> {
                Requirement requirement = repositoryAdmin.getHelper().requirement(WoTApp.ASAWOO_WOTAPP_NAMESPACE,
                        "("+ WoTApp.ASAWOO_WOTAPP_ID_PROPERTY +"=*)");

                try {
                    Resource[] resources = repositoryAdmin.discoverResources(new Requirement[] { (Requirement)requirement });
                    Resolver resolver = repositoryAdmin.resolver();
                    for(int i = 0; i < resources.length; i++) {
                        resolver.add(resources[i]);
                    }
                    if (resources.length == 0) {
                        future.fail(new Error("No applications listed"));
                    } else if (resolver.resolve()) {
                        future.complete();
                    } else {
                        future.fail(new Error(resolver.getUnsatisfiedRequirements()[0].getRequirement().toString()));
                    }

                } catch(Exception e) {
                    future.fail(e);
                }
            }, false, res -> {
                if (res.failed()) {
                    routingContext.response().setStatusCode(500).end(res.cause().getMessage());
                } else {
                    routingContext.response().setStatusCode(HttpResponseStatus.OK.code()).end();
                }
            });
        });

        router.patch(PATH_WOTAPPS+":id").handler(routingContext -> {
            String id = routingContext.request().getParam(WoTApp.ASAWOO_WOTAPP_ID_PROPERTY);
            System.out.println("Wotapp activation route " + id);
            JsonObject body = routingContext.getBodyAsJson();
            Boolean active = body.getBoolean(WoTApp.ASAWOO_WOTAPP_ACTIVE_PROPERTY);
            vertx.executeBlocking(future -> {
                try {
                    if (active) {
                        loadWotappBundle(id);
                    } else {
                        unloadWotappBundle(id);
                    }
                    future.complete();
                } catch(Exception e) {
                    future.fail(e);
                }
            }, false, res -> {
                if (res.failed()) {
                    if (res.cause().getClass() == NoSuchElementException.class) {
                        routingContext.response().setStatusCode(404).end(res.cause().getMessage());
                    } else {
                        routingContext.response().setStatusCode(500).end(res.cause().getMessage());
                    }
                } else {
                    routingContext.response().setStatusCode(HttpResponseStatus.OK.code()).end();
                }
            });
        });

        // register these routes
        registerRoute(PATH_WOTAPPS, HttpMethod.GET.name(), null);
        registerRoute(PATH_WOTAPPS+":id", HttpMethod.PATCH.name(), Arrays.asList(WoTApp.ASAWOO_WOTAPP_ACTIVE_PROPERTY));

        addStaticResources();

    }

    @Invalidate
    private void stop() {
        logger.info("WoTApp manager is stopping");
        // unregister system routes
        restRegistry.unregisterSystemRoute(WoTApp.ASAWOO_WOTAPP_NAMESPACE, null);

        removeStaticResources();
    }

    @Bind(optional = true, aggregate = true)
    private void bindWoTApp(WoTApp wotapp) throws IOException {

        if (repositoryAdmin == null) {
            logger.error("Trying to use OBR repository admin to list wotapps, but it was not available");
            return;
        }

        String id = wotapp.getId();

        if (wotapps.containsKey(id)) {
            throw new RuntimeException("A WoTapp with the same ID is already deployed [ID="+id+"]");
        } else if (Arrays.asList(AsawooConfiguration.RESERVED_WORDS).contains(id)) {
            throw new RuntimeException("WoTapp ID: "+id+" is forbidden as it is a reserved word");
        }

        logger.info("A WoTApp was bound: " + id);
        this.wotapps.put(id, wotapp);
        File wotappDir = createWotAppDir(id);
        this.wotappDirs.put(id, wotappDir);
        copyWebResources(wotappDir, wotapp.getBundleContext());
        Route staticWebRoute = router.get(getWotappUrl(id) + "/*").handler(StaticHandler.create()
                .setWebRoot(wotappDir.getPath())
                .setIndexPage(INDEX_FILENAME)
        );

        // Special case for the "admin" wotapp if it exists, the root of the web server (/)
        // is redirected toward it
        // FIXME: use a config file ? Some cleaner mechanism ?
        if (id == "admin") {
            router.get("/").handler(routingContext -> {
               routingContext.response()
                       .setStatusCode(HttpResponseStatus.FOUND.code())
                       .putHeader("Location", getWotappUrl(id) + "/")
                       .end();
            });
        }
        this.webRoutes.put(id, staticWebRoute);

        registerRoute(staticWebRoute.getPath(), HttpMethod.GET.name(), null);
    }

    @Unbind
    private void unbindWoTApp(WoTApp wotapp) {
        String id = wotapp.getId();
        System.out.println("A WoTApp was unbound: " + id);
        // unplug web route
        Route waRoute = this.webRoutes.get(id).remove();
        restRegistry.unregisterSystemRoute(WoTApp.ASAWOO_WOTAPP_NAMESPACE, waRoute.getPath());

        // delete directory
        FileSystem fileSystem = vertx.fileSystem();
        fileSystem.deleteRecursiveBlocking(this.wotappDirs.get(id).getPath(), true);

        this.wotapps.remove(id);
        this.wotappDirs.remove(id);
        this.webRoutes.remove(id);

    }

    /**
     * add static resources
     */
    private void addStaticResources() throws IOException {
        File wotappDir = createWotAppDir(STATIC_DIR);
        this.wotappDirs.put(STATIC_DIR, wotappDir);
        copyWebResources(wotappDir, bundleContext);
        Route staticWebRoute = router.get("/" + STATIC_DIR + "/*").handler(StaticHandler.create()
                        .setWebRoot(wotappDir.getPath()));
        this.webRoutes.put(STATIC_DIR, staticWebRoute);
    }

    /**
     * remove static resources mapping
     */
    private void removeStaticResources() {
        this.wotappDirs.remove(STATIC_DIR);
        this.webRoutes.remove(STATIC_DIR);
    }

    private String getWotappUrl(String wotappId) {
        return PATH_WOTAPPS + wotappId;
    }

    private void registerRoute(String path, String httpMethod, List<String> parameters) {
        restRegistry.registerSystemRoute(WoTApp.ASAWOO_WOTAPP_NAMESPACE,
                new FunctionalityRequest(path, httpMethod, parameters));
    }

    /*
    private ArrayList<HashMap<String, Object>> getAvailableWotapps() {

    }*/

    private File createWotAppDir(String wotappId) {
        FileSystem fileSystem = vertx.fileSystem();

        File wotappDir = new File(AsawooConfiguration.getWebrootDir(), wotappId);
        if (!wotappDir.exists()) {
            fileSystem.mkdirsBlocking(wotappDir.getPath());
        }
        return wotappDir;
    }


    private void copyWebResources(File wotappDir, BundleContext bundleContext) throws IOException {
        // TODO: replace copying a defined list of files with a recursive copy of a whole directory ?

        // index
        URL indexResource = bundleContext.getBundle().getResource(WEBROOT_BUNDLE_LOCATION+"/"+INDEX_FILENAME);
        if (indexResource != null) {
            File indexFile = new File(wotappDir, INDEX_FILENAME);
            Files.copy(indexResource.openStream(),
                    indexFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }


        for (String srcDirStr : SRC_DIRS) {
            File srcDir = new File(wotappDir, srcDirStr);
            if (!srcDir.exists()) {
                vertx.fileSystem().mkdirsBlocking(srcDir.getPath());
            }
            Enumeration<URL> srcEntries = bundleContext.getBundle().findEntries(WEBROOT_BUNDLE_LOCATION+"/" + srcDirStr, "*", true /* recursive */);
            if (srcEntries != null) {
                while (srcEntries.hasMoreElements()) {
                    URL fileUrl = srcEntries.nextElement();
                    String filename = fileUrl.getPath();
                    File srcFile = new File(srcDir, filename.substring(filename.lastIndexOf('/') + 1));
                    // logger.info("Copy file " + fileUrl + " to " + srcFile.toPath());
                    try {
                        Files.copy(fileUrl.openStream(), srcFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch(Exception e) {
                        // FIXME: happens with directories, it should be possible to prevent it
                    }
                }
            }
        }
    }

}

package asawoo.services.deviceconfiguration;

import asawoo.core.avatar.AvatarFactory;
import asawoo.core.config.AvatarConfiguration;
import asawoo.core.util.AsawooUtils;
import asawoo.services.semanticrepo.SemanticRepo;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import org.apache.felix.bundlerepository.*;
import org.apache.felix.ipojo.annotations.*;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import javax.servlet.UnavailableException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
@Provides
@Instantiate
public class DeviceConfigurationManager {

	@Requires
	SemanticRepo semanticRepo;

	@Requires
	private Router router;

	@Requires
	private Vertx vertx;

	@Requires
	private AvatarFactory avatarFactory;

	@Requires(optional = true, proxy = false)
	private RepositoryAdmin repositoryAdmin;

	private Property ssnImplements = ResourceFactory.createProperty("http://www.w3.org/ns/ssn/","implements");
	private Resource ssnSystem = ResourceFactory.createResource("http://www.w3.org/ns/ssn/System");

	private static final String SPARQL_LIST_DEVICES = "" +
			"	 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
			"    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
			"    PREFIX ssn: <http://www.w3.org/ns/ssn/>\n" +
			"    PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
			"    PREFIX asawoo-c: <http://liris.cnrs.fr/asawoo/capabilities#>\n" +
			"    PREFIX asawoo-d: <http://liris.cnrs.fr/asawoo/devices#>\n" +
			"    PREFIX asawoo-f: <http://liris.cnrs.fr/asawoo/functionalities#>\n" +
			"    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>\n" +
			"\n" +
			"    SELECT *\n" +
			"    FROM <http://liris.cnrs.fr/asawoo/devices>\n" +
			"    FROM <http://liris.cnrs.fr/asawoo/capabilities>\n" +
			"    WHERE {\n" +
			"      ?device rdf:type ssn:System .\n" +
			"      OPTIONAL { ?device rdfs:label ?deviceLabel }\n" +
			"      OPTIONAL {\n" +
			"        ?device ssn:implements ?capability .\n" +
			"        ?capability rdf:type ?capabilityClass .\n" +
			"        OPTIONAL {\n" +
			"          ?capability ?property ?propertyValue .\n" +
			"          ?property rdf:type owl:ObjectProperty\n" +
			"        }\n" +
			"      }\n" +
			"    }";

	private Logger logger = LoggerFactory.getLogger(DeviceConfigurationManager.class);

	/*
	 * Return the sparql result containing device configuration(s) as a jena model
	 * FIXME: better implementation with a "construct" sparql query
	 */
	private Model getModelFromResults(ResultSet results, String deviceURI) {
		Model items = ModelFactory.createDefaultModel();
		items.setNsPrefix("ssn", "http://www.w3.org/ns/ssn/");
		items.setNsPrefix("asawoo-d", "http://liris.cnrs.fr/asawoo/devices#");
		items.setNsPrefix("asawoo-c", "http://liris.cnrs.fr/asawoo/capabilities#");
		while(results.hasNext()) {
			QuerySolution solution = results.nextSolution();
			RDFNode deviceNode = solution.get("device");
			if (deviceNode == null && deviceURI == null) {
				continue;
			}

			Resource deviceResource;
			if (deviceNode == null) {
				deviceResource = items.createResource(deviceURI);
			} else {
				deviceResource = items.createResource(deviceNode.asResource().getURI());
			}
			deviceResource.addProperty(RDF.type, ssnSystem);
			RDFNode deviceLabelNode = solution.get("deviceLabel");
			if (deviceLabelNode != null) {
				deviceResource.addProperty(RDFS.label, deviceLabelNode.asLiteral());
			}
			RDFNode capabilityNode = solution.get("capability");
			if (capabilityNode != null) {
				Resource capabilityResource = items.createResource(capabilityNode.asResource().getURI());
				deviceResource.addProperty(ssnImplements, capabilityResource);

				RDFNode capabilityClassNode = solution.get("capabilityClass");
				if (capabilityClassNode != null) {
					capabilityResource.addProperty(RDF.type, capabilityClassNode.asResource());
				}

				RDFNode propertyNode = solution.get("property");
				RDFNode propertyValueNode = solution.get("propertyValue");
				if (propertyNode != null && propertyValueNode != null) {
					capabilityResource.addProperty(ResourceFactory.createProperty(propertyNode.asResource().getURI()), propertyValueNode.asLiteral());
				}
			}
		}

		return items;
	}

	private String writeJSONLD(Model model) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, "JSON-LD");
		return out.toString();
	}

	private AvatarConfiguration createAvatarConfiguration(Resource deviceResource) {
		logger.debug("Transform a device configuration into avatar configuration for compatibility with anterior code");
		ListMultimap<String, Properties> functionalitiesConfigurations = ArrayListMultimap.create();

		Statement labelStatement = deviceResource.getProperty(RDFS.label);
		String avatarLabel = "";
		if (labelStatement != null) {
			avatarLabel = labelStatement.getObject().toString();
		}

		// TODO 15/12/2017 LT: Also get instance.name from capabilities AND/OR filters such as device=Gopigo

		StmtIterator capabilitiesIt = deviceResource.listProperties(ssnImplements);
		while(capabilitiesIt.hasNext()) {
			logger.debug("Implements a capability");
			Statement statement = capabilitiesIt.nextStatement();
			Resource capability = statement.getObject().asResource();
			if (capability == null) {
				throw new NoSuchElementException("Statement links to undefined capability : " + statement);
			}
			Properties props = new Properties();
			StmtIterator propertiesIt = capability.listProperties();
			while(propertiesIt.hasNext()) {
				Statement propertyStatement = propertiesIt.nextStatement();
				if (propertyStatement.getObject().isLiteral()) {
					props.setProperty(propertyStatement.getPredicate().getLocalName(), propertyStatement.getObject().asLiteral().getString());
				}
			}

			// Find the bundle that provides the functionality factory to interact with a device's capability

			String capabilityClassName;
			try {
				capabilityClassName = capability.getProperty(RDF.type).getObject().asResource().getLocalName();
			} catch(NullPointerException e) {
				throw new NoSuchElementException("Device links to undefined capability or capability class : " + capability);
			}
			Requirement requirement = repositoryAdmin.getHelper().requirement(AsawooUtils.ASAWOO_CAPABILITY_NAMESPACE,
					"("+AsawooUtils.ASAWOO_CAPABILITY_NAME_FILTER_PROPERTY +"=" + capabilityClassName + ")");

			org.apache.felix.bundlerepository.Resource[] resources = repositoryAdmin.discoverResources(new Requirement[] { (Requirement)requirement });
			if (resources == null || resources.length == 0) {
				throw new NoSuchElementException("Capability class not supported by a bundle : " + capabilityClassName);
			}
			Optional<Capability> optionalOSGICapability = Arrays.stream(resources[0].getCapabilities())
					.filter(c -> {
						return c.getName().equals(AsawooUtils.ASAWOO_CAPABILITY_NAMESPACE)
								&& c.getPropertiesAsMap().get("name").equals(capabilityClassName);
					})
					.findFirst();
			if (optionalOSGICapability.isPresent()) {
				Capability osgiCapability = optionalOSGICapability.get();
				logger.debug("OSGI capability " + osgiCapability);
				logger.debug(osgiCapability.getPropertiesAsMap());
				Map<String, Object> osgiProperties = osgiCapability.getPropertiesAsMap();
				functionalitiesConfigurations.put(osgiProperties.get("factory").toString(), props);
			} else {
				throw new NoSuchElementException("Capability class not supported by a bundle : " + capabilityClassName);
			}
		}
		return new AvatarConfiguration(deviceResource.getLocalName(), avatarLabel, functionalitiesConfigurations);
	}

	@Validate
	private void start() throws UnavailableException {
		logger.info("Start the Device configuration manager");

		// Start by looking for existing configurations and creating their avatars
		Model initialModel = getModelFromResults(semanticRepo.select(SPARQL_LIST_DEVICES), null);
		ResIterator initialIt = initialModel.listResourcesWithProperty(RDF.type, ssnSystem);
		while(initialIt.hasNext()) {
			Resource resource = initialIt.nextResource();
			logger.debug(resource);
			AvatarConfiguration avatarConfiguration = createAvatarConfiguration(resource);
			avatarFactory.createAvatar(avatarConfiguration);
		}

		// Get the list of device configuration over HTTP. Response will be a JSONLD graph.
		router.get("/deviceconfigurations").handler(routingContext -> {
			logger.debug("Get list of device configurations");

			vertx.<ResultSet>executeBlocking(future -> {
				ResultSet results = semanticRepo.select(SPARQL_LIST_DEVICES);
				future.complete(results);
			}, false, res -> {
				if (res.failed()) {
					routingContext.response().setStatusCode(500).end(res.cause().getMessage());
				} else {
					routingContext.response()
							.setStatusCode(HttpResponseStatus.OK.code())
							.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
							.end(writeJSONLD(getModelFromResults(res.result(), null)));
				}

			});
		});

		// Get a device configuration over HTTP. Response will be a JSONLD graph.
		router.get("/deviceconfigurations/:id").handler(routingContext -> {
			String id = routingContext.pathParam("id");
			logger.debug("Get a device configuration : " + id);

			// Use the same sparql query used for listing, but with 1 injected parameter.
			ParameterizedSparqlString pss = new ParameterizedSparqlString();
			String deviceURI = "http://liris.cnrs.fr/asawoo/devices#" + id;
			pss.setCommandText(SPARQL_LIST_DEVICES);
			pss.setIri("device", deviceURI);

			vertx.<ResultSet>executeBlocking(future -> {
				ResultSet results = semanticRepo.select(pss.toString());
				future.complete(results);
			}, false, res -> {
				if (res.failed()) {
					routingContext.response().setStatusCode(500).end(res.cause().getMessage());
				} else {
					routingContext.response()
							.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
							.setStatusCode(200)
							.end(writeJSONLD(getModelFromResults(res.result(), deviceURI)));
				}
			});
		});

		// Delete a device configuration over HTTP
		router.delete("/deviceconfigurations/:id").handler(routingContext -> {
			String id = routingContext.pathParam("id");
			logger.debug("Delete a device configuration : " + id);

			// Use the same sparql query used for listing, but with 1 injected parameter.
			ParameterizedSparqlString pss = new ParameterizedSparqlString();
			String deviceURI = "http://liris.cnrs.fr/asawoo/devices#" + id;
			pss.setCommandText(SPARQL_LIST_DEVICES);
			pss.setIri("device", deviceURI);
			vertx.executeBlocking(future -> {
				ResultSet results;
				results = semanticRepo.select(pss.toString());

				// actualize the avatars manager
				avatarFactory.disposeAvatar(id);

				// remove statements
				semanticRepo.delete(getModelFromResults(results, deviceURI), "http://liris.cnrs.fr/asawoo/devices");
				future.complete();
			}, false, res -> {
				if (res.failed()) {
					routingContext.response().setStatusCode(500).end(res.cause().getMessage());
				} else {
					routingContext.response().setStatusCode(204).end();
				}
			});
		});

		// Post 1 or many device configurations (new or updated)
		router.post("/deviceconfigurations").handler(routingContext -> {
			logger.debug("Create a device configuration");

			Model model = ModelFactory.createDefaultModel();
			InputStream in = new ByteArrayInputStream(routingContext.getBodyAsString().getBytes(StandardCharsets.UTF_8));
			try {
				model.read(in, null, "JSONLD");
			} catch(Exception e) {
				routingContext.response()
						.setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
						.end(e.getMessage());
			}

			vertx.executeBlocking(future -> {
				ResIterator it = model.listResourcesWithProperty(RDF.type, ssnSystem);
				while (it.hasNext()) {
					Resource resource = it.nextResource();
					// Device resource found, first find existing statement about this device if there are some
					ParameterizedSparqlString pss = new ParameterizedSparqlString();
					pss.setCommandText(SPARQL_LIST_DEVICES);
					pss.setIri("device", resource.getURI());
					ResultSet results;
					results = semanticRepo.select(pss.toString());

					// Before persisting in semantic repository we actualize the avatars manager
					AvatarConfiguration avatarConfiguration = createAvatarConfiguration(resource);
					avatarFactory.disposeAvatar(avatarConfiguration.getAvatarId());
					avatarFactory.createAvatar(avatarConfiguration);
					// Delete then insert so than we don't keep deprecated statements
					semanticRepo.delete(getModelFromResults(results, resource.getURI()), "http://liris.cnrs.fr/asawoo/devices");
					semanticRepo.insert(resource.getModel(), "http://liris.cnrs.fr/asawoo/devices");
				}
				future.complete();
			}, false, res -> {
				if (res.failed()) {
					logger.error("Failed to load device configuration " + res.cause());
					res.cause().printStackTrace();
					routingContext.response().setStatusCode(500).end(res.cause().getMessage());
				} else {
					routingContext.response().setStatusCode(201).end();
				}
			});


		});
	}
}
